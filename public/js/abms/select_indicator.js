
$(document).ready( function (event) {

    $('#continue').toggle($('.selected').length > 0);

    $('form').submit(function (e) {
        $('form#indicator').attr('action', '/select_indicators/'  + $('a.selected').data('id'));
        return true;
    });
});

function selectIndicator(id)
{
    $('a[data-id]').removeClass('selected');
    $('a[data-id="' + id + '"]').addClass('selected');
    $('div#indicator_description').load('/select_indicators/description/'+id);
    $('#continue').toggle($('.selected').length > 0);
}
