$(document).ready( function (event) {

    $(':button#continue').toggle($(':button.selected').length > 0);

    $('form').submit(function (e) {

        $(':button.selected').each( function (i,el) {
            $('<input name="accounts[]" type="hidden">').val( JSON.stringify({
                'parent-id': $(el).data('parent-id'),
                'id' : $(el).data('id'),
                'driver' : $(el).data('driver')
            })).appendTo(e.target);
        });
        return true;
    });
    $(':button[data-id]').click( function (e) {
        $(e.target)
            //.toggleClass('btn-outline-secondary')
            .toggleClass('btn-danger selected')
            .text($(e.target).hasClass('selected') ? 'Selected' : 'Select');
        $(e.target).closest('div.row')
            .toggleClass('bg-warning');
        $(':button#continue').toggle($(':button.selected').length > 0);
    });
});
