@extends('layouts.master')

@section('scripts')
    <link href="{{asset('css/kpiConfig.css')}}" rel="stylesheet">
    @can('any_platform_selected')
        <script>
            $(document).ready(() => { $('#main').addClass('with-panel');})
        </script>
    @endcan
@endsection
@section('content')
    <div style="margin-top: 10%;" id="main">

    @include('includes.platform_panel')
    <!-- end sidebar -->

        <div class="d-flex justify-content-center mb-3 text-center">
            <h1 class="h1 w-75">¿What is the <span class="text-red">name</span> of this KPI?</h1>
        </div>

        <div class="d-flex justify-content-center mb-5 text-center">
            <p class="mb-0 font-weight-normal">A name for ypur KPI is very useful
                <span class="text-red">to identify</span>
                it and introduce it to your friends.</p>
        </div>

        <br>

        <div class="login-box d-flex justify-content-center">
            <form class="width-25" method="post" id="indicator">
                @csrf
                <div class="user-box">
                    <input type="text" name="name" value="{{isset($indicator->name) ? $indicator->name : request('name')}}" required>
                    <label>enter the name</label>
                </div>
                    <input type="radio" name="indicator_type" value="measurer" {{ !isset($indicator) || $indicator->indicator_type == 'measurer' ? 'checked="checked"' : ''}}> I want to create a  measuring indicator. To compare two different time ranges<br>
                    <input type="radio" name="indicator_type" value="reporter"  {{ isset($indicator) && $indicator->indicator_type == 'reporter' ? 'checked="checked"' : ''}}> I wish to create a reporting indicator. Only to show result. <span style="color: red"> (Not sported yet!)</span>
            </form>
        </div>

        <div class="d-flex justify-content-around mb-2 mt-5">
            <a type="button" class="btn btn-outline-danger w-25" href="/select_indicators">back</a>
            <a type="button" class="btn btn-outline-danger w-25" href="javascript:$('form#indicator').trigger('submit')">next</a>
        </div>

    </div>
    <!--end login-->
@endsection
