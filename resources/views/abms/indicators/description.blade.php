@extends('layouts.master')

@section('scripts')
    <link href="{{asset('css/kpiDescription.css')}}" rel="stylesheet">
    @can('any_platform_selected')
        <script>
            $(document).ready(() => { $('#main').addClass('with-panel');})
        </script>
    @endcan
@endsection
@section('content')
    <div style="margin-top: 10%;" id="main">

    @include('includes.platform_panel')
    <!-- end sidebar -->

        <div class="d-flex justify-content-center mb-3 text-center">
            <h1 class="h1 w-75">What about a <span class="text-red">description</span>?</h1>
        </div>
        <div class="d-flex justify-content-center mb-3 text-center">
            <h2 class="h2 w-75">{{$request['name']}}</h2>
        </div>

        <div class="d-flex justify-content-center mb-5 text-center">
            <p class="mb-0 font-weight-normal">A good description helps
                <span class="text-red">to understand</span>
                what are you measuring,<br>tell the difference between your KPI's name, and the other KPI's.</p>
        </div>

        <br>

        <div class="login-box d-flex justify-content-center">
            <form class="width-50" method="post" id="indicator">
                @csrf
                <input type="hidden" name="name" value="{{$request['name']}}">
                <input type="hidden" name="indicator_type" value="{{$request['indicator_type']}}">
                <div class="user-box">
                    <textarea type="text" name="description" required rows="6">{{trim(isset($indicator->description) ? $indicator->description : request('description'))}}</textarea>
                    <label>enter a description</label>
                </div>
            </form>
        </div>

        <div class="d-flex justify-content-around mb-2 mt-5">
            <a type="button" class="btn btn-outline-danger w-25" href="/select_indicators">back</a>
            <a type="button" class="btn btn-outline-danger w-25" href="javascript:$('form#indicator').trigger('submit')">next</a>
        </div>

    </div>
    <!--end login-->
@endsection
