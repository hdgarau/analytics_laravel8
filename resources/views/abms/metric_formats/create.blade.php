{{ Form::open(array('url' => '/metric_formats', 'method' => 'post')) }}
    <div class="form-group">
        {!! Form::Label('Nombre') !!}
        {!! Form::text('nombre','', [ 'class' => 'form-control', 'placeholder' => 'Nombre']) !!}
    </div>
    <div class="form-group">
        {!! Form::Label('Título a mostrar') !!}
        {!! Form::text('titulo','', [ 'class' => 'form-control', 'placeholder' => 'Título a mostrar']) !!}
    </div>
    <div class="">
        {!! Form::submit('Guardar', [ 'class' => 'btn btn-success right']) !!}
    </div>
{{ Form::close() }}
