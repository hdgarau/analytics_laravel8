@extends('layouts.master')

@section('scripts')
    <link href="{{asset('css/indicatorLabels.css')}}" rel="stylesheet">
    <link href="{{asset('js/plugins/tagsinput.css')}}" rel="stylesheet">
    <script src="{{asset('js/plugins/tagsinput.js')}}"></script>
    <script>
        $(document).ready(  function (e) {
            $('form#primary').submit(function (e) {
                $('input#tags').val().split(',').forEach(el => {
                    var input = $('<input type="hidden" value="' + el + '" name="labels[]">');
                    $(e.target).append(input);
                    return true;
                });
            });
            // $(':input#tag').autocomplete({
            //     source: function( request, response ) {
            //         // Fetch data
            //         $.ajax({
            //             url:"/ajax/labels/list",
            //             type: 'get',
            //             dataType: "json",
            //             data: {
            //                 search: request.term,
            //                 //without: labelsSelectedNames
            //             },
            //             success: function( data ) {
            //
            //                 response( data );
            //             }
            //         });
            //     },
            //     select: function (event, ui) {
            //         // Set selection
            //         //$('#employee_search').val(ui.item.label); // display the selected text
            //         // save selected id to input
            //         selectLabel(ui.item.label);
            //         return false;
            //     }
            // });
            @foreach($labels as $label)
            selectLabel('{{$label}}');
            @endforeach
        });
        function selectLabel(val) {
            if(val)
            {
                $('#labels_selected').append('<div><span class="badge badge-primary">' + val + ' </span> <button type="button" class="badge badge-danger" onclick="$(this).closest(\'div\').remove()" >X</button></div>' );
                $('#tag').val('' );
            }
        }
    </script>

    <link href="https://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootswatch/4.3.1/flatly/bootstrap.min.css">
    <style>
        body { background-color:#fafafa;}
        .container { margin: 150px auto; }
        h2 { margin:20px auto; font-size:14px;}
        .badge { margin: 2px 5px; }
    </style>
    <script type="text/javascript"
            src="https://pagead2.googlesyndication.com/pagead/show_ads.js">
    </script>
@endsection
@section('content')
    <div style="margin-top: 8%;" class="text-center">

        @if ($errors->any())
            @include('includes.toast',['show' => true])
        @endif
        <div class="d-flex justify-content-center mb-5">
            <h1 class="h1 w-75">What about some tags?  </h1>
        </div>

        <div class="d-flex justify-content-center mb-5">
            <p class="mb-0 w-25 text">Tags complement description, They help you to highlight what you find important. Add a new tag or choose an existing tag from list.</p>
        </div>

        <div class="login-box d-flex justify-content-center">
            <form class="width-25" method="post" id="add_tag">
                @csrf
                <div class="user-box">
                    <input type="text" name="tags" id="tags"  data-role="tagsinput" value="{{implode(',',$labels)}}">
                    <label>Press enter to add tag...</label>
                </div>
            </form>
        </div>

        <div class="d-flex justify-content-center">
            <form class="width-25 mt-4" method="post" id="primary" >
                @csrf
                <button type="submit" class="btn btn-outline-danger w-100 mt-4" >Finish</button>
            </form>
        </div>

    </div>
@endsection
