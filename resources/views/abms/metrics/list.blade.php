@extends('layouts.master')

@section('content')
    <div class="row h-100" style="float: none; background-color: #efefef">
        <div class="col-md-11 my-auto" align="center" >
            <h1>
                <strong> Metrics - <a href="/metrics/create"> [ New ]</a> </strong>
            </h1>
            <div class="row" style="margin-bottom: 200px;">
                <div class="col-md-10 offset-2">
                    <table id="table_metrics">
                        <thead>
                            <tr>
                                <th>Platform</th>
                                <th>Name</th>
                                <th>Descriptions</th>
                                <th>Formats</th>
                                <th>Objectives</th>
                                <th>Edit</th>
                                <th>Delete</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($metrics as $metric)
                                <tr>
                                    <td>{{$metric->platform->name}}</td>
                                    <td>{{$metric->name}}</td>
                                    <td>{{$metric->description}}</td>
                                    <td>{{$metric->format_names}}</td>
                                    <td>{{$metric->objective_names}}</td>
                                    <td><a class="btn btn-primary" href="/metrics/{{$metric->id}}/edit">Edit</a></td>
                                    <td>
                                        <form action="/metrics/{{$metric->id}}" method="post">
                                            @csrf
                                            @method('delete')
                                            <button class="btn btn-danger" href="">Delete</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>
@endsection
@section('scripts')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css" />
    <script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
    <script>
        $(document).ready((event) => {
            $('table#table_metrics').dataTable();
        });
    </script>
@endsection
