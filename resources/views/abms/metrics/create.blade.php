@extends('layouts.master')

@section('content')
    <div class="row h-100" style="float: none; background-color: #efefef">
        <div class="col-md-11 my-auto" align="center" >
            <h1>
                <strong> Create Metric  </strong>
            </h1>
            <div class="row" style="margin-bottom: 200px;">
                <div class="col-md-10 offset-2">
                    <form method="post" action="/metrics" >
                        @csrf

                        <div class="row">
                            <div class="col-md-6">
                                <x-select-std name="plataforma" source="platforms" />
                            </div>
                            <div class="col-md-6">
                                <input name="nombre" class="form-control" placeholder="Nombre" >
                            </div>
                        </div>
                        <div class="row" style="text-align: left">
                            <div class="form-group col-md-3">
                                <x-select-checkbox name="formatos" source="metric_formats" />
                            </div>
                            <div class="form-group col-md-3">
                                <x-select-checkbox name="objetivos" source="metric_objectives" />
                            </div>
                            <div class="form-group col-md-6">
                                <textarea name="description" class="form-control" placeholder="Description"></textarea>

                            </div>
                        </div>

                        <div class="form-group">


                        </div>

                        <div class="">
                            <a href="/metrics" class="btn btn-danger">Cancel</a>
                            <input type="submit" class="btn btn-success right">
                        </div>

                    </form>
                    <script>
                        $(document).ready(function () {

                        })
                    </script>
@endsection

