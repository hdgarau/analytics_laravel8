<form action="/users" method="post">
    @csrf
    <input type="hidden" name="tipo_usuario" value="client">
    <div class="form-group">
        <label for="nombre"> Nombre</label>
        <input type="text" class="form-control" name="nombre" placeholder="Nombre">
    </div>
    <div class="form-group">
        <label for="email"> e-mail</label>
        <input type="text" class="form-control" name="email" placeholder="e-mail">
    </div>
    <div class="form-group">
        <label for="clave"> Clave</label>
        <input type="password" class="form-control" name="clave" placeholder="">
    </div>
    <div class="">
        <button type="submit" class="btn btn-success right">Guardar</button>
    </div>
</form>
