<form action="/users/{{$obj->id}}" method="post">
    @method('put')
    @csrf
    <input type="hidden" name="tipo_usuario" value="{{$obj->user_type}}">
    <div class="form-group">
        <label for="nombre"> Nombre</label>
        <input type="text" class="form-control" name="nombre" value="{{$obj->name}}" placeholder="Nombre" required>
    </div>
    <div class="form-group">
        <label for="email"> e-mail</label>
        <input type="text" class="form-control" name="email" value="{{$obj->email}}" placeholder="e-mail" required>
    </div>
    <div class="form-group">
        <label for="clave"> Modificar Clave <input type="checkbox" id="ck_modificar_clave"> </label>
        <input type="password" class="form-control" id="clave" name="clave" placeholder="">
    </div>
    <div class="">
        <button type="submit" class="btn btn-success right">Guardar</button>
    </div>
</form>
<script>
    $(document).ready(function () {
        $('#ck_modificar_clave').change(function (e) {
            var checked = $(this).prop('checked');
            $('#clave').attr({'disabled':!checked}).toggle(checked);
        }).triggerHandler('change');
    })
</script>
