{{ Form::open(array('url' => '/metric_objectives/' . $obj->id, 'method' => 'put')) }}
<div class="form-group">
    {!! Form::Label('Nombre') !!}
    {!! Form::text('nombre',$obj->name, [ 'class' => 'form-control', 'placeholder' => 'Nombre']) !!}
</div>
<div class="form-group">
    {!! Form::Label('Título a mostrar') !!}
    {!! Form::text('titulo',$obj->caption, [ 'class' => 'form-control', 'placeholder' => 'Título a mostrar']) !!}
</div>
<div class="">
    {!! Form::submit('Guardar', [ 'class' => 'btn btn-success right']) !!}
</div>
{{ Form::close() }}
