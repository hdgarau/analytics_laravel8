@extends('layouts.master')

@section('scripts')
    <link href="{{asset('css/kpiMetric.css')}}" rel="stylesheet">
    @include('abms.indicator_metrics.edit_scripts')
    @can('any_platform_selected')
        <script>
            $(document).ready(() => { $('#main').addClass('with-panel');})
        </script>
    @endcan
@endsection
@section('content')
    <div style="margin-top: 10%;" id="main">

    @include('includes.platform_panel')

            <!-- end sidebar -->

            <div class="d-flex justify-content-center mb-3 text-center">
                <h1 class="h1 w-75">¿Which <span class="text-red">metrics</span> has this KPI?</h1>
            </div>

            <div class="d-flex justify-content-center mb-5 text-center">
                <p class="mb-0 font-weight-normal">Here is the place to <span class="text-red">mix</span>.
                    Search available metrics from social media,<br>web or email marketing to build a powerful KPI.
                    You may search by platform, metric or objective.</p>
            </div>

            <br>

            <div class="container p-0 mb-5">
                <div class="row d-flex justify-content-between align-items-center">
                    <div class="col-lg-6 d-flex justify-content-between">
                        @foreach($platforms as $platform)
                            <button class="btn border-0 text-black cols p-4">
                                <img  data-id="{{$platform->id}}" src="{{asset('img/' . $platform->driver . '.svg')}}" width="50px">
                            </button>
                        @endforeach

                    </div>

                    <div class="col-lg-5">
                        <div class="login-box">
                            <form class="w-100">
                                <div class="user-box">
                                    <input type="text" name="search" id="search" required>
                                    <label>search</label>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <div class="container mb-4 mt-5" id="result">

            </div>

            <div class="d-flex justify-content-around mb-2 mt-5">
                <a type="button" class="btn btn-outline-danger w-25" href="/indicator/{{$indicator->id}}">back</a>
                <a type="button" class="btn btn-outline-danger w-25" href="/indicator_labels/{{$indicator->id}}">next</a>
            </div>

        </div>
@endsection
