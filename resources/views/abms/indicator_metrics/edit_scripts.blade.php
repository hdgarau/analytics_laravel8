<script>
    function filterAndDraw(  )
    {
        let platforms_selected = Array.from( $(':button.selected>img[data-id]').map((i, el) => $(el).data('id')));
        let key_word = $(':input#search').val().trim();
        let url = '/indicators/{{$indicator->id}}/metrics?mode=all&platforms=' + platforms_selected.join(',') + '&key_word=' + key_word;

        if(platforms_selected.length == 0 )
        {
            alert('Must choose at least one platform');
            return;
        }
        fetch( url ).then((res) => res.json())
            .then((metrics) => {

                if(metrics.length > 0)
                {
                    $('div#result').text('');
                    for(obj of metrics)
                    {

                        let class_name = obj.selected ? 'bg-warning' : 'bg-light';
                        let label = obj.selected ? 'Selected' : 'select metric';


                        let $div = $('<div  class="container mb-4 mt-5">');
                        $div.append($('<p class="text-red font-weight-bold"><img src="{{asset('img/')}}/' + obj.driver + '.svg"  width="15px" class="mr-2"> ' + obj.driver.toUpperCase() + ' | ' + obj.name + '</p>'));
                        $div.append($('<div  class="d-flex justify-content-between align-items-center mb-1""><p class="mb-0 font-weight-normal"> ' + obj.description + '</p><a type="button" class="btn btn-light bg-transparent border-0 text-red w-25 ' + class_name + '" onclick="toggleMetric(' + obj.id + ')"> ' + label + '</a></div>'));
                        $div.appendTo($('div#result'));
                    }
                }
                else
                {
                    $('div#result').html('<h1>No data</h1>');
                }
            });
    }
    function toggleMetric(metric_id)
    {
        fetch('/indicator/{{$indicator->id}}/toggle_metric/' + metric_id,
            {
                method : 'post',
                credentials: "same-origin",
                body: new FormData($('<form>@csrf</form>').get(0))
            })
            .then().catch().then(filterAndDraw)
    }
    $(document).ready(  function (e) {
        $(':input#search').keyup(filterAndDraw);
        $(':button>img[data-id]').click( function (event ) {
            $(event.target).parent().toggleClass('selected');
            filterAndDraw();

        })
        @if(!empty($platforms_preselected))
            @foreach($platforms_preselected as $platform)
                 $(':button>img[data-id="{{$platform->id}}"]').trigger('click');
            @endforeach
        @endif
    });
</script>
