<div>
    @if(isset($data['page']) && is_iterable($data['page']))
        <h3>{{$platform_name}} (Page)</h3>
        <button type="button" onclick="$(this).next().toggle()">Ver/ocultar detalle</button>
        <div rel="detalle" >
            @foreach($data['page'] as $row)
                <h5>{{$row->getItem('Usuario')}}</h5>
                @php
                    $totalFBPage = !isset($totalFBPage) ? 0 : $totalFBPage;
                    $totalFBPage += array_sum($row->getItem('Valores'));
                @endphp
                <div >{{$row->getItem('Metric')}}: {{number_format( array_sum($row->getItem('Valores')),0,',','.')}}</div>
            @endforeach
        </div>
{{--        <h2>Total: {{isset($totalFBPage) ? number_format( $totalFBPage,0,',','.') : 0}}</h2>--}}
    @endif
    @if(isset($data['post']) && is_iterable($data['post']))
        <h3>{{$platform_name}} (Post)</h3>
        <button type="button" onclick="$(this).next().toggle()">Ver/ocultar detalle</button>
        <div rel="detalle" >
            @foreach($data['post'] as $row)
                <h5>{{$row->getItem('Usuario')}}</h5>
                <p>{{$row->getItem('Mensaje')}}  - <a href="{{$row->getItem('Link')}}" target="_blank">{{$row->getItem('Creado')}} </a></p>
                @foreach($row->getItem('Insights') as $metric)
                    @php
                        $totalFBPost = !isset($totalFBPost) ? 0 : $totalFBPost;
                        $totalFBPost += array_sum($metric->getItem('Valores'));
                    @endphp
                    <div >{{$metric->getItem('Metric')}}: {{number_format( array_sum($metric->getItem('Valores')),0,',','.')}}</div>
                @endforeach
            @endforeach
        </div>
{{--        <h2>Total: {{isset($totalFBPost) ? number_format( $totalFBPost,0,',','.') : 0}}</h2>--}}
    @endif
</div>

@php
    unset($totalFBPage);
    unset($totalFBPost);
@endphp
