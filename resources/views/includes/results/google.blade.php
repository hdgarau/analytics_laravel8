<div>
    <h3>{{$platform_name}} </h3>
    <button type="button" onclick="$(this).next().toggle()">Ver/ocultar detalle</button>
    <div rel="detalle" >
        @foreach($data as $row)
            <h5>{{isset($row['account_name']) ? $row['account_name'] : dd($row)}}</h5>
            @foreach($row['metrics'] as $metric => $val)
                @php
                    $total = !isset($total) ? 0 : $total;
                    $total += $val;
                @endphp
                <div >{{$metric}}: {{number_format( $val,0,',','.')}}</div>
                <ul>
                    <li>Max.: {{$max_metric_platform[$metric]}}</li>
                    <li>Value: {{number_format( $val * 10 / ($max_metric_platform[$metric] == 0 ? 1 : $max_metric_platform[$metric] ) ,2,',','.')}}</li>
                </ul>
            @endforeach
        @endforeach
    </div>
{{--    <h2>Total: {{isset($total) ? number_format( $total,0,',','.') : 0}}</h2>--}}
</div>
@php
    unset($total);
@endphp
