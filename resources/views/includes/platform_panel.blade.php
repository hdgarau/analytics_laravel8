@can('any_platform_selected')
    <div id="mySidebar" class="sidebar ml-0">
        <p class="text-sidebar font-weight-bold" href="#">PLATAFORMAS CONECTADAS</p>
        @foreach(session('userPlatforms_selected') as $platform_selected)
            <p class="text-sidebar" href="#"><img width="15px" class="mr-2" src="{{asset('img/checked.svg')}}">{{\App\Models\Platform::Find($platform_selected->platform_id)->name}}</p>
        @endforeach
    </div>
@endcan
