<div class="row">
    <div class="info-box box-default col-md-3">
        <div class="box-header with-border">
            <div class="info-box">

                <div class="pull-right" style="text-align: right">

                    <form class="frm_user_platform" action="/user_plataform/delete/{{$item->id}}" method="post">
                        @if(!is_array($item->accounts))
                        <button type="button" class="btn btn-prymary fa fa-eye"
                                data-toggle="modal"
                                data-url="/metrics/values/user/{{$item->id}}?token={{$item->token}}&token_secret={{isset($item->tokenSecret) ? $item->tokenSecret : ''}}"
                                data-target="#metrics"
                                data-titulo="{{$item->name }}">
                        </button>
                        @endif
                        @if($item->user_id == auth::user()->id)
                            @csrf
                            @method('delete')
                            <button class="btn btn-danger fa fa-trash"></button>
                        @endif
                    </form>
                </div>
                <!-- Apply any bg-* class to to the icon to color it -->
                <span class="info-box-icon bg-{{$color}}-gradient"><i class="fab fa-{{$platform_icon}}"></i></span>
                <div class="info-box-content">
                    <div class="widget-user-image fa-pull-left" >

                        <span class="fa-pull-left"></span><img class="img-circle elevation-2 " src="{{ $item->avatar }}" height="80px">
                    </div>
                    <span class="info-box-number align-bottom" >{{$item->name }}</span>
                </div><!-- /.info-box-content -->
            </div><!-- /.info-box -->
        </div>

        <div class="box-body bg-gray">
            @foreach($item->accounts as $account)
                <div class="row">
                    @foreach((is_array($account) ? $account : [$account]) as $account_item)
                        <div class="col-md-offset-1">
                            <div class="col-md-{{ 8 / (is_array($account) && count($account) > 0 ? count($account) : 1)}}">
                                <div class="info-box box-default">
                                    <span class="info-box-icon bg-blue-gradient"><i class="fab fa-{{$account_item->getDriver()}}"></i></span>
                                    <div class="info-box-content">
                                        <div class="pull-right" style="text-align: right">
                                            <button type="button" class="btn btn-prymary fa fa-eye"
                                                    data-toggle="modal"
                                                    data-url="/metrics/values/user/{{$item->id}}?account_id={{$account_item->getID()}}"
                                                    data-target="#metrics"
                                                    data-titulo="{{$account_item->getName()}}">
                                            </button>
                                        </div>
                                        <div class="widget-user-image fa-pull-left" >

                                            <span class="fa-pull-left"></span><img class="img-circle elevation-2 " src="{{ $account_item->getParam('picture') }}" height="50px">
                                        </div>
                                        <span class="info-box-number align-bottom" >{{$account_item->getName() }}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
                @endforeach

    </div>
    </div>
</div>                <!-- /.widget-user -->
