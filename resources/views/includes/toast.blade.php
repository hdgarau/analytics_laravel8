<!-- toast -->
<div aria-live="polite" aria-atomic="true" style="position: relative; margin-right: 20px;">
    <div class="toast text-center" style="position: absolute; top: 0; right: 250px; width: 250px;" data-delay="2000">
        <div class="toast-header text-danger bg-light">
            <img src="{{ asset('assets/img/warning.svg')}}" class="rounded mr-2" style="width: 25px;">
            <strong class="mr-auto">ERROR!</strong>
            <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="toast-body">
            @if ($errors->any())
                <div class="alert alert-danger" style="text-align: left">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>
    </div>
</div>
@if(isset($show) && $show)
    <script>
        $(document).ready(function(){
                $('.toast').toast('show');
        });
    </script>
@endif
