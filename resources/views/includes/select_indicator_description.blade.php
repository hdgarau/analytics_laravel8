<h4>Description</h4>
<p class="text-black border-top border-bottom">
    <em style="padding-left: 20px"> {{ $indicator->description }}</em>
</p>
<div class="">
    <p class="text-black font-weight-bold p-3 mb-0" >COMBINED MEASURING OF</p>
    @foreach($metrics as $driver => $metrics_info)
    <div class="alert  d-flex justify-content align-items-center mb-1"
         role="alert">
        <p class="mb-0 text-black">
            <img src="{{asset('/img/' . $driver . '.svg')}}" width="25px" class="mr-2">
        </p>
        <p class="mb-0 text-black">
            @foreach($metrics_info as $I => $metric)
                <SPAN>{{($I >0 ? ', ' : '') . $metric->name }}</SPAN>
            @endforeach
    </div>
    @endforeach
    <div style="">
        <p><strong>TAGS: </strong>
                @foreach(explode(',', $label_names ) as $tag)
                    <span class="badge badge-warning" style="margin-left: 10px; font-size:15px">{{$tag}}</span>
                  @endforeach
        </p>
    </div>
    <a type="" HREF="/indicator/{{$indicator->id}}" class="btn bg-secondary w-25">edit KPI</a>
</div>
<hr>
