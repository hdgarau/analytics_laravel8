<input type="hidden" name="compare_with" value="past">
<div style=" margin-bottom: 25px">
    <p>Select a distant past period</p>
</div>
<div style=" padding-bottom: 20px; border-bottom: solid 1px; ">
    <div class="row"  >
        <div class="col-md-6">
            <input class="form-control" name="date_past_since" placeholder="Since.." type="date">
        </div>
        <div class="col-md-6">
            <input class="form-control" name="date_past_until" placeholder="Until.." type="date">
        </div>

    </div>
</div>
<div style=" margin-bottom: 25px">
    <p>Select a near past period</p>
</div>
<div style=" padding-bottom: 20px; border-bottom: solid 1px; ">
    <div class="row"  >
        <div class="col-md-6">
            <input class="form-control" name="date_recent_since" placeholder="Since.." type="date">
        </div>
        <div class="col-md-6">
            <input class="form-control" name="date_recent_until" placeholder="Until.." type="date">
        </div>

    </div>
</div>
<br>
<br>
