@php
    $userPlatform = current(
        array_filter(is_array(session()->get('userPlatforms_selected')) ? session()->get('userPlatforms_selected') : [],
        function ($el) use ($platform){ return $platform->id == $el->platform_id;}));
@endphp
<div class="col-lg-2 col-md-4 col-sm-6 mb-4 text-center ">
    <button class="btn border-0 text-black cols">
        <p class="mb-0 mb-4">{{$platform->name}}</p>
        <img src="{{asset('/img/' . $platform->driver . '.svg')}}" class="w-25" height="80px" width="80px">
        @if(!empty($userPlatform))
            @if($platform->driver == 'mailchimp')
                <form id="change_server">
                    @csrf
                    <input id="server" placeholder="server..." value="{{$userPlatform->server}}" ONCHANGE="saveServer({{$userPlatform->id}})">
                </form>
            @endif
            <a type="button" href="/select_platform/{{$platform->id}}" class="btn btn-warning  border-0 text-black mt-4">Disconnect</a><br>
            <form action="/user_platform/{{$userPlatform->id}}" method="post">
                @csrf
                @method('delete')
                <button type="submit" class="btn btn-danger  border-0 text-black mt-4">Remove</button><br>
            </form>

        @else
            <a type="button" href="/select_platform/{{$platform->id}}" class="btn btn-light bg-transparent border-0 text-black mt-4 ">Connect</a>
        @endif
    </button>
</div>
