@extends('layouts.master')

@section('scripts')
    <script src="{{asset('js/abms/select_profiles.js')}}"></script>
    <link href="{{asset('css/profiles.css')}}" rel="stylesheet">
    @can('any_platform_selected')
        <script>
            $(document).ready(() => { $('#main').addClass('with-panel');})
        </script>
    @endcan
@endsection

@section('content')

    <div style="margin-top: 8%;" id="main">
        <!-- sidebar -->

        @include('includes.platform_panel')

    <div class="d-flex justify-content-center mb-3 text-center">
        <h1 class="h1 w-75">¿Which <span class="text-red">profiles</span> will be on the KPI?</h1>
    </div>

    <div class="d-flex justify-content-center mb-5 text-center">
        <p class="mb-0 font-weight-normal">Selecciona los perfiles en cada plataforma.<br>
            Si la plataforma no aparece aquí, entonces <a href="/home" class="text-red">conéctala</a>.</p>
    </div>

    <br>

        @foreach($profiles as $profile)

            <div class="d-flex align-items-center mb-2 mt-5">
                <img src="{{asset('img/' . $profile->platform->driver . '.svg')}}" width="25px">
                <p class="mb-0 ml-2">{{$profile->platform->name}} ({{$profile->email}})</p>
            </div>
            <div class="container mb-4">
            @foreach($profile->accounts as $account)
                        <div class="alert alert-light d-flex justify-content-between align-items-center mb-1" role="alert">
                            <p class="mb-0 text-black">{{$account->getName()}}</p>
                            <button type="button"
                                    data-id="{{$account->getID()}}"
                                    data-parent-id="{{$profile->id}}"
                                    data-driver="{{$account->getDriver()}}"

                                    class="btn btn-outline-danger w-25">seleccionar</button>
                        </div>
                @endforeach
            </div>
        @endforeach

            <div class="d-flex justify-content-around mb-2 mt-5">
                <a  href="/home" class="btn btn-outline-danger w-25 " >Back</a>
                <form class="width-25" method="post">
                    @csrf
                    <button type="submit" id="continue" class="btn btn-outline-danger w-100 " >siguiente</button>
                </form>
            </div>
    </div>
    <!--
    <div class="row h-100" style="float: none; background-color: #efefef">
            <div class="col-md-11 my-auto" align="center" >
                <h1>
                    <strong> Witch profiles will be on the KPI?</strong>
                </h1>
                    <p> Select the profiles by each platform. If this one is not here <a href="/home"> connect it</a></p>
               Card content-->

@endsection
