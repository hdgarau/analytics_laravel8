@extends('layouts.master')

@section('scripts')
    <link href="{{asset('css/selectIndicators.css')}}" rel="stylesheet">
    <script>
        function setNextCondition(elementDOM) {
            let $elementDOM =   $(elementDOM);
            if($elementDOM.val() === '')
            {
                if($elementDOM.closest('.row').next('div').next('div').length === 0)
                {
                    $elementDOM.closest('.row').next('div').remove();
                }
            }
            else
            {
                console.log($elementDOM.closest('.row').next('div'));
                if($elementDOM.closest('.row').next('div').length === 0)
                {
                    let $next = $elementDOM.closest('.row').clone();
                    $next.find(':input').each(el=> $(el).val(''));
                    $elementDOM.closest('.row').after($next);
                }
            }
        }
        $(document).ready( function (eventReady) {
            $('form').submit( function (event) {
                let incomplete = $('div.row:has(:input[name^="logic_gate"])')
                    .removeClass( 'bg-warning')
                    .filter((i,el) => $(el).find(':input[name^="logic_gate"]').val() != '' && $(el).find(':input[name^="keyword"]').val() == '')
                    .addClass( 'bg-warning')
                    .length > 0;
                if(incomplete)
                {
                    event.preventDefault();
                    alert('You must complete input field with AND/OR defined.');
                }
            });
        });
    </script>
@endsection

@section('content')
    <div style="margin-top: 5%;" id="main">
        <!-- sidebar -->
    @include('includes.platform_panel')
    <!-- end sidebar -->

        <div class="form-signin text-center">
        <h1>Filter by content</h1>

        @if ($errors->any())
            @include('includes.toast',['show' => true])
        @endif
            <hr>
            <div class="login-box d-flex justify-content-center">
            <form method="post" id="indicator">
                @csrf
                @foreach($filtersAvailable as $platform => $filters)
                    <h2>{{$platform}}</h2>
                    @if(count($filters) > 1)
                        <p>Join filters By <input name="union[{{$platform}}]" type="radio" selected="selected" value="AND">AND  <input type="radio" value="OR">OR  </p>
                    @endif
                    @foreach($filters as $filter)
                        <h3>{{ $filter['title'] }}</h3>
                        <div class="row mt-4" >
                            <div id="indicator_list" class="col-md-8 ">
                                <input name="keyword[{{$platform}}][{{$filter['type']}}][]" class="form-control" placeholder="Tip condition...">
                            </div>
                            <div class="col-md-4">
                                <select name="logic_gate[{{$platform}}][{{$filter['type']}}][]" class="form-control" onchange="setNextCondition(this)">
                                    <option value="">-</option>
                                    <option value="and">AND</option>
                                    <option value="or">OR</option>
                                </select>
                            </div>
                        </div>
                    @endforeach
                    <hr>
                @endforeach

                <div class="d-flex justify-content-around mb-2 mt-5">
                    <a type="button" class="btn btn-outline-danger w-25" href="/select_time_range">back</a>
                    <a type="button" class="btn btn-outline-danger w-25" href="/show_result">Skip</a>

                    <button type="button" onclick="$('form#indicator').trigger('submit')" id="continue" class="btn btn-outline-danger w-25" href="">next</button>
                </div>
            </form>
        </div>

    </div>
@endsection
