@extends('layouts.master')

@section('scripts')
    <link href="{{asset('css/home.css')}}" rel="stylesheet">
    @can('any_platform_selected')
        <script>
            $(document).ready(() => {
                $('#main').addClass('with-panel');
            });

            function saveServer(idUserPlatform) {
                var token = $("form#change_server [name='_token']").val();
                fetch('/user_platform/update_fields/' + idUserPlatform,  {
                    method: 'post',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                        credentials: "same-origin"
                    },
                    body: JSON.stringify({server: $('form#change_server :text#server').val(),_token:token})
                }).catch(() => alert('Error: Can\'t save Server'))
            }
        </script>
    @endcan
@endsection
@section('content')
    <div style="margin-top: 8%;" id="main">
        <!-- sidebar -->

        @include('includes.platform_panel')
        <!-- end sidebar -->

        <div class="d-flex justify-content-center mb-3 text-center">
            <h1 class="h1 w-75">¿En dónde están los <span class="text-red">datos</span> que quieres medir?</h1>
        </div>

        <div class="d-flex justify-content-center mb-5 text-center">
            <p class="mb-0 font-weight-normal">Selecciona las plataformas que quieres medir. Es necesario conectarlas para continuar.<br>
                Una vez estén conectadas comenzaremos a trabajar para tí.</p>
        </div>

        <br>

        <div class="d-flex justify-content-center mb-5 mt-5 row">
            @each('includes.home_card_platform', $platforms,'platform')
        </div>
        @can('any_platform_selected')

        <div class="d-flex justify-content-center mb-5">
            <form class="width-25">
                <a type="button" class="btn btn-outline-danger w-100 mt-4" href="/select_profiles">siguiente</a>
            </form>
        </div>
@endcan
    </div>

    <!--end login-->
@endsection
