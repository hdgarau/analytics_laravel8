@extends('layouts.master')

@section('scripts')
    <link href="{{asset('css/register.css')}}" rel="stylesheet">
@endsection
@section('content')
    <!--signin-->
    <div class="form-signin text-center">
      <h1>sign in</h1>
      <p class="mb-3 font-weight-normal">or back to <a type="button" class="text-red" href="/login">login</a></p>

        @if ($errors->any())
            @include('includes.toast',['show' => true])
        @endif
      <div class="login-box d-flex justify-content-center">
        <form class="w-25" method="POST">
            @csrf
          <div class="user-box">
            <input type="text" name="name" required>
            <label>name</label>
          </div>
          <div class="user-box">
            <input type="text" name="last_name" required>
            <label>last name</label>
          </div>
          <div class="user-box">
            <input type="email" name="email" required>
            <label>email address</label>
          </div>
          <div class="user-box">
            <input type="email" name="email_confirmation" required>
            <label>confirm email address</label>
          </div>
          <div class="user-box">
            <input type="password" name="password" required>
            <label>password</label>
          </div>
          <div class="user-box">
            <input type="password" name="password_confirmation" required>
            <label>confirm password</label>
          </div>

          <button type="submit" class="btn btn-outline-danger w-100 mt-4" >sign up</button>
        </form>
      </div>

    </div>
    @endsection
