@extends('layouts.master')

@section('scripts')
    <link href="{{asset('css/resetPass.css')}}" rel="stylesheet">
@endsection
@section('content')
    <div style="margin-top: 8%;" class="text-center">

        @if ($errors->any())
            @include('includes.toast',['show' => true])
        @endif
        <div class="d-flex justify-content-center mb-5">
            <h1 class="h1 w-75">reset password</h1>
        </div>

        <div class="d-flex justify-content-center mb-5">
            <p class="mb-0 w-25 text">Please write the email address you used for sign-in to reset your password</p>
        </div>

        <div class="login-box d-flex justify-content-center">
            <form class="width-25" method="post" id="email">
                @csrf
                <div class="user-box">
                    <input type="text" name="email" required>
                    <label>email address</label>
                </div>
            </form>
        </div>

        <div class="d-flex justify-content-center">
            <form class="width-25 mt-4" >
                <button type="button" class="btn btn-outline-danger w-100 mt-4" onclick="javascript:$('form#email').trigger('submit')">send</button>
            </form>
        </div>

    </div>
    @endsection
