@extends('layouts.master')

@section('scripts')
    <link href="{{asset('css/resetPass.css')}}" rel="stylesheet">
@endsection
@section('content')
    <div style="margin-top: 8%;">

        @if ($errors->any())
            @include('includes.toast',['show' => true])
        @endif
        <div class="d-flex justify-content-center mb-5">
            <h1>Change password for {{$user->name}} ({{$user->email}})</h1>
        </div>

        <div class="login-box d-flex justify-content-center">
            <form class="width-25" method="post" id="pass" action="/reset_password_code/{{$user->id}}">
                @csrf;
                @method('put')
                <div class="user-box">
                    <input type="password" name="password" required>
                    <label>new password</label>
                </div>
                <div class="user-box">
                    <input type="password" name="password_confirmation" required>
                    <label>confirm password</label>
                </div>
            </form>
        </div>

        <div class="d-flex justify-content-center">
            <form class="width-25 mt-4">
                <a type="button" class="btn btn-outline-danger w-100 mt-4"  onclick="javascript:$('form#pass').trigger('submit')">Change Password</a>
            </form>
        </div>

    </div>
@endsection
