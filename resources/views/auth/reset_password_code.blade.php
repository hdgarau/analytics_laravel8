@extends('layouts.master')

@section('scripts')
    <link href="{{asset('css/resetPass.css')}}" rel="stylesheet">
@endsection
@section('content')
    <div style="margin-top: 8%;">

        @if ($errors->any())
            @include('includes.toast',['show' => true])
        @endif
        <div class="d-flex justify-content-center mb-5">
            <h1 class="h1 w-75">Please <span class="text-red">check your email</span>, we
                have sent a message with a code to reset your password.</h1>
        </div>

        <div class="login-box d-flex justify-content-center">
            <form class="width-25" method="post" id="code">
                @csrf
                <div class="user-box">
                    <input type="text" name="code" required>
                    <label>enter your code</label>
                </div>
            </form>
        </div>

        <div class="d-flex justify-content-center">
            <form class="width-25 mt-4">
                <a type="button" class="btn btn-outline-danger w-100 mt-4"  onclick="javascript:$('form#code').trigger('submit')">continue</a>
            </form>
        </div>

        <div class="mt-4 text-center mb-5">
            <a type="button" class="btn btn-light bg-white border-0 text-red" href="/reset_password_no_code/{{$user->id}}">Didn't receive the email?</a>
        </div>

    </div>
    @endsection
