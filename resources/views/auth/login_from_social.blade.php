@extends('layouts.master')

@section('scripts')
    <link href="{{asset('css/loginSocial.css')}}" rel="stylesheet">
@endsection
@section('content')
    <div style="margin-top: 8%;">

        <div class="d-flex justify-content-center mb-5">
            <h1 class="h1 w-75">Please <span class="text-red">log in with a platform</span> you are measuring with
                <img src="{{asset('/img/deductive-logo.JPG')}}" style="width: 200px;">
            </h1>
        </div>

        <div class="d-flex justify-content-center mb-5 row">
            <div class="col-lg-2 col-md-4 col-sm-6 mb-4 text-center">
                <button class="btn border-0 text-black cols">
                    <p class="mb-0 mb-4">FACEBOOK</p>
                    <img src="/img/facebook.svg" class="w-25">
                    <a type="button" href="/login/facebook" class="btn btn-light bg-transparent border-0 text-black mt-4">connect</a>
                </button>
            </div>
            <div class="col-lg-2 col-md-4 col-sm-6 mb-4 text-center">
                <button class="btn border-0 text-black cols">
                    <p class="mb-0 mb-4">TWITTER</p>
                    <img src="/img/twitter.svg" class="w-25">
                    <a type="button" href="/login/twitter" class="btn btn-light bg-transparent border-0 text-black mt-4">connect</a>
                </button>
            </div>
            <div class="col-lg-2 col-md-4 col-sm-6 mb-4 text-center">
                <button class="btn border-0 text-black cols">
                    <p class="mb-0 mb-4">GOOGLE ANALYTICS</p>
                    <img src="/img/google-analytics-icon-vector-27.jpg" class="w-25">
                    <a type="button" href="/login/google" class="btn btn-light bg-transparent border-0 text-black mt-4">connect</a>
                </button>
            </div>
            <div class="col-lg-2 col-md-4 col-sm-6 mb-4 text-center">
                <button class="btn border-0 text-black cols">
                    <p class="mb-0 mb-4">MAILCHIMP</p>
                    <img src="/img/Mailchimp-logo.png" class="w-25">
                    <a type="button" href="/login/mailchimp" class="btn btn-light bg-transparent border-0 text-black mt-4">connect</a>
                </button>
            </div>
        </div>

        <div class="d-flex justify-content-center mb-5">
            <form class="width-25">
                <a type="button" class="btn btn-outline-danger w-100 mt-4" href="../login/login.html">continue</a>
            </form>
        </div>

    </div>
    @endsection
