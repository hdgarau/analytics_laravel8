@extends('layouts.master')

@section('scripts')
    <link href="{{asset('css/notEmail.css')}}" rel="stylesheet">
@endsection
@section('content')
    <div style="margin-top: 4%;">

        <div class="d-flex justify-content-center mb-5">
            <h1 class="h1 w-75">Didn't receive the email to reset your password? <span class="text-red">Please
                    verify!</span></h1>
        </div>

        <div class="d-flex justify-content-center">
            <div class="width-30 bg-light d-flex align-items-center mb-2 p-3">
                <img src="{{asset('img/checked.svg')}}" style="width: 15px;">
                <a href="/reset_password_code/{{$user->id}}" class="mb-0 ml-3 font-500 text-black btn btn-light bg-transparent border-0">
                    is our message in the spam folder?
                </a>
            </div>
        </div>{{--
        <div class="d-flex justify-content-center">
            <div class="width-30 bg-light d-flex align-items-center mb-2 p-3">
                <img src="{{asset('img/checked.svg')}}" style="width: 15px;">
                <a class="mb-0 ml-3 font-500 text-black btn btn-light bg-transparent border-0" href="../resend-code/resendCode.html">
                    is soyuncorreo@mail.com the right email address?
                </a>
            </div>
        </div>--}}
        <div class="d-flex justify-content-center">
            <div class="width-30 bg-light d-flex align-items-center mb-2 p-3">
                <img src="{{asset('img/checked.svg')}}" style="width: 15px;">
                <a class="mb-0 ml-3 font-500 text-black btn btn-light bg-transparent border-0">
                    I just remember my password,<a class="text-red font-500" href="/login">back to login</a>.
                </a>
            </div>
        </div>
        <div class="d-flex justify-content-center mb-4">
            <div class="width-30 bg-light d-flex align-items-center p-3">
                <img src="{{asset('img/checked.svg')}}" style="width: 15px;">
                <a class="mb-0 ml-3 font-500 text-black btn btn-light bg-transparent border-0" href="/login_from_social">
                    I still got no email.
                </a>
            </div>
        </div>

    </div>
    @endsection
