@extends('layouts.master')

@section('scripts')
    <link href="{{asset('css/verification.css')}}" rel="stylesheet">
@endsection
@section('content')
    <div style="margin-top: 8%;">

        <div class="d-flex justify-content-center mb-5">
            <h1 class="h1 w-75">We need to verify your email, please <span class="text-red">click on the link</span> we
                have sent to your inbox.</h1>
        </div>

        <div class="d-flex justify-content-center mb-5">
            <p class="mb-0 text-center">Some features will be unlocked after verification, meanwhile, take a look around
            </p>
        </div>

        <div class="d-flex justify-content-center">
            <form class="width-25">
                <a type="button" class="btn btn-outline-danger w-100 mt-4" href="/login">continue</a>
            </form>
        </div>

    </div>

    @endsection
