@extends('layouts.master')

@section('scripts')
    <link href="{{asset('css/login.css')}}" rel="stylesheet">
@endsection
@section('content')
    <div style="margin-top: 8%;">
        @if ($errors->any())
            @include('includes.toast',['show' => true])
        @endif
      <div class="form-signin text-center">
        <img src="{{asset('img/logo.JPG')}}">
        <p class="mb-0 font-weight-normal">log into Deductive</p>
        <p class="mb-3 font-weight-normal">or create an <a type="button" class="text-red" href="/register">account</a></p>

        <div class="login-box d-flex justify-content-center">
          <form class="w-25" method="POST">
              @csrf
              <div class="user-box">
              <input type="text" name="email" required>
              <label>email address</label>
            </div>
            <div class="user-box">
              <input type="password" name="password" required>
              <label>password</label>
            </div>

            <button type="submit" class="btn btn-outline-danger w-100 mt-4">sign up</button>
          </form>
        </div>
        <div class="mt-3">
          <button type="button" class="btn btn-light bg-white border-0">Forgot your email address?</button>
          <button type="button" class="btn btn-light bg-white border-0" onclick="window.location.href = '/reset_password'">Forgot your password?</button>
        </div>

      </div>
    </div>
    <!--end login-->
@endsection
