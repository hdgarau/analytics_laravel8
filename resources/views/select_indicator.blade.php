@extends('layouts.master')

@section('scripts')
    <script src="{{asset('js/abms/select_indicator.js')}}"></script>
    <link href="{{asset('css/selectIndicators.css')}}" rel="stylesheet">

@endsection

@section('content')
    <div style="margin-top: 5%;" id="main">
        <!-- sidebar -->
    @include('includes.platform_panel')
    <!-- end sidebar -->

        <div class="d-flex justify-content-center mb-3 text-center">
            <h1 class="h1 w-75">Choose your <span class="text-red">KPI</span></h1>
        </div>

        <div class="d-flex justify-content-center mb-5 text-center">
            <a type="button" class="btn btn-light bg-transparent border-0 text-black">ALL KPIS</a>
            <a type="button" class="btn btn-light bg-transparent border-0 text-black">ENGAGEMENT</a>
            <a type="button" class="btn btn-light bg-transparent border-0 text-black">REACH</a>
            <a type="button" class="btn btn-light bg-transparent border-0 text-black">AWARENESS</a>
            <a type="button" class="btn btn-light bg-transparent border-0 text-black">ROI</a>
            <a type="button" class="btn btn-light bg-transparent border-0 text-black">CONTENT</a>
        </div>

        <div class="container p-0 mb-5">
            <div class="row">
                <div class="col-lg-4">


                    <div class="mt-3" style="overflow-y: scroll; height: 250px;">
                        @foreach($indicators as $indicator)
                            <div>
                                <a type="button"
                                   onclick="javascript:selectIndicator({{$indicator->id}})"
                                   data-id="{{$indicator->id}}"
                                   class="btn btn-light bg-transparent border-0 text-black font-weight-bold ">{{$indicator->name}}</a>
                            </div>
                        @endforeach
                    </div>
                </div>

                <div class="col-lg-8 " >
                    <div id="indicator_description">

                    </div>
                    <div class="d-flex justify-content-center m-3">
                        <a type="button" class="btn btn-outline-danger w-25 mb-3" href="/indicator">+ setup a new KPI</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="d-flex justify-content-around mb-2 mt-5">
            <a type="button" class="btn btn-outline-danger w-25" href="/select_profiles">back</a>
            <button type="button" onclick="$('form#indicator').trigger('submit')" id="continue" class="btn btn-outline-danger w-25" href="">next</button>
            <form method="post" id="indicator">
                @csrf
            </form>
        </div>

    </div>
    @if(!empty($indicator_preselect) )
        <script>selectIndicator({{$indicator_preselect->id}})</script>
    @endif
@endsection
