@extends('layouts.master')

@section('scripts')
    <link href="{{asset('css/selectIndicators.css')}}" rel="stylesheet">

    <script>
        $(document).ready( function (event) {

            //$('li#continue').toggle($('li.selected').length > 0);

            $('form').submit(function (e) {

                $(':button.selected').each( function (i,el) {
                    //$('<input name="accounts[]">').val( JSON.stringify({'parent-id': $(el).data('parent-id'), 'id' : $(el).data('id')}).appendTo(e.target));
                });
                return true;
            });
            $('a[data-url]').click( function (e) {
                $('a[data-url]').removeClass('selected');
                $(e.target).addClass('selected');
                $('div#show_url').load('/select_time_range/' + $(e.target).data('url'));
            });
        });

    </script>
@endsection

@section('content')
    <div style="margin-top: 5%;" id="main">
        <!-- sidebar -->
    @include('includes.platform_panel')
    <!-- end sidebar -->

        <div class="d-flex justify-content-center mb-3 text-center">
            <h1 class="h1 w-75">Pick the dates to be <span class="text-red">measured</span></h1>
        </div>


        <div class="container p-0 mb-5">
            <div class="row">
                <div class="col-lg-4">


                    <div class="mt-3" style="overflow-y: scroll; height: 250px;">
                            <div>
                                <a type="button"
                                   data-url="form_past_today"
                                   class="btn btn-light bg-transparent border-0 text-black font-weight-bold">Past VS Today</a>
                            </div>

                            <div>
                                <a type="button"
                                   data-url="form_past_past"
                                   class="btn btn-light bg-transparent border-0 text-black font-weight-bold">Distant Past VS Near Past</a>
                            </div>

                    </div>
                </div>

                <div class="col-lg-8 " >
                    <form method="post" id="indicator">
                        @csrf
                        <div id="show_url">

                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="d-flex justify-content-around mb-2 mt-5">
            <a type="button" class="btn btn-outline-danger w-25" href="/select_profiles">back</a>

            <button type="button" onclick="$('form#indicator').trigger('submit')" id="continue" class="btn btn-outline-danger w-25" href="">next</button>
        </div>

    </div>


@endsection
