@extends('layouts.master')
@section('content')
    <div class="row h-75">
        <div class="col-md-4 my-auto">
        </div>
        <div class="col-md-4 my-auto" align="center">
            <h1>
                <strong>Configure KEY to OAuth</strong>
            </h1>
            <!--Card content-->
            <div class="card-body px-lg-5 pt-0">

            @if(isset($msg))
        <div class="tab tab-info">{{$msg}}</div>
    @endif
    <form method="post">
        @csrf
        <h3>Facebook</h3>
        <div class="md-form">
            <input type="text" value="{{config('services.facebook.client_id')}}" class="form-control" name="facebook_id" placeholder="Facebook ID" required>
        </div>
        <div class="md-form">
            <input type="text" value="{{config('services.facebook.client_secret')}}" class="form-control" name="facebook_secret" placeholder="Facebook Secret"  required>
        </div>
        <hr/>
        <h3>Twitter</h3>
        <div class="md-form">
            <input type="text" value="{{config('services.twitter.client_id')}}" class="form-control" name="twitter_id" placeholder="Twitter ID" required>
        </div>
        <div class="md-form">
            <input type="text" value="{{config('services.twitter.client_secret')}}" class="form-control" name="twitter_secret" placeholder="Twitter Secret"  required>
        </div>
        <hr/>
        <h3>Google</h3>
        <div class="md-form">
            <input type="text" value="{{config('services.google.client_id')}}" class="form-control" name="google_id" placeholder="Google ID" required>
        </div>
        <div class="md-form">
            <input type="text" value="{{config('services.google.client_secret')}}" class="form-control" name="google_secret" placeholder="Google Secret"  required>
        </div>
        <hr/>
        <h3>Mailchimp</h3>
        <div class="md-form">
            <input type="text" value="{{config('services.mailchimp.client_id')}}" class="form-control" name="mailchimp_id" placeholder="MailChimp ID" required>
        </div>
        <div class="md-form">
            <input type="text" value="{{config('services.mailchimp.client_secret')}}" class="form-control" name="mailchimp_secret" placeholder="MailChimp Secret"  required>
        </div>

        <hr>
        <button class="btn btn-outline-info btn-rounded btn-block my-4 waves-effect z-depth-0" type="submit">Save</button>


    </form>

            </div>
        </div>
        <div class="col-md-4 my-auto">
        </div>
    </div>

    <script>
        $(document).ready(function () {
            $('form').submit(function (e) {
                var clave = $('#new').val();
                var check = $('#new_again').val();
                if(check != clave)
                {
                    alert('La clave no coincide con la verificación');
                    e.preventDefault();
                    return 0;
                }
                if(clave.length < 5) {
                    alert('La clave dee tener al menos 6 caracteres');
                    e.preventDefault();
                    return 0;
                }
                return 1;
            })
        })
    </script>
@stop
