@extends('layouts.master')

@section('scripts')
    <script >
        $(document).ready(function (e) {
            $('div[rel="detalle"]').hide();
        })
    </script>
    <link href="{{asset('css/selectIndicators.css')}}" rel="stylesheet">

@endsection

@section('content')
    <div style="margin-top: 5%;" id="main">
        <!-- sidebar -->
    @include('includes.platform_panel')
    <!-- end sidebar -->

        <div class="d-flex justify-content-center mb-3 text-center">
            <h1 class="h1 w-75">Results</h1>
        </div>

        <div class="container p-0 mb-5">
            <h1>Past: {{$date_past_since}} to {{$date_past_until}}</h1>
            @foreach( $dataPast as $platform_name => $data)
                @include( 'includes.results.' . strtolower(str_replace(' ','',$platform_name)),
                    ['data' => current($data), 'platform_name' => $platform_name, 'max_metric_platform' => $max_metric[$platform_name] ])
            @endforeach
            <hr/>
            <h1>Recent Past / Present: {{$date_recent_since}} to {{$date_recent_until}}</h1>
            @foreach( $dataRecent as $platform_name => $data)
                @include( 'includes.results.' . strtolower(str_replace(' ','',$platform_name)),
                    ['data' => current($data), 'title' => $platform_name, 'max_metric_platform' => $max_metric[$platform_name] ])
            @endforeach
            <div class="d-flex justify-content-around mb-2 mt-5">
                <a type="button" class="btn btn-outline-danger w-25" href="/select_key_words">back</a>
                <form method="post" id="indicator">
                    @csrf
                </form>
            </div>
        </div>
    </div>
@endsection
