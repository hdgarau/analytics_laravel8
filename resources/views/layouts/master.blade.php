<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="generator" content="Jekyll v4.1.1">
    <title>{{config('app.name')}}</title>
    <link rel="canonical" href="https://getbootstrap.com/docs/4.5/examples/sign-in/">

    <!-- Bootstrap core CSS -->
    <link href="{{asset('dist/css/bootstrap.min.css')}}" rel="stylesheet">
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <!-- Custom styles for this template -->
    @yield('scripts')
  </head>

  <body>
    <!--header-->
    <nav class="navbar navbar-expand-lg navbar-light bg-white justify-content-between " >
      <a class="navbar-brand"><img src="{{asset('/img/deductive-logo.JPG')}}" alt="logo"></a>

      <div>
          <button type="button" class="btn btn-light bg-white border-0 text-black">ABOUT</button>
          @if(!auth()->check() )
              <a type="button" class="btn btn-light bg-white border-0 text-black" href="/login">SIGN IN</a>
          @else
              <button type="button" class="btn btn-light bg-white border-0 text-black"><a class="btn btn-light bg-white border-0 text-black" href="/logout">LOG OUT</a></button>
              <button type="button" class="btn btn-light bg-white border-0 text-black"><a class="btn btn-light bg-white border-0 text-black" href="/home">HOME</a></button>
              @can('is-admin')
                  <div class="btn-group">
                      <a class=" dropdown-toggle nav-link" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          ADMIN
                      </a>
                      <div class="dropdown-menu">
{{--                          <a class="dropdown-item" class="btn btn-light bg-white border-0 text-black" href="/users">Users</a>--}}
                          <a class="dropdown-item" class="btn btn-light bg-white border-0 text-black" href="/metrics">Metrics</a>
                          <div class="dropdown-divider"></div>
                          <a class="dropdown-item" class="btn btn-light bg-white border-0 text-black" href="/configuration">Configure KEYs</a>
                      </div>
                  </div>
              @endcan
          @endif
      </div>
    </nav>
    <!--end header-->
    @yield('content')
  </body>
</html>
