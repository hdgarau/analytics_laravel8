<h1>Activate account</h1>
<p>
    <a href="{{URL::to('/user/activate/' . base64_encode(encrypt($user->id)))}}">Click here</a> to activate your account
</p>
