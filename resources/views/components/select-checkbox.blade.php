<div class="form-group col-md-3">
    @foreach($options as $option)
        <input type="checkbox" name="{{$name}}[]" {{ $isSelected($option->value) ? 'checked="checked"' : '' }} value="{{ $option->value }}">
        {{ $option->label }}    <br>
    @endforeach
</div>
