<div>
    <select name="{{$name}}" class="form-control" >
        <option></option>
        @foreach($options as $option)
            <option {{ $isSelected($option->value) ? 'selected="selected"' : '' }} value="{{ $option->value}}">
                {{ $option->label }}
            </option>
        @endforeach
    </select>
</div>
