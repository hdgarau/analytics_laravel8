<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIndicatorMetricsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('indicator_metrics', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('metric_id')->unsigned();
            $table->integer('indicator_id')->unsigned();
            $table->timestamps();
        });

        Schema::table('label_indicators', function($table) {
           // $table->foreign('metric_id')->references('id')->on('metrics');
            //$table->foreign('indicator_id')->references('id')->on('indicators');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('indicator_metrics');
    }
}
