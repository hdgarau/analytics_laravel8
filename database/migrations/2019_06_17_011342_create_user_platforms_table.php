<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserPlatformsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_platforms', function (Blueprint $table) {
            $table->integer('user_id');
            $table->integer('platform_id')->unsigned();
            $table->string('platform_foreign_id',255);
            $table->text('token');//->unique();
            $table->text('token_secret')->nullable();
            $table->text('refresh_token')->nullable();
            $table->text('server')->nullable(); //only mailchimp
            $table->bigIncrements('id');
            $table->timestamps();
        });
        Schema::table('user_platforms', function($table)
        {
            $table->foreign('platform_id')->references('id')->on('platforms');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_platforms');
    }
}
