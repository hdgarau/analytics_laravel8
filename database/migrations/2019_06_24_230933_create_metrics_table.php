<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMetricsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('metrics', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->json('format_names');
            $table->json('objective_names');
            $table->integer('platform_id')->unsigned();
            $table->text('description')->nullable();
            $table->text('periods')->nullable();
            $table->timestamps();
        });
        Schema::table('metrics', function($table) {
            //$table->foreign('platform_id')->references('id')->on('platforms');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('metrics');
    }
}
