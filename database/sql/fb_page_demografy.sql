INSERT INTO `metrics`
(`name`,
`description`,
periods,
`format_names`,
`objective_names`,
`platform_id`)
VALUES
('page_fans','Número total de personas que indicaron que les gusta tu página.','day','"[\\"page\\"]"','"[\\"engagement\\"]"',1),

('page_fans_locale','Datos de idioma totales de las personas a las que les gusta tu página según la configuración de idioma predeterminado que seleccionaron al acceder a Facebook.','day','"[\\"page\\"]"','"[\\"engagement\\"]"',1),

('page_fans_city','Datos de ubicación de Facebook totales, clasificados por ciudad, sobre las personas a las que les gusta tu página.','day','"[\\"page\\"]"','"[\\"engagement\\"]"',1),

('page_fans_country','Número de personas, por países, a las que les gusta tu página. Solo se incluyen los 45 países en los que haya más personas a las que les guste tu página.','day','"[\\"page\\"]"','"[\\"engagement\\"]"',1),

('page_fans_gender_age','Número de personas que vieron alguna de tus publicaciones al menos una vez, agrupadas por edad y sexo. Los datos demográficos totales se basan en una serie de factores, como la información de edad y sexo que proporcionan los usuarios en sus perfiles de Facebook. Esta cifra es una estimación.','day','"[\\"page\\"]"','"[\\"engagement\\"]"',1),

('page_fan_adds','Número de personas nuevas que indicaron que les gusta tu página.','day','"[\\"page\\"]"','"[\\"engagement\\"]"',1),

('page_fan_adds_unique','Número de personas nuevas que indicaron que les gusta tu página.','day, week, days_28','"[\\"page\\"]"','"[\\"engagement\\"]"',1),

('page_fans_by_like_source','Este es un desglose del número de Me gusta de la página de los lugares más comunes en los que las personas pueden indicar que les gusta tu página. (See possible types)','day','"[\\"page\\"]"','"[\\"engagement\\"]"',1),

('page_fans_by_like_source_unique','Número de personas que indicaron que les gusta tu página, desglosado por los lugares más comunes en donde las personas pueden indicar que les gusta tu página. (See possible types)','day','"[\\"page\\"]"','"[\\"engagement\\"]"',1),

('page_fan_removes','"Ya no me gusta" de tu página.','day','"[\\"page\\"]"','"[\\"engagement\\"]"',1),

('page_fan_removes_unique','"Ya no me gusta" de tu página.','day, week, days_28','"[\\"page\\"]"','"[\\"engagement\\"]"',1),

('page_fans_by_unlike_source_unique','Número de personas a las que ya no les gusta tu página, desglosado por las formas más comunes en las que las personas pueden indicar que ya no les gusta tu página.','day','"[\\"page\\"]"','"[\\"engagement\\"]"',1);
