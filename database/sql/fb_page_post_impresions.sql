INSERT INTO `metrics`
(`name`,
`description`,
periods,
`format_names`,
`objective_names`,
`platform_id`)
VALUES
('post_impressions','Número de veces que se mostró en la pantalla de una persona la publicación de tu página. Las publicaciones incluyen estados, fotos, enlaces, videos y más.','lifetime','"[\\"post\\"]"','"[\\"engagement\\"]"',1),

('post_impressions_unique','Número de personas a las que se mostró en pantalla la publicación de tu página. Las publicaciones incluyen estados, fotos, enlaces, videos y más.','lifetime','"[\\"post\\"]"','"[\\"engagement\\"]"',1),

('post_impressions_paid','Número de veces que se mostró en la pantalla de una persona la publicación de tu página a través de distribución pagada, como un anuncio.','lifetime','"[\\"post\\"]"','"[\\"engagement\\"]"',1),

('post_impressions_paid_unique','Número de personas a las que se mostró en pantalla la publicación de tu página a través de distribución pagada, como un anuncio.','lifetime','"[\\"post\\"]"','"[\\"engagement\\"]"',1),

('post_impressions_fan','Número de impresiones en la publicación de tu página generadas por personas que indicaron que les gusta tu página.','lifetime','"[\\"post\\"]"','"[\\"engagement\\"]"',1),

('post_impressions_fan_unique','Número de personas que indicaron que les gusta tu página y vieron la publicación de tu página.','lifetime','"[\\"post\\"]"','"[\\"engagement\\"]"',1),

('post_impressions_fan_paid','Número de impresiones en la publicación de tu página generadas por personas que indicaron que les gusta tu página en un anuncio o una historia patrocinada.','lifetime','"[\\"post\\"]"','"[\\"engagement\\"]"',1),

('post_impressions_fan_paid_unique','Número de personas que indicaron que les gusta tu página y vieron la publicación de tu página en un anuncio o una historia patrocinada.','lifetime','"[\\"post\\"]"','"[\\"engagement\\"]"',1),

('post_impressions_organic','Número de veces que se mostraron en la pantalla de una persona las publicaciones de tu página a través de distribución no pagada.','lifetime','"[\\"post\\"]"','"[\\"engagement\\"]"',1),

('post_impressions_organic_unique','Número de personas a las que se mostró en pantalla la publicación de tu página a través de distribución no pagada.','lifetime','"[\\"post\\"]"','"[\\"engagement\\"]"',1),

('post_impressions_viral','Número de veces que se mostró en la pantalla de una persona la publicación de tu página con información social asociada. Se muestra información social cuando el amigo de una persona interactúa con tu página o publicación, por ejemplo, indica que le gusta tu página o la sigue, interactúa con una publicación, comparte una foto de tu página o registra una visita en ella.','lifetime','"[\\"post\\"]"','"[\\"engagement\\"]"',1),

('post_impressions_viral_unique','Número de personas a las que se mostró en pantalla la publicación de tu página con información social asociada. Como forma de distribución orgánica, se muestra información social cuando el amigo de una persona interactúa con tu página o publicación, por ejemplo, indica que le gusta tu página o la sigue, interactúa con una publicación, comparte una foto de tu página o registra una visita en ella.','lifetime','"[\\"post\\"]"','"[\\"engagement\\"]"',1),

('post_impressions_nonviral','Número de veces que se mostró en la pantalla de una persona la publicación de tu página. No incluye contenido creado sobre tu página con información social asociada. Se muestra información social cuando el amigo de una persona interactúa con tu página o publicación, por ejemplo, indica que le gusta tu página o la sigue, interactúa con una publicación, comparte una foto de tu página o registra una visita en ella.','lifetime','"[\\"post\\"]"','"[\\"engagement\\"]"',1),

('post_impressions_nonviral_unique','Número de personas a las que se mostró en pantalla la publicación de tu página. No incluye contenido creado sobre tu página con información social asociada. Como forma de distribución orgánica, se muestra información social cuando el amigo de una persona interactúa con tu página o publicación, por ejemplo, indica que le gusta tu página o la sigue, interactúa con una publicación, comparte una foto de tu página o registra una visita en ella.','lifetime','"[\\"post\\"]"','"[\\"engagement\\"]"',1),

('post_impressions_by_story_type','Número de veces que se vio esta publicación a través de una historia publicada por un amigo de la persona que ve la publicación.','lifetime','"[\\"post\\"]"','"[\\"engagement\\"]"',1),

('post_impressions_by_story_type_unique','Número de personas que vieron la publicación de tu página en una historia de un amigo, por tipo de historia.','lifetime','"[\\"post\\"]"','"[\\"engagement\\"]"',1);
