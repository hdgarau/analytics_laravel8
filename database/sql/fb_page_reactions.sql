INSERT INTO `metrics`
(`name`,
`description`,
periods,
`format_names`,
`objective_names`,
`platform_id`)
VALUES
('page_actions_post_reactions_like_total','Total de reacciones "Me gusta" diarias en las publicaciones de una página.','day, week, days_28','"[\\"page\\"]"','"[\\"engagement\\"]"',1),

('page_actions_post_reactions_love_total','Total de reacciones "Me encanta" diarias en las publicaciones de una página.','day, week, days_28','"[\\"page\\"]"','"[\\"engagement\\"]"',1),

('page_actions_post_reactions_wow_total','Total de reacciones "Me asombra" diarias en las publicaciones de una página.','day, week, days_28','"[\\"page\\"]"','"[\\"engagement\\"]"',1),

('page_actions_post_reactions_haha_total','Total de reacciones "Me divierte" diarias en las publicaciones de una página.','day, week, days_28','"[\\"page\\"]"','"[\\"engagement\\"]"',1),

('page_actions_post_reactions_sorry_total','Total de reacciones "Me entristece" diarias en las publicaciones de una página.','day, week, days_28','"[\\"page\\"]"','"[\\"engagement\\"]"',1),

('page_actions_post_reactions_anger_total','Total de reacciones "Me enoja" diarias en las publicaciones de una página.','day, week, days_28','"[\\"page\\"]"','"[\\"engagement\\"]"',1),

('page_actions_post_reactions_total','Total de reacciones diarias a las publicaciones de una página por tipo.','day','"[\\"page\\"]"','"[\\"engagement\\"]"',1);
