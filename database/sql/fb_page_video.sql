INSERT INTO `metrics`
(`name`,
`description`,
periods,
`format_names`,
`objective_names`,
`platform_id`)
VALUES
('page_video_views','Número de veces que se reprodujeron los videos de tu página durante al menos 3 segundos, o casi en su totalidad si duran menos de 3 segundos. En una única instancia de reproducción de video, se excluirá el tiempo de las reproducciones repetidas.','day, week, days_28','"[\\"page\\"]"','"[\\"engagement\\"]"',1),

('page_video_views_paid','Número de veces que se reprodujeron los videos promocionados de tu página durante al menos 3 segundos, o casi en su totalidad si duran menos de 3 segundos. Para cada impresión de un video, se contarán las reproducciones por separado y se excluirá el tiempo de las reproducciones repetidas.','day, week, days_28','"[\\"page\\"]"','"[\\"engagement\\"]"',1),

('page_video_views_organic','Número de veces que se reprodujeron los videos de tu página durante al menos 3 segundos, o casi en su totalidad si duran menos de 3 segundos, mediante alcance orgánico. En una única instancia de reproducción de video, se excluirá el tiempo de las reproducciones repetidas.','day, week, days_28','"[\\"page\\"]"','"[\\"engagement\\"]"',1),

('page_video_views_by_paid_non_paid','Número de veces que se reprodujeron los videos de tu página durante al menos 3 segundos, o casi en su totalidad si duran menos de 3 segundos, desglosado por totales, pagadas y no pagadas. En una única instancia de reproducción de video, se excluirá el tiempo de las reproducciones repetidas.','day, week, days_28','"[\\"page\\"]"','"[\\"engagement\\"]"',1),

('page_video_views_autoplayed','Número de veces que se reprodujeron los videos de tu página automáticamente durante al menos 3 segundos, o casi en su totalidad si duran menos de 3 segundos. En una única instancia de reproducción de video, se excluirá el tiempo de las reproducciones repetidas.','day, week, days_28','"[\\"page\\"]"','"[\\"engagement\\"]"',1),

('page_video_views_click_to_play','Número de veces que se reprodujeron los videos de tu página durante al menos 3 segundos, o casi en su totalidad si duran menos de 3 segundos, después de que las personas hicieron clic para reproducirlos. En una única instancia de reproducción de video, se excluirá el tiempo de las reproducciones repetidas.','day, week, days_28','"[\\"page\\"]"','"[\\"engagement\\"]"',1),

('page_video_views_unique','Número de personas que vieron los videos de tu página durante al menos 3 segundos, o casi en su totalidad si duran menos de 3 segundos. En una única instancia de reproducción de video, se excluirá el tiempo de las reproducciones repetidas.','day, week, days_28','"[\\"page\\"]"','"[\\"engagement\\"]"',1),

('page_video_repeat_views','Número de veces que se volvieron a reproducir los videos de tu página durante al menos 3 segundos, o casi en su totalidad si duran menos de 3 segundos.','day, week, days_28','"[\\"page\\"]"','"[\\"engagement\\"]"',1),

('page_video_complete_views_30s','Número de veces que se reprodujeron los videos de tu página durante al menos 30 segundos, o casi en su totalidad si duran menos de 30 segundos. En una única instancia de reproducción de video, se excluirá el tiempo de las reproducciones repetidas.','day, week, days_28','"[\\"page\\"]"','"[\\"engagement\\"]"',1),

('page_video_complete_views_30s_paid','Número de veces que se reprodujeron los videos promocionados de tu página durante al menos 30 segundos, o casi en su totalidad si duran menos de 30 segundos. Para cada impresión de un video, se contarán las reproducciones por separado y se excluirá el tiempo de las reproducciones repetidas.','day, week, days_28','"[\\"page\\"]"','"[\\"engagement\\"]"',1),

('page_video_complete_views_30s_organic','Número de veces que se reprodujeron los videos de tu página durante al menos 30 segundos, o casi en su totalidad si duran menos de 30 segundos, mediante alcance orgánico. En una única instancia de reproducción de video, se excluirá el tiempo de las reproducciones repetidas.','day, week, days_28','"[\\"page\\"]"','"[\\"engagement\\"]"',1),

('page_video_complete_views_30s_autoplayed','Número de veces que se reprodujeron los videos de tu página automáticamente durante al menos 30 segundos, o casi en su totalidad si duran menos de 30 segundos. En una única instancia de reproducción de video, se excluirá el tiempo de las reproducciones repetidas.','day, week, days_28','"[\\"page\\"]"','"[\\"engagement\\"]"',1),

('page_video_complete_views_30s_click_to_play','Número de veces que se reprodujeron los videos de tu página durante al menos 30 segundos, o casi en su totalidad si duran menos de 30 segundos, después de que las personas hicieron clic para reproducirlos. En una única instancia de reproducción de video, se excluirá el tiempo de las reproducciones repetidas.','day, week, days_28','"[\\"page\\"]"','"[\\"engagement\\"]"',1),

('page_video_complete_views_30s_unique','Número de personas que reprodujeron los videos de tu página durante al menos 30 segundos, o casi en su totalidad si duran menos de 30 segundos. En una única instancia de reproducción de video, se excluirá el tiempo de las reproducciones repetidas.','day, week, days_28','"[\\"page\\"]"','"[\\"engagement\\"]"',1),

('page_video_complete_views_30s_repeat_views','Número de veces que se volvieron a reproducir los videos de tu página durante al menos 30 segundos, o casi en su totalidad si duran menos de 30 segundos.','day, week, days_28','"[\\"page\\"]"','"[\\"engagement\\"]"',1);
