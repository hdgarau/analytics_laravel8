INSERT INTO `metrics`
(`name`,
`description`,
periods,
`format_names`,
`objective_names`,
`platform_id`)
VALUES
('page_views_total','Número de veces que personas que iniciaron sesión y no iniciaron sesión vieron el perfil de una página.','day, week, days_28','"[\\"page\\"]"','"[\\"engagement\\"]"',1),

('page_views_logout','Número de veces que personas que no iniciaron sesión en Facebook vieron el perfil de una página.','day','"[\\"page\\"]"','"[\\"engagement\\"]"',1),

('page_views_logged_in_total','Número de veces que personas que iniciaron sesión en Facebook vieron el perfil de una página.','day, week, days_28','"[\\"page\\"]"','"[\\"engagement\\"]"',1),

('page_views_logged_in_unique','Número de personas con sesión iniciada en Facebook que vieron el perfil de la página.','day, week, days_28','"[\\"page\\"]"','"[\\"engagement\\"]"',1),

('page_views_external_referrals','Principales dominios externos de referencia que envían tráfico a tu página.','day','"[\\"page\\"]"','"[\\"engagement\\"]"',1),

('page_views_by_profile_tab_total','Número de personas que visitaron cada pestaña del perfil de la página.','day, week, days_28','"[\\"page\\"]"','"[\\"engagement\\"]"',1),

('page_views_by_profile_tab_logged_in_unique','Número de personas con sesión iniciada en Facebook que vieron el perfil de tu página, desglosado por pestaña.','day, week, days_28','"[\\"page\\"]"','"[\\"engagement\\"]"',1),

('page_views_by_internal_referer_logged_in_unique','Número de personas con sesión iniciada en Facebook que vieron tu página, desglosado por referencia interna en Facebook.','day, week, days_28','"[\\"page\\"]"','"[\\"engagement\\"]"',1),

('page_views_by_site_logged_in_unique','Número de personas con sesión iniciada en Facebook que vieron el perfil de tu página, desglosado por el tipo de dispositivo.','day, week, days_28','"[\\"page\\"]"','"[\\"engagement\\"]"',1),

('page_views_by_age_gender_logged_in_unique','Número de personas con sesión iniciada en Facebook que vieron el perfil de tu página, desglosado por grupo de sexo.','day, week, days_28','"[\\"page\\"]"','"[\\"engagement\\"]"',1),

('page_views_by_referers_logged_in_unique','Número de visitas a la página de usuarios conectados (usuarios únicos) por origen de referencia.','day, week','"[\\"page\\"]"','"[\\"engagement\\"]"',1);
