INSERT INTO `metrics`
(`name`,
`description`,
periods,
`format_names`,
`objective_names`,
`platform_id`)
VALUES
('page_tab_views_login_top_unique','Content: Número de usuarios que iniciaron sesión en Facebook y vieron las pestañas de tu página. (See possible types)','day, week','"[\\"page\\"]"','"[\\"recordacion_de_marca\\"]"',1),
('page_tab_views_login_top','Content: Número de veces que usuarios que iniciaron sesión en Facebook vieron pestañas de tu página. (See possible types)','day, week','"[\\"page\\"]"','"[\\"recordacion_de_marca\\"]"',1),
('page_tab_views_logout_top','Content: Número de veces que usuarios que no iniciaron sesión en Facebook vieron pestañas de tu página. (See possible types)','day','"[\\"page\\"]"','"[\\"recordacion_de_marca\\"]"',1)
