INSERT INTO `metrics`
(`name`,
`description`,
periods,
`format_names`,
`objective_names`,
`platform_id`)
VALUES
('post_reactions_like_total','Número total de reacciones "Me gusta" de una publicación.','lifetime','"[\\"post\\"]"','"[\\"engagement\\"]"',1),

('post_reactions_love_total','Número total de reacciones "Me encanta" de una publicación.','lifetime','"[\\"post\\"]"','"[\\"engagement\\"]"',1),

('post_reactions_wow_total','Número total de reacciones "Me asombra" de una publicación.','lifetime','"[\\"post\\"]"','"[\\"engagement\\"]"',1),

('post_reactions_haha_total','Número total de reacciones "Me divierte" de una publicación.','lifetime','"[\\"post\\"]"','"[\\"engagement\\"]"',1),

('post_reactions_sorry_total','Número total de reacciones "Me entristece" de una publicación.','lifetime','"[\\"post\\"]"','"[\\"engagement\\"]"',1),

('post_reactions_anger_total','Número total de reacciones "Me enoja" de una publicación.','lifetime','"[\\"post\\"]"','"[\\"engagement\\"]"',1),

('post_reactions_by_type_total','Número total de reacciones a la publicación por tipo.','lifetime','"[\\"post\\"]"','"[\\"engagement\\"]"',1);
