INSERT INTO `metrics`
(`name`,
`description`,
 `periods`,
`format_names`,
`objective_names`,
`platform_id`)
VALUES
('page_total_actions','Número de clics en la información de contacto de tu página y el botón de llamada a la acción.','day, week, days_28','"[\\"page\\"]"','"[\\"recordacion_de_marca\\"]"',1),

('page_cta_clicks_logged_in_total','Número total de clics en el botón de llamada a la acción de la página realizados por personas que iniciaron sesión en Facebook.','day, week, days_28','"[\\"page\\"]"','"[\\"recordacion_de_marca\\"]"',1),

('page_cta_clicks_logged_in_unique','Número de clics únicos en el botón de llamada a la acción de la página por parte de personas que iniciaron sesión en Facebook.','day, week, days_28','"[\\"page\\"]"','"[\\"recordacion_de_marca\\"]"',1),

('page_cta_clicks_by_site_logged_in_unique','Número de personas con sesión iniciada en Facebook que hicieron clic en el botón de llamada a la acción, desglosado por sitio web, celulares, API, etc.','day, week, days_28','"[\\"page\\"]"','"[\\"recordacion_de_marca\\"]"',1),

('page_cta_clicks_by_age_gender_logged_in_unique','Número de personas con sesión iniciada en Facebook que hicieron clic en el botón de llamada a la acción de la página, desglosado por grupo de edad y sexo.','day, week, days_28','"[\\"page\\"]"','"[\\"recordacion_de_marca\\"]"',1),

('page_cta_clicks_logged_in_by_country_unique','Número de personas con sesión iniciada en Facebook que hicieron clic en el botón de llamada a la acción de la página, desglosado por país.','day, week','"[\\"page\\"]"','"[\\"recordacion_de_marca\\"]"',1),

('page_cta_clicks_logged_in_by_city_unique','Número de personas con sesión iniciada en Facebook que hicieron clic en el botón de llamada a la acción de la página, desglosado por ciudad.','day, week','"[\\"page\\"]"','"[\\"recordacion_de_marca\\"]"',1),

('page_call_phone_clicks_logged_in_unique','Número de personas que iniciaron sesión en Facebook e hicieron clic en el botón "Llamar".','day, week, days_28','"[\\"page\\"]"','"[\\"recordacion_de_marca\\"]"',1),

('page_call_phone_clicks_by_age_gender_logged_in_unique','Número de personas que iniciaron sesión en Facebook e hicieron clic en el botón "Llamar", desglosado por grupo de edad y sexo.','day, week, days_28','"[\\"page\\"]"','"[\\"recordacion_de_marca\\"]"',1),

('page_call_phone_clicks_logged_in_by_country_unique','Número de personas que iniciaron sesión en Facebook e hicieron clic en el botón "Llamar", desglosado por país.','day, week','"[\\"page\\"]"','"[\\"recordacion_de_marca\\"]"',1),

('page_call_phone_clicks_logged_in_by_city_unique','Número de personas que iniciaron sesión en Facebook e hicieron clic en el botón "Llamar", desglosado por ciudad.','day, week','"[\\"page\\"]"','"[\\"recordacion_de_marca\\"]"',1),

('page_call_phone_clicks_by_site_logged_in_unique','Número de personas que hicieron clic en el número de teléfono o el botón "Llamar" de tu página mientras tenían la sesión iniciada en Facebook, desglosado por el tipo de dispositivo que usaron.','day, week, days_28','"[\\"page\\"]"','"[\\"recordacion_de_marca\\"]"',1),

('page_get_directions_clicks_logged_in_unique','Número de personas que iniciaron sesión en Facebook e hicieron clic en el botón "Cómo llegar".','day, week, days_28','"[\\"page\\"]"','"[\\"recordacion_de_marca\\"]"',1),

('page_get_directions_clicks_by_age_gender_logged_in_unique','Número de personas que iniciaron sesión en Facebook e hicieron clic en el botón "Cómo llegar", desglosado por grupo de edad y sexo.','day, week, days_28','"[\\"page\\"]"','"[\\"recordacion_de_marca\\"]"',1),

('page_get_directions_clicks_logged_in_by_country_unique','Número de personas que iniciaron sesión en Facebook e hicieron clic en el botón "Cómo llegar", desglosado por país.','day, week','"[\\"page\\"]"','"[\\"recordacion_de_marca\\"]"',1),

('page_get_directions_clicks_logged_in_by_city_unique','Número de personas que iniciaron sesión en Facebook e hicieron clic en el botón "Cómo llegar", desglosado por ciudad.','day, week','"[\\"page\\"]"','"[\\"recordacion_de_marca\\"]"',1),

('page_get_directions_clicks_by_site_logged_in_unique','Número de personas que iniciaron sesión en Facebook e hicieron clic en el botón "Cómo llegar", desglosado por sitio web, celulares, API, etc.','day, week, days_28','"[\\"page\\"]"','"[\\"recordacion_de_marca\\"]"',1),

('page_website_clicks_logged_in_unique','Número de personas que iniciaron sesión en Facebook e hicieron clic en el botón de llamada a la acción "Ir al sitio web".','day, week, days_28','"[\\"page\\"]"','"[\\"recordacion_de_marca\\"]"',1),

('page_website_clicks_by_age_gender_logged_in_unique','Número de personas que iniciaron sesión en Facebook e hicieron clic en el botón de llamada a la acción "Ir al sitio web", desglosado por grupo de edad y sexo.','day, week, days_28','"[\\"page\\"]"','"[\\"recordacion_de_marca\\"]"',1),

('page_website_clicks_logged_in_by_country_unique','Número de personas que iniciaron sesión en Facebook e hicieron clic en el botón de llamada a la acción "Ir al sitio web", desglosado por país.','day, week','"[\\"page\\"]"','"[\\"recordacion_de_marca\\"]"',1),

('page_website_clicks_logged_in_by_city_unique','Número de personas que iniciaron sesión en Facebook e hicieron clic en el botón de llamada a la acción "Ir al sitio web", desglosado por ciudad.','day, week','"[\\"page\\"]"','"[\\"recordacion_de_marca\\"]"',1),

('page_website_clicks_by_site_logged_in_unique','Número de personas que iniciaron sesión en Facebook e hicieron clic en el botón de llamada a la acción de la página, desglosado por sitio web, celulares, API, etc.','day, week, days_28','"[\\"page\\"]"','"[\\"recordacion_de_marca\\"]"',1)

