INSERT INTO `metrics`
(`name`,
`description`,
periods,
`format_names`,
`objective_names`,
`platform_id`)
VALUES
('page_video_views_10s','Número de veces que se reprodujeron los videos de tu página durante al menos 10 segundos, o casi en su totalidad si duran menos de 10 segundos. En una única instancia de reproducción de video, se excluirá el tiempo de las reproducciones repetidas.','day, week, days_28','"[\\"page\\"]"','"[\\"engagement\\"]"',1),

('page_video_views_10s_paid','Número de veces que se reprodujeron los videos promocionados de tu página durante al menos 10 segundos, o casi en su totalidad si duran menos de 10 segundos. Para cada impresión de un video, se contarán las reproducciones por separado y se excluirá el tiempo de las reproducciones repetidas.','day, week, days_28','"[\\"page\\"]"','"[\\"engagement\\"]"',1),

('page_video_views_10s_organic','Número de veces que se reprodujeron los videos de tu página durante al menos 10 segundos, o casi en su totalidad si duran menos de 10 segundos, mediante alcance orgánico. En una única instancia de reproducción de video, se excluirá el tiempo de las reproducciones repetidas.','day, week, days_28','"[\\"page\\"]"','"[\\"engagement\\"]"',1),

('page_video_views_10s_autoplayed','Número de veces que se reprodujeron los videos de tu página automáticamente durante al menos 10 segundos, o casi en su totalidad si duran menos de 10 segundos. En una única instancia de reproducción de video, se excluirá el tiempo de las reproducciones repetidas.','day, week, days_28','"[\\"page\\"]"','"[\\"engagement\\"]"',1),

('page_video_views_10s_click_to_play','Número de veces que se reprodujeron los videos de tu página durante al menos 10 segundos, o casi en su totalidad si duran menos de 10 segundos, después de que las personas hicieron clic para reproducirlos. En una única instancia de reproducción de video, se excluirá el tiempo de las reproducciones repetidas.','day, week, days_28','"[\\"page\\"]"','"[\\"engagement\\"]"',1),

('page_video_views_10s_unique','Número de personas que vieron los videos de tu página durante al menos 10 segundos, o casi en su totalidad si duran menos de 10 segundos. En una única instancia de reproducción de video, se excluirá el tiempo de las reproducciones repetidas.','day, week, days_28','"[\\"page\\"]"','"[\\"engagement\\"]"',1),

('page_video_views_10s_repeat','Número de veces que se volvieron a reproducir los videos de tu página durante al menos 10 segundos, o casi en su totalidad si duran menos de 10 segundos.','day, week, days_28','"[\\"page\\"]"','"[\\"engagement\\"]"',1),

('page_video_view_time','Tiempo total, en milisegundos, que las personas vieron el video de tu página.','day','"[\\"page\\"]"','"[\\"engagement\\"]"',1);
