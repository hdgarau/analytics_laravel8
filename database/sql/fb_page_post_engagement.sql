INSERT INTO `metrics`
(`name`,
`description`,
periods,
`format_names`,
`objective_names`,
`platform_id`)
VALUES
('page_posts_impressions','Número de veces que se mostraron en la pantalla de una persona las publicaciones de tu página, incluidos estados, fotos, enlaces, videos y más.','day, week, days_28','"[\\"page\\"]"','"[\\"engagement\\"]"',1),

('page_posts_impressions_unique','Número de personas a las que se mostró en pantalla alguna publicación de tu página, incluidos estados, fotos, enlaces, videos y más.','day, week, days_28','"[\\"page\\"]"','"[\\"engagement\\"]"',1),

('page_posts_impressions_paid','Número de veces que se mostraron en la pantalla de una persona las publicaciones de tu página a través de distribución pagada, como un anuncio.','day, week, days_28','"[\\"page\\"]"','"[\\"engagement\\"]"',1),

('page_posts_impressions_paid_unique','Número de personas a las que se mostró en pantalla alguna publicación de tu página a través de distribución pagada, como un anuncio.','day, week, days_28','"[\\"page\\"]"','"[\\"engagement\\"]"',1),

('page_posts_impressions_organic','Número de veces que se mostraron en la pantalla de una persona las publicaciones de tu página a través de distribución no pagada.','day, week, days_28','"[\\"page\\"]"','"[\\"engagement\\"]"',1),

('page_posts_impressions_organic_unique','Número de personas a las que se mostró en pantalla alguna publicación de tu página a través de distribución no pagada.','day, week, days_28','"[\\"page\\"]"','"[\\"engagement\\"]"',1),

('page_posts_served_impressions_organic_unique','Número de personas a las que se entregaron las publicaciones de tu página en la sección de noticias, independientemente de si se mostraron en pantalla o no. Las publicaciones incluyen estados, fotos, enlaces, videos y más.','day, week, days_28','"[\\"page\\"]"','"[\\"engagement\\"]"',1),

('page_posts_impressions_viral','Número de veces que se mostraron en la pantalla de una persona las publicaciones de tu página con información social asociada. Se muestra información social cuando el amigo de una persona interactúa con tu página o publicación, por ejemplo, indica que le gusta tu página o la sigue, interactúa con una publicación, comparte una foto de tu página o registra una visita en ella.','day, week, days_28','"[\\"page\\"]"','"[\\"engagement\\"]"',1),

('page_posts_impressions_viral_unique','Número de personas a las que se mostró en pantalla alguna publicación de tu página con información social asociada. Como forma de distribución orgánica, se muestra información social cuando el amigo de una persona interactúa con tu página o publicación, por ejemplo, indica que le gusta tu página o la sigue, interactúa con una publicación, comparte una foto de tu página o registra una visita en ella.','day, week, days_28','"[\\"page\\"]"','"[\\"engagement\\"]"',1),

('page_posts_impressions_nonviral','Número de veces que se mostraron en la pantalla de una persona las publicaciones de tu página. No incluye contenido creado sobre tu página con información social asociada. Se muestra información social cuando el amigo de una persona interactúa con tu página o publicación, por ejemplo, indica que le gusta tu página o la sigue, interactúa con una publicación, comparte una foto de tu página o registra una visita en ella.','day, week, days_28','"[\\"page\\"]"','"[\\"engagement\\"]"',1),

('page_posts_impressions_nonviral_unique','Número de personas a las que se mostró en pantalla alguna publicación de tu página. No incluye contenido creado sobre tu página con información social asociada. Como forma de distribución orgánica, se muestra información social cuando el amigo de una persona interactúa con tu página o publicación, por ejemplo, indica que le gusta tu página o la sigue, interactúa con una publicación, comparte una foto de tu página o registra una visita en ella.','day, week, days_28','"[\\"page\\"]"','"[\\"engagement\\"]"',1),

('page_posts_impressions_frequency_distribution','Número de personas que vieron las publicaciones de tu página, desglosado por el número de veces que las personas vieron tus publicaciones.','day, week, days_28','"[\\"page\\"]"','"[\\"engagement\\"]"',1);
