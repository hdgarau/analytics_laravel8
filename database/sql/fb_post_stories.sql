INSERT INTO `metrics`
(`name`,
`description`,
periods,
`format_names`,
`objective_names`,
`platform_id`)
VALUES

('post_activity','Número de historias generadas acerca de la publicación de tu página ("historias").','lifetime','"[\\"post\\"]"','"[\\"engagement\\"]"',1),

('post_activity_unique','Número de personas que crearon una historia sobre la publicación de tu página ("personas que están hablando de esto").','lifetime','"[\\"post\\"]"','"[\\"engagement\\"]"',1),

('post_activity_by_action_type','Número de historias creadas acerca de la publicación de tu página, por tipo de acción.','lifetime','"[\\"post\\"]"','"[\\"engagement\\"]"',1),

('post_activity_by_action_type_unique','Número de personas que crearon una historia sobre la publicación de tu página, por tipo de acción.','lifetime','"[\\"post\\"]"','"[\\"engagement\\"]"',1);
