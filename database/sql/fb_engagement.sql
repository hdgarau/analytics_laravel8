INSERT INTO `metrics`
(`name`,
`description`,
periods,
`format_names`,
`objective_names`,
`platform_id`)
VALUES
('page_engaged_users','Número de personas que interactuaron con tu página. La interacción incluye cualquier clic.','day, week, days_28','"[\\"page\\"]"','"[\\"engagement\\"]"',1),

('page_post_engagements','Número de veces que las personas interactuaron con tus publicaciones por medio de reacciones, comentarios, contenido compartido, etc.','day, week, days_28','"[\\"page\\"]"','"[\\"engagement\\"]"',1),

('page_consumptions','Número de veces que las personas hicieron clic en alguno de tus contenidos.','day, week, days_28','"[\\"page\\"]"','"[\\"engagement\\"]"',1),

('page_consumptions_unique','Número de personas que hicieron clic en alguno de tus contenidos.','day, week, days_28','"[\\"page\\"]"','"[\\"engagement\\"]"',1),

('page_consumptions_by_consumption_type','Número de veces que las personas hicieron clic en alguno de tus contenidos, por tipo.','day, week, days_28','"[\\"page\\"]"','"[\\"engagement\\"]"',1),

('page_consumptions_by_consumption_type_unique','Número de personas que hicieron clic en alguno de tus contenidos, por tipo.','day, week, days_28','"[\\"page\\"]"','"[\\"engagement\\"]"',1),

('page_places_checkin_total','Número de veces que las personas registraron una visita en un lugar.','day, week, days_28','"[\\"page\\"]"','"[\\"engagement\\"]"',1),

('page_places_checkin_total_unique','Número de personas que registraron una visita en un lugar.','day, week, days_28','"[\\"page\\"]"','"[\\"engagement\\"]"',1),

('page_places_checkin_mobile','Número de veces que las personas registraron una visita en un lugar con su teléfono celular.','day, week, days_28','"[\\"page\\"]"','"[\\"engagement\\"]"',1),

('page_places_checkin_mobile_unique','Número de personas que registraron una visita en un lugar con su teléfono celular.','day, week, days_28','"[\\"page\\"]"','"[\\"engagement\\"]"',1),

('page_places_checkins_by_age_gender','Sexo y edad de las personas que registraron una visita en tu lugar.','day','"[\\"page\\"]"','"[\\"engagement\\"]"',1),

('page_places_checkins_by_locale','Idiomas principales de las personas que registraron una visita en tu lugar.','day','"[\\"page\\"]"','"[\\"engagement\\"]"',1),

('page_places_checkins_by_country','Países principales de las personas que registraron una visita en tu lugar.','day','"[\\"page\\"]"','"[\\"engagement\\"]"',1),

('page_negative_feedback','Número de veces que las personas realizaron una acción negativa (por ejemplo, indicar que ya no les gusta una publicación u ocultarla).','day, week, days_28','"[\\"page\\"]"','"[\\"engagement\\"]"',1),

('page_negative_feedback_unique','Número de personas que realizaron una acción negativa (por ejemplo, indicar que ya no les gusta una publicación u ocultarla).','day, week, days_28','"[\\"page\\"]"','"[\\"engagement\\"]"',1),

('page_negative_feedback_by_type','Número de veces que las personas realizaron una acción negativa, desglosado por tipo. (See possible types)','day, week, days_28','"[\\"page\\"]"','"[\\"engagement\\"]"',1),

('page_negative_feedback_by_type_unique','Número de personas que realizaron una acción negativa, desglosado por tipo. (See possible types)','day, week, days_28','"[\\"page\\"]"','"[\\"engagement\\"]"',1),

('page_positive_feedback_by_type','Número de veces que las personas realizaron una acción positiva, desglosado por tipo. (See possible types)','day, week, days_28','"[\\"page\\"]"','"[\\"engagement\\"]"',1),

('page_positive_feedback_by_type_unique','Número de personas que realizaron una acción positiva, desglosado por tipo. (See possible types)','day, week, days_28','"[\\"page\\"]"','"[\\"engagement\\"]"',1),

('page_fans_online','Número de fans que vieron cualquier publicación en Facebook en un día determinado, desglosado por horas en PST/PDT.','day','"[\\"page\\"]"','"[\\"engagement\\"]"',1),

('page_fans_online_per_day','Número de fans que vieron cualquier publicación en Facebook en un día determinado.','day','"[\\"page\\"]"','"[\\"engagement\\"]"',1),

('page_fan_adds_by_paid_non_paid_unique','Número de personas nuevas que indicaron que les gusta tu página, desglosado por pagadas y no pagadas.','day','"[\\"page\\"]"','"[\\"engagement\\"]"',1);
