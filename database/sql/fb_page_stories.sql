INSERT INTO `metrics`
(`name`,
`description`,
periods,
`format_names`,
`objective_names`,
`platform_id`)
VALUES
('page_content_activity_by_action_type_unique','Número de personas que están hablando de las historias de tu página, por tipo de historia de la página. (See possible types)','day, week, days_28','"[\\"page\\"]"','"[\\"engagement\\"]"',1),

('page_content_activity_by_age_gender_unique','Número de personas que hablan de la página por edad y sexo. Esta cifra es una estimación.','day, week, days_28','"[\\"page\\"]"','"[\\"engagement\\"]"',1),

('page_content_activity_by_city_unique','Número de personas que están hablando de la página, por ciudad del usuario.','day, week, days_28','"[\\"page\\"]"','"[\\"engagement\\"]"',1),

('page_content_activity_by_country_unique','Número de personas, por países, que están hablando sobre tu página. Solo se incluyen los 45 países en los que haya más personas hablando sobre tu página.','day, week, days_28','"[\\"page\\"]"','"[\\"engagement\\"]"',1),

('page_content_activity_by_locale_unique','Número de personas que están hablando de la página, por idioma del usuario.','day, week, days_28','"[\\"page\\"]"','"[\\"engagement\\"]"',1),

('page_content_activity','Número de historias creadas acerca de tu página (historias).','day, week, days_28','"[\\"page\\"]"','"[\\"engagement\\"]"',1),

('page_content_activity_by_action_type','Número de historias acerca de las historias de tu página, por tipo de historia de la página. (See possible types)','day, week, days_28','"[\\"page\\"]"','"[\\"engagement\\"]"',1);
