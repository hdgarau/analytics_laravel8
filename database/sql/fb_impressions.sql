INSERT INTO `metrics`
(`name`,
`description`,
periods,
`format_names`,
`objective_names`,
`platform_id`)
VALUES
('page_impressions','Número de veces que se mostró en la pantalla de una persona contenido de tu página o relacionado con esta. Incluye publicaciones, historias, visitas, anuncios, información social de personas que interactúan con tu página y más.','day, week, days_28','"[\\"page\\"]"','"[\\"engagement\\"]"',1),

('page_impressions_unique','Número de personas a las que se mostró en pantalla contenido de tu página o relacionado con esta. Incluye publicaciones, historias, visitas, anuncios, información social de personas que interactúan con tu página y más.','day, week, days_28','"[\\"page\\"]"','"[\\"engagement\\"]"',1),

('page_impressions_paid','Número de veces que se mostró en la pantalla de una persona contenido de tu página o relacionado con ella a través de distribución pagada, como un anuncio.','day, week, days_28','"[\\"page\\"]"','"[\\"engagement\\"]"',1),

('page_impressions_paid_unique','Número de personas a las que se mostró en pantalla contenido de tu página o relacionado con ella a través de distribución pagada, como un anuncio.','day, week, days_28','"[\\"page\\"]"','"[\\"engagement\\"]"',1),

('page_impressions_organic','Número de veces que se mostró en la pantalla de una persona contenido de tu página o relacionado con esta a través de distribución no pagada. Incluye publicaciones, historias, visitas, información social de personas que interactúan con tu página y más.','day, week, days_28','"[\\"page\\"]"','"[\\"engagement\\"]"',1),

('page_impressions_organic_unique','Número de personas a las que se mostró en pantalla contenido de tu página o relacionado con esta a través de distribución no pagada. Incluye publicaciones, historias, visitas, información social de personas que interactúan con tu página y más.','day, week, days_28','"[\\"page\\"]"','"[\\"engagement\\"]"',1),

('page_impressions_viral','Número de veces que se mostró en la pantalla de una persona contenido de tu página o relacionado con esta con información social asociada. Se muestra información social cuando el amigo de una persona interactúa con tu página, publicación o historia, por ejemplo, indica que le gusta tu página o la sigue, interactúa con una publicación, comparte una foto de tu página o registra una visita en ella.','day, week, days_28','"[\\"page\\"]"','"[\\"engagement\\"]"',1),

('page_impressions_viral_unique','Número de personas a las que se mostró en pantalla contenido de tu página o relacionado con esta con información social asociada. Como forma de distribución orgánica, se muestra información social cuando el amigo de una persona interactúa con tu página, publicación o historia, por ejemplo, indica que le gusta tu página o la sigue, interactúa con una publicación, comparte una foto de tu página o registra una visita en ella.','day, week, days_28','"[\\"page\\"]"','"[\\"engagement\\"]"',1),

('page_impressions_nonviral','Número de veces que se mostró en la pantalla de una persona contenido de tu página. No incluye contenido creado sobre tu página con información social asociada. Se muestra información social cuando el amigo de una persona interactúa con tu página, publicación o historia, por ejemplo, indica que le gusta tu página o la sigue, interactúa con una publicación, comparte una foto de tu página o registra una visita en ella.','day, week, days_28','"[\\"page\\"]"','"[\\"engagement\\"]"',1),

('page_impressions_nonviral_unique','Número de personas a las que se mostró en pantalla contenido de tu página. No incluye contenido creado sobre tu página con información social asociada. Como forma de distribución orgánica, se muestra información social cuando el amigo de una persona interactúa con tu página, publicación o historia, por ejemplo, indica que le gusta tu página o la sigue, interactúa con una publicación, comparte una foto de tu página o registra una visita en ella.','day, week, days_28','"[\\"page\\"]"','"[\\"engagement\\"]"',1),

('page_impressions_by_story_type','Total de impresiones de publicaciones realizadas por un amigo acerca de tu página, por tipo. (See possible types)','day, week, days_28','"[\\"page\\"]"','"[\\"engagement\\"]"',1),

('page_impressions_by_story_type_unique','Número de personas que vieron publicaciones realizadas por un amigo acerca de tu página, por tipo. (See possible types)','day, week, days_28','"[\\"page\\"]"','"[\\"engagement\\"]"',1),

('page_impressions_by_city_unique','Número de personas que vieron cualquier contenido relacionado con tu página, por ciudad.','day, week, days_28','"[\\"page\\"]"','"[\\"engagement\\"]"',1),

('page_impressions_by_country_unique','Número de personas que vieron cualquier contenido relacionado con tu página, por país.','day, week, days_28','"[\\"page\\"]"','"[\\"engagement\\"]"',1),

('page_impressions_by_locale_unique','Número de personas que vieron cualquier contenido relacionado con tu página, por idioma seleccionado por el usuario.','day, week, days_28','"[\\"page\\"]"','"[\\"engagement\\"]"',1),

('page_impressions_by_age_gender_unique','Número de personas que vieron algún contenido de tu página o relacionado con esta, agrupadas por edad y sexo. Esta cifra es una estimación.','day, week, days_28','"[\\"page\\"]"','"[\\"engagement\\"]"',1),

('page_impressions_frequency_distribution','Número de personas a las que llegó tu página, desglosado por el número de veces que vieron cualquier contenido acerca de tu página.','day, week, days_28','"[\\"page\\"]"','"[\\"engagement\\"]"',1),

('page_impressions_viral_frequency_distribution','Número de personas a las que llegó tu página a través de una historia publicada por un amigo, desglosado por el número de veces que las personas vieron las historias acerca de tu página.','day, week, days_28','"[\\"page\\"]"','"[\\"engagement\\"]"',1);
