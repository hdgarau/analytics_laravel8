INSERT INTO `metrics`
(`name`,
`description`,
periods,
`format_names`,
`objective_names`,
`platform_id`)
VALUES

('page_daily_video_ad_break_ad_ impressions_by_crosspost_status','The total number of times an ad was shown during ad breaks in crossposted videos.','day','"[\\"page\\"]"','"[\\"engagement\\"]"',1),

('page_daily_video_ad_ break_cpm_by_crosspost_status','The average amount paid by advertisers for 1,000 of impressions of their ads in a crossposted videos. This is a gross number and includes the amount paid to Facebook.','day','"[\\"page\\"]"','"[\\"engagement\\"]"',1),

('page_daily_video_ad_ break_earnings_by_crosspost_status','An estimate of the amount you earned from ad breaks in a crossposted videos, based on the number of impressions and CPM of ads shown. Actual payments may differ if there are content ownership claims or other adjustments.','day','"[\\"page\\"]"','"[\\"engagement\\"]"',1),

('post_video_ad_break_ad_impressions','The total number of times an ad was shown during ad breaks in your videos.','day, lifetime','"[\\"post\\"]"','"[\\"engagement\\"]"',1),

('post_video_ad_break_earnings','An estimate of the amount you earned from ad breaks in your videos, based on the number of impressions and CPM of ads shown. Actual payments may differ if there are content ownership claims or other adjustments.','day, lifetime','"[\\"post\\"]"','"[\\"engagement\\"]"',1),

('post_video_ad_break_ad_cpm','The average amount paid by advertisers for 1,000 impressions of their ads in your videos. This number also includes the amount paid to Facebook.','day, lifetime','"[\\"post\\"]"','"[\\"engagement\\"]"',1);
