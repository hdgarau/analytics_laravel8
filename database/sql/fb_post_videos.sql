INSERT INTO `metrics`
(`name`,
`description`,
periods,
`format_names`,
`objective_names`,
`platform_id`)
VALUES
('post_video_avg_time_watched','Tiempo promedio, en milisegundos, que las personas reprodujeron tus videos. Solo disponible para los videos creados después del 25 de agosto de 2016. Devuelve 0 para los videos compartidos.','lifetime','"[\\"post\\"]"','"[\\"engagement\\"]"',1),

('post_video_complete_views_organic','Número de veces que se reprodujeron tus videos desde el principio hasta el 95% o más de su duración mediante alcance orgánico. En una única instancia de reproducción de video, se excluirá el tiempo de las reproducciones repetidas. Devuelve 0 para los videos compartidos.','lifetime','"[\\"post\\"]"','"[\\"engagement\\"]"',1),

('post_video_complete_views_organic_unique','Número de personas que reprodujeron tus videos desde el principio hasta el 95% o más de su duración mediante alcance orgánico. En una única instancia de reproducción de video, se excluirá el tiempo de las reproducciones repetidas. Devuelve 0 para los videos compartidos.','lifetime','"[\\"post\\"]"','"[\\"engagement\\"]"',1),

('post_video_complete_views_paid','Número de veces que se reprodujeron tus videos promocionados desde el principio hasta el 95% o más de su duración. Para cada impresión de un video, se contarán las reproducciones por separado y se excluirá el tiempo de las reproducciones repetidas. Devuelve 0 para los videos compartidos.','lifetime','"[\\"post\\"]"','"[\\"engagement\\"]"',1),

('post_video_complete_views_paid_unique','Número de personas que vieron tus videos promocionados desde el principio hasta el 95% o más de su duración. Para cada impresión de un video, se contarán las reproducciones por separado y se excluirá el tiempo de las reproducciones repetidas.','lifetime','"[\\"post\\"]"','"[\\"engagement\\"]"',1),

('post_video_retention_graph','Número de veces que se reprodujeron tus videos en cada intervalo como porcentaje del total de reproducciones. Los videos se dividen en 40 intervalos iguales. Esta métrica no cuenta las impresiones mientras el video estaba en vivo. Los gráficos de retención pueden mostrar más impresiones una vez avanzado el video que al principio. Es posible que las personas empiecen a verlo desde la mitad, lo adelanten, lo guarden y continúen viéndolo desde un momento concreto, u otros comportamientos similares.','lifetime','"[\\"post\\"]"','"[\\"engagement\\"]"',1),

('post_video_retention_graph_clicked_to_play','Número de veces que se reprodujeron tus videos en cada intervalo como porcentaje del total de reproducciones después de que las personas hicieron clic para reproducirlos. Los videos se dividen en 40 intervalos iguales. Esta métrica no cuenta las impresiones mientras el video estaba en vivo. Los gráficos de retención pueden mostrar más impresiones una vez avanzado el video que al principio. Es posible que las personas empiecen a verlo desde la mitad, lo adelanten, lo guarden y continúen viéndolo desde un momento concreto, u otros comportamientos similares.','lifetime','"[\\"post\\"]"','"[\\"engagement\\"]"',1),

('post_video_retention_graph_autoplayed','Número de veces que se reprodujeron tus videos automáticamente en cada intervalo como porcentaje del total de reproducciones automáticas. Los videos se dividen en 40 intervalos iguales. Esta métrica no cuenta las impresiones mientras el video estaba en vivo. Los gráficos de retención pueden mostrar más impresiones una vez avanzado el video que al principio. Es posible que las personas empiecen a verlo desde la mitad, lo adelanten, lo guarden y continúen viéndolo desde un momento concreto, u otros comportamientos similares.','lifetime','"[\\"post\\"]"','"[\\"engagement\\"]"',1),

('post_video_views_organic','Número de veces que se reprodujeron tus videos durante al menos 3 segundos, o casi en su totalidad si duran menos de 3 segundos, mediante alcance orgánico. En una única instancia de reproducción de video, se excluirá el tiempo de las reproducciones repetidas.','lifetime, day','"[\\"post\\"]"','"[\\"engagement\\"]"',1),

('post_video_views_organic_unique','Número de personas que vieron tus videos durante al menos 3 segundos, o casi en su totalidad si duran menos de 3 segundos, mediante alcance orgánico. En una única instancia de reproducción de video, se excluirá el tiempo de las reproducciones repetidas.','lifetime','"[\\"post\\"]"','"[\\"engagement\\"]"',1),

('post_video_views_paid','Número de veces que se reprodujeron tus videos promocionados durante al menos 3 segundos, o casi en su totalidad si duran menos de 3 segundos. Para cada impresión de un video, se contarán las reproducciones por separado y se excluirá el tiempo de las reproducciones repetidas.','lifetime, day','"[\\"post\\"]"','"[\\"engagement\\"]"',1),

('post_video_views_paid_unique','Número de personas que vieron tus videos promocionados durante al menos 3 segundos, o casi en su totalidad si duran menos de 3 segundos. Para cada impresión de un video, se contarán las reproducciones por separado y se excluirá el tiempo de las reproducciones repetidas.','lifetime','"[\\"post\\"]"','"[\\"engagement\\"]"',1),

('post_video_length','Duración, en milisegundos, de una publicación con video.','lifetime','"[\\"post\\"]"','"[\\"engagement\\"]"',1),

('post_video_views','Número de veces que se reprodujeron tus videos durante al menos 3 segundos, o casi en su totalidad si duran menos de 3 segundos. En una única instancia de reproducción de video, se excluirá el tiempo de las reproducciones repetidas.','lifetime, day','"[\\"post\\"]"','"[\\"engagement\\"]"',1),

('post_video_views_unique','Número de personas que vieron tus videos durante al menos 3 segundos, o casi en su totalidad si duran menos de 3 segundos. En una única instancia de reproducción de video, se excluirá el tiempo de las reproducciones repetidas.','day, lifetime','"[\\"post\\"]"','"[\\"engagement\\"]"',1),

('post_video_views_autoplayed','Número de veces que se reprodujeron tus videos automáticamente durante al menos 3 segundos, o casi en su totalidad si duran menos de 3 segundos. En una única instancia de reproducción de video, se excluirá el tiempo de las reproducciones repetidas.','lifetime','"[\\"post\\"]"','"[\\"engagement\\"]"',1),

('post_video_views_clicked_to_play','Número de veces que se reprodujeron tus videos durante al menos 3 segundos, o casi en su totalidad si duran menos de 3 segundos, después de que las personas hicieron clic para reproducirlos. En una única instancia de reproducción de video, se excluirá el tiempo de las reproducciones repetidas.','lifetime','"[\\"post\\"]"','"[\\"engagement\\"]"',1),

('post_video_views_15s','Número de veces que se reprodujeron tus videos durante al menos 15 segundos, o casi en su totalidad si duran menos de 15 segundos. En una única instancia de reproducción de video, se excluirá el tiempo de las reproducciones repetidas.','lifetime','"[\\"post\\"]"','"[\\"engagement\\"]"',1),

('post_video_views_60s_excludes_shorter','Número de veces que se reprodujeron tus videos durante al menos 60 segundos. Esta métrica solo se contabiliza para los videos que duran 60 segundos o más. En una única instancia de reproducción de video, se excluirá el tiempo de las reproducciones repetidas.','lifetime, day','"[\\"post\\"]"','"[\\"engagement\\"]"',1),

('post_video_views_10s','Número de veces que se reprodujeron tus videos durante al menos 10 segundos, o casi en su totalidad si duran menos de 10 segundos. En una única instancia de reproducción de video, se excluirá el tiempo de las reproducciones repetidas.','lifetime, day','"[\\"post\\"]"','"[\\"engagement\\"]"',1),

('post_video_views_10s_unique','Número de personas que vieron tus videos durante al menos 10 segundos, o casi en su totalidad si duran menos de 10 segundos. Para cada impresión de un video, se contarán las reproducciones por separado y se excluirá el tiempo de las reproducciones repetidas.','lifetime','"[\\"post\\"]"','"[\\"engagement\\"]"',1),

('post_video_views_10s_autoplayed','Número de veces que se reprodujeron tus videos automáticamente durante al menos 10 segundos, o casi en su totalidad si duran menos de 10 segundos. En una única instancia de reproducción de video, se excluirá el tiempo de las reproducciones repetidas.','lifetime','"[\\"post\\"]"','"[\\"engagement\\"]"',1),

('post_video_views_10s_clicked_to_play','Número de veces que se reprodujeron tus videos durante al menos 10 segundos, o casi en su totalidad si duran menos de 10 segundos, después de que las personas hicieron clic para reproducirlos. En una única instancia de reproducción de video, se excluirá el tiempo de las reproducciones repetidas.','lifetime','"[\\"post\\"]"','"[\\"engagement\\"]"',1),

('post_video_views_10s_organic','Número de veces que se reprodujeron tus videos durante al menos 10 segundos, o casi en su totalidad si duran menos de 10 segundos, mediante alcance orgánico. En una única instancia de reproducción de video, se excluirá el tiempo de las reproducciones repetidas.','lifetime','"[\\"post\\"]"','"[\\"engagement\\"]"',1),

('post_video_views_10s_paid','Número de veces que se reprodujeron tus videos promocionados durante al menos 10 segundos, o casi en su totalidad si duran menos de 10 segundos. Para cada impresión de un video, se contarán las reproducciones por separado y se excluirá el tiempo de las reproducciones repetidas.','lifetime, day','"[\\"post\\"]"','"[\\"engagement\\"]"',1),

('post_video_views_10s_sound_on','Número de veces que se reprodujeron tus videos con sonido activado durante al menos 10 segundos, o casi en su totalidad si duran menos de 10 segundos. En una única instancia de reproducción de video, se excluirá el tiempo de las reproducciones repetidas.','lifetime','"[\\"post\\"]"','"[\\"engagement\\"]"',1),

('post_video_views_sound_on','Número de veces que se reprodujeron tus videos con sonido activado durante al menos 3 segundos, o casi en su totalidad si duran menos de 3 segundos. En una única instancia de reproducción de video, se excluirá el tiempo de las reproducciones repetidas.','lifetime','"[\\"post\\"]"','"[\\"engagement\\"]"',1),

('post_video_view_time','Tiempo total, en milisegundos, que se reprodujeron tus videos, incluidas las reproducciones repetidas y las de menos de 3 segundos. Devuelve 0 para los videos compartidos.','lifetime, day','"[\\"post\\"]"','"[\\"engagement\\"]"',1),

('post_video_view_time_organic','Tiempo total, en milisegundos, que se reprodujeron tus videos mediante alcance orgánico. Devuelve 0 para los videos compartidos.','lifetime, day','"[\\"post\\"]"','"[\\"engagement\\"]"',1),

('post_video_view_time_by_age_bucket_and_gender','Tiempo total, en milisegundos, que se reprodujeron tus videos por públicos principales (edad y sexo).','lifetime','"[\\"post\\"]"','"[\\"engagement\\"]"',1),

('post_video_view_time_by_region_id','Tiempo total, en milisegundos, que se reprodujeron tus videos en los 45 lugares más populares (región y país).','lifetime, day','"[\\"post\\"]"','"[\\"engagement\\"]"',1),

('post_video_views_by_distribution_type','Número de veces que se reprodujeron tus videos por tipo de distribución (propiedad de la página o compartida).','lifetime','"[\\"post\\"]"','"[\\"engagement\\"]"',1),

('post_video_view_time_by_distribution_type','Tiempo total, en milisegundos, que se reprodujeron tus videos por tipo de distribución (propiedad de la página o compartida).','lifetime','"[\\"post\\"]"','"[\\"engagement\\"]"',1),

('post_video_view_time_by_country_id','Número total de minutos que se reprodujeron tus videos en los 45 lugares más populares (país).','lifetime','"[\\"post\\"]"','"[\\"engagement\\"]"',1);
