INSERT INTO `metrics`
(`name`,
`description`,
periods,
`format_names`,
`objective_names`,
`platform_id`)
VALUES
('post_video_complete_views_30s_autoplayed','Número de veces que se reprodujeron tus videos automáticamente durante al menos 30 segundos, o casi en su totalidad si duran menos de 30 segundos. En una única instancia de reproducción de video, se excluirá el tiempo de las reproducciones repetidas. Devuelve 0 para los videos compartidos.','lifetime','"[\\"post\\"]"','"[\\"engagement\\"]"',1),

('post_video_complete_views_30s_clicked_to_play','Número de veces que se reprodujeron tus videos durante al menos 30 segundos, o casi en su totalidad si duran menos de 30 segundos, después de que las personas hicieron clic para reproducirlos. En una única instancia de reproducción de video, se excluirá el tiempo de las reproducciones repetidas. Devuelve 0 para los videos compartidos.','lifetime','"[\\"post\\"]"','"[\\"engagement\\"]"',1),

('post_video_complete_views_30s_organic','Número de veces que se reprodujeron tus videos durante al menos 30 segundos, o casi en su totalidad si duran menos de 30 segundos, mediante alcance orgánico. En una única instancia de reproducción de video, se excluirá el tiempo de las reproducciones repetidas. Devuelve 0 para los videos compartidos.','lifetime','"[\\"post\\"]"','"[\\"engagement\\"]"',1),

('post_video_complete_views_30s_paid','Número de veces que se reprodujeron tus videos promocionados durante al menos 30 segundos, o casi en su totalidad si duran menos de 30 segundos. Para cada impresión de un video, se contarán las reproducciones por separado y se excluirá el tiempo de las reproducciones repetidas. Devuelve 0 para los videos compartidos.','lifetime','"[\\"post\\"]"','"[\\"engagement\\"]"',1),

('post_video_complete_views_30s_unique','Número de personas que vieron tus videos durante al menos 30 segundos, o casi en su totalidad si duran menos de 30 segundos. En una única instancia de reproducción de video, se excluirá el tiempo de las reproducciones repetidas.','lifetime','"[\\"post\\"]"','"[\\"engagement\\"]"',1);
