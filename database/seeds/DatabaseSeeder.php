<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        \App\Platform::create([
            'driver' => 'facebook',
            'name' => 'Facebook',
        ]);
        \App\Platform::create([
            'driver' => 'twitter',
            'name' => 'Twitter',
        ]);
        \App\Platform::create([
            'driver' => 'instagram',
            'name' => 'Instagram',
        ]);
        \App\Platform::create([
            'driver' => 'google',
            'name' => 'Google',
        ]);
        \App\Platform::create([
            'driver' => 'mailchimp',
            'name' => 'Mail Chimp',
        ]);


        //Usuario de prueba
        App\User::create([
            'name' => 'Administrador 1',
            'last_name' => 'Administrador 1',
            'email' => 'admin@analytics.com',
            'user_type' => 'admin',
            'password' => hash::make('123'),
        ]);
        App\User::create([
            'name' => 'Cliente 1',
            'last_name' => 'Administrador 1',
            'email' => 'client1@analytics.com',
            'user_type' => 'client',
            'password' => hash::make('123'),
        ]);
        App\User::create([
            'name' => 'Cliente 2',
            'last_name' => 'Administrador 1',
            'email' => 'client2@analytics.com',
            'user_type' => 'client',
            'password' => hash::make('123'),
        ]);

        //agrego formatos de indicadores
        App\MetricFormats::create(['name' => 'post', 'caption' => 'Indicador de Post']);
        App\MetricFormats::create(['name' => 'page', 'caption' => 'Indicador de Página']);
        App\MetricFormats::create(['name' => 'ads', 'caption' => 'Indicador ADs']);
        App\MetricFormats::create(['name' => 'web', 'caption' => 'Indicador WEB']);

        //agrego objetivos de indicadores
        App\MetricObjectives::create(['name' => 'awareness', 'caption' => 'Awareness']);
        App\MetricObjectives::create(['name' => 'recordacion_de_marca', 'caption' => 'Recordación de marca']);
        App\MetricObjectives::create(['name' => 'lead_generation', 'caption' => 'Lead Generation']);
        App\MetricObjectives::create(['name' => 'engagement', 'caption' => 'Engagement']);

        //agrego algunos indicadores
        //facebook
        App\Metric::create([
            'name' => 'page_impressions_unique',
            'format_names' => json_encode(['page']),
            'objective_names' => json_encode(['awareness','lead_generation']),
            'platform_id' => 1,
        ]);
        App\Metric::create([
            'name' => 'page_impressions',
            'format_names' => json_encode(['page']),
            'objective_names' => json_encode(['recordacion_de_marca','lead_generation']),
            'platform_id' => 1,
        ]);
        App\Metric::create([
            'name' => 'post_impressions',
            'format_names' => json_encode(['page']),
            'objective_names' => json_encode(['recordacion_de_marca']),
            'platform_id' => 1,
        ]);
        //instagram
        App\Metric::create([
            'name' => 'impressions',
            'format_names' => json_encode(['page','post']),
            'objective_names' => json_encode(['lead_generation']),
            'platform_id' => 3,
        ]);
        App\Metric::create([
            'name' => 'reach',
            'format_names' => json_encode(['page','post']),
            'objective_names' => json_encode(['awareness','engagement']),
            'platform_id' => 3,
        ]);
        App\Metric::create([
            'name' => 'profile_views',
            'format_names' => json_encode(['page']),
            'objective_names' => json_encode(['engagement']),
            'platform_id' => 3,
        ]);
    }
}
