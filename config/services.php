<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],
    //Redes sociales
    'facebook' => [
        'client_id' => env('FACEBOOK_CLIENT_ID','414416605934964'),         // Your Facebook App Client ID
        'client_secret' => env('FACEBOOK_CLIENT_SECRET','312c9a475cafe96012316f4a35a761bf'), // Your Facebook App Client Secret
        'redirect' => env('FACEBOOK_REDIRECT'), // Your application route used to redirect users back to your app after authentication
        'default_graph_version' => 'v3.3',
    ],
    'google' => [
        'client_id' => env('GOOGLE_CLIENT_ID'),         // Your  App Client ID
        'response_type' => 'get',         // Your  App Client ID
        'client_secret' => env('GOOGLE_CLIENT_SECRET'), // Your  App Client Secret
        'redirect_uri' => env('GOOGLE_REDIRECT'), // Your application route used to redirect users back to your app after authentication
        'redirect' => env('GOOGLE_REDIRECT'), // Your application route used to redirect users back to your app after authentication
        'default_graph_version' => 'v2',
        'approval_prompt' => 'force',
        'access_type' => 'offline',
        'prompt' => 'consent' ,
    ],
    'twitter' => [
        'client_id' => env('TWITTER_CLIENT_ID'),         // Your  App Client ID
        'response_type' => 'get',         // Your  App Client ID
        'client_secret' => env('TWITTER_CLIENT_SECRET'), // Your  App Client Secret
        'redirect_uri' => env('TWITTER_REDIRECT'), // Your application route used to redirect users back to your app after authentication
        'redirect' => env('TWITTER_REDIRECT'), // Your application route used to redirect users back to your app after authentication
        'default_graph_version' => 'v2',
    ],
    'linkedin' => [
        'client_id' => env('LINKEDIN_CLIENT_ID'),         // Your  App Client ID
        'response_type' => 'get',         // Your  App Client ID
        'client_secret' => env('LINKEDIN_CLIENT_SECRET'), // Your  App Client Secret
        'redirect_uri' => env('LINKEDIN_REDIRECT'), // Your application route used to redirect users back to your app after authentication
        'redirect' => env('LINKEDIN_REDIRECT'), // Your application route used to redirect users back to your app after authentication
        'default_graph_version' => 'v2',
    ],
    'mailchimp' => [
        'client_id' => env('MAILCHIMP_CLIENT_ID'),         // Your  App Client ID
        'client_secret' => env('MAILCHIMP_CLIENT_SECRET'), // Your  App Client Secret
        'redirect' => env('MAILCHIMP_REDIRECT'), // Your application route used to redirect users back to your app after authentication
        //'default_graph_version' => 'v2',
        'server' => env('MAILCHIMP_SERVER')
    ],
];
