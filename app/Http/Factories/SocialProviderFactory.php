<?php
/**
 * Created by PhpStorm.
 * User: HERNAN
 * Date: 1/7/2019
 * Time: 12:54 PM
 */

namespace App\Http\Factories;

use App\Http\SocialProviders\SocialProvider;

class SocialProviderFactory
{
    static function driver($driver, $token = null, $server = null): SocialProvider
    {
        $class = 'App\Http\SocialProviders\SocialProvider' . ucfirst($driver);
        return new $class($token, $server);
    }

}
