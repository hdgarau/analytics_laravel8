<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfIsNotDatesSelected
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {

        if (empty( session('date_past_since')) || empty( session('date_past_until')) ||
            empty( session('date_recent_since')) || empty( session('date_recent_until')  )) {
            return redirect('/select_time_range');
        }

        return $next($request);
    }
}
