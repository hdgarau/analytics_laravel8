<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfIsNotPlatformSelected
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (empty( session('userPlatforms_selected'))) {
            return redirect('/home');
        }
        //Si elije mc tiene que tener server

        $userPlatform = current(
            array_filter(is_array(session()->get('userPlatforms_selected')) ? session()->get('userPlatforms_selected') : [],
                function ($el) { return  $el->getDriver() == 'mailchimp' && empty($el->server);}));
        if(!empty($userPlatform))
        {
            return redirect('/home');
        }
        return $next($request);
    }
}
