<?php

namespace App\Http\SocialProviders;


use App\Http\Classes\Account;
use App\Http\Classes\Dictionary;
use App\Models\Metric;
use Carbon\Carbon;
use Facebook\Facebook;
use Illuminate\Support\Collection;

class SocialProviderFacebook extends SocialProvider
{
    const scopes =  [
        'read_insights',
        'pages_show_list',
        'instagram_basic',
        'instagram_manage_insights',
        'public_profile',
    ];
    const AVAILABLE_FILTERS = [
        [ 'type' => 'keyword', 'title' => 'Post Contents' ],
    ];
    const FORMATS_METRICS = ['page', 'post'];

    /*
    const scopes = ['read_insights, pages_manage_ads,
        pages_manage_metadata,
        pages_read_engagement,
        pages_read_user_content,pages_manage_posts,
        pages_manage_engagement, instagram_manage_insights','instagram_basic'];*/

    protected $driver = 'facebook';
    protected $fb = null;

    public function __construct($token = null)
    {
        $this->fb = new Facebook( [
            'app_id' => config('services.facebook.client_id'),
            'app_secret' => config('services.facebook.client_secret',NULL),
            'default_graph_version' => 'v2.6',
        ]);
        parent::__construct($token);

    }

    public function getIdByToken($token='',$secret = null)
    {
        return $this->getEndPoint( $this->getURLProfile(),$token )->getDecodedBody()['id'];
    }
    public function getNameByToken($token='',$secret = null)
    {
        return $this->getEndPoint( $this->getURLProfile(),$token )->getDecodedBody()['name'];
    }

    static function getURLPageMetrics($subject, $metrics, Array $data = []){
        if(is_array($metrics))
        {
            $metrics = implode(',',$metrics);
        }
        $url = $subject.'/insights?period=day&metric=' . $metrics;
        $desde = isset($data['desde']) ? $data['desde'] : '';
        $hasta = isset($data['hasta']) ? $data['hasta'] : '';
        return $url . SocialProviderFacebook::addFiltrosURL($desde,$hasta);
    }
    static function getURLPostMetrics($subject, $metrics, Array $data = []){
        if(is_array($metrics))
        {
            $metrics = implode(',',$metrics);
        }
        $url = $subject.'/posts?fields=insights.period(day).metric('. $metrics.'),message,created_time,permalink_url';
        $desde = isset($data['desde']) ? $data['desde'] : '';
        $hasta = isset($data['hasta']) ? $data['hasta'] : '';
        return $url . SocialProviderFacebook::addFiltrosURL($desde,$hasta);
    }
    static function addFiltrosURL($desde = '',$hasta = '')
    {
        $addURL = '';
        if(!empty($desde))
        {
            $addURL .= '&since=' . Carbon::parse($desde)->timestamp;
        }
        if(!empty($hasta))
        {
            $addURL .= '&until=' . Carbon::parse($hasta)->timestamp;
        }
        return $addURL;
    }
    public function getMetricsResponse(
        Collection $userAccountsSelectedCollection, array $metrics, array $filter,
        Carbon $date_since,Carbon $date_until
    ) : array
    {
        $response = [ ];
        $params = ['desde' => $date_since->format('Y-m-d'), 'hasta' => $date_until->format('Y-m-d') ];
        foreach ($userAccountsSelectedCollection as $userAccount)
        {
            if(!empty($metrics ['page']))
            {
                $res = $this->getEndPoint($this::getURLPageMetrics($userAccount->getID(),  $metrics ['page'], $params),$userAccount->getToken() );
                $response ['page'] /*[$userAccount->getName()]*/ = $this->arrayMetricsToStorageCollection($res, $userAccount);
            }
            if(!empty($metrics ['post']))
            {
                $res = $this->getEndPoint($this::getURLPostMetrics($userAccount->getID(),  $metrics ['post'], $params),$userAccount->getToken() );
                $response ['post']/* [$userAccount->getName()] */= $this->arrayPostMetricsToStorageCollection($res, $userAccount, isset ( $filter[ 'keyword' ] ) ? $filter ['keyword'] : []);
            }
        }
        return $response;
    }
    static public function getMaxMetricsFromMetricsResponse($data, &$result = [])
     {
         if(isset($data['page']) && is_iterable($data['page']))
         {
             $result['page'] = static::getMaxMetricsFromMetricsResponsePage($data['page']);
         }
         if(isset($data['post']) && is_iterable($data['post']))
         {
             $result['post'] = static::getMaxMetricsFromMetricsResponsePost($data['post']);
         }
         return $result;
     }
     static public function getMaxMetricsFromMetricsResponsePage($data, &$result = [])
     {
         foreach ($data as $row)
         {
             $val = array_sum($row->getItem('Valores'));
             if(!isset($result[$row->getItem('Metric')]) || $result[$row->getItem('Metric')] < $val)
             {
                 $result[$row->getItem('Metric')] = $val;
             }
         }
         return $result;
     }
     static public function getMaxMetricsFromMetricsResponsePost($data, &$result = [])
     {
         foreach ($data as $row)
         {
             static::getMaxMetricsFromMetricsResponsePage($row->getItem('Insights'), $result);
         }
         return $result;
     }

    public function getUserAccounts($user)
    {
        $accounts = array();
        $url = 'me?fields=accounts{name,picture,access_token,connected_instagram_account}'; //traigo las páginas

        foreach ($this->getEndPoint($url,$user->token)->getGraphUser() as $edgeUser)
        {
            if(is_object($edgeUser))
            {
                foreach ($edgeUser as $edgePage)
                {
                    $accountFacebook = new Account(
                        $this->getDriver(),
                        $edgePage->getfield('id'),
                        $edgePage->getfield('name'),
                        $edgePage->getfield('access_token')
                    );
                    $accountFacebook->addParam( 'picture' , $edgePage->getfield('picture')->getfield('url'));
                    $accountInstagram = null;

                    if(!empty($edgePage->getfield('connected_instagram_account')))
                    {
                        $accountInstagram = new Account(
                                'instagram',
                                $edgePage->getfield('connected_instagram_account')->getfield('id'),
                                $edgePage->getfield('name') . ' (IG)',
                                $edgePage->getfield('access_token')
                            );
                    }

                    //array_push( $accounts, array_merge([$accountFacebook], $accountInstagram) );
                    array_push( $accounts, $accountFacebook, $accountInstagram );
                }
            }
        }
        return array_filter( $accounts );
    }

    public function setToken($token)
    {
        $this->fb->setDefaultAccessToken($token);
        parent::setToken($token); // TODO: Change the autogenerated stub
    }

    protected function getEndPoint($url,$token = '', $method = 'GET',$token_secret = '')
    {
        if(!empty($token))
        {
            //throw new \Exception('Tokebn no definido');
            $this->setToken($token);
        }
        try {
            //dd($this->token, $this->fb, $this->fb->getDefaultAccessToken() == $token);
            $result = $this->fb->get($url);
        }
        catch(\Exception $e)
        {
            dd($e->getMessage() . $url);
        }

        return  $result;
    }
    protected function arrayPostMetricsToStorageCollection($arrayPostsResponse,Account $account,array $filter = [])
    {
        $obj = $arrayPostsResponse;
        if(is_object($arrayPostsResponse)) {
            $arrayPostsResponse = $arrayPostsResponse->getDecodedBody();
            $obj = $arrayPostsResponse['data'];
        }
        $result = array();
        $filterPost = new \App\Http\Classes\LogicGate\LogicGatesRoot( $filter );

        $arr['Plataforma'] = $account->getDriver();
        $arr['Usuario'] = $account->getName();
        foreach ($obj as $post) {
            $postMsg = isset($post['message']) ? $post['message'] : (isset($post['caption']) ? $post['caption'] : '');
            if(isset($post['error']))
            {
                $arr['Error'] = $post['error'];
            }
            else {
                if (! $filterPost->test( $postMsg )) //filter by campaign
                {
                    continue;
                }
                $arr['Mensaje'] = $postMsg;
                $arr['Creado'] = date('Y-m-d h:i:s', strtotime($post['created_time']));
                $arr['Link'] = $post['permalink_url'];;
                $arr['Id'] = $post['id'];
                $arr['Insights'] = isset($post['insights']) ? $this->arrayMetricsToStorageCollection($post['insights'],$account) : [];
            }
            $indicatorValue = new Dictionary($arr);
            array_push($result, $indicatorValue);
        }
        return Collect($result);
    }
    protected function arrayMetricsToStorageCollection($arrayMetricsResponse,Account $account)
    {
        if(is_object($arrayMetricsResponse) && method_exists($arrayMetricsResponse,'getDecodedBody'))
        {
            $arrayMetricsResponse = $arrayMetricsResponse->getDecodedBody();
        }
        $obj = $arrayMetricsResponse['data'];
        $result = array();
        $arr['Plataforma'] = $account->getDriver();
        $arr['Usuario'] = $account->getName();

        foreach ($obj as $metric)
        {
            $arr['Metric'] = $metric['name'];
            $arr['Descripcion'] = $metric['description'];
            $arr['Periodo'] = $metric['period'];
            foreach ($metric['values'] as  $data)
            {

                $fecha = isset($data['end_time']) ? \Carbon\Carbon::parse($data['end_time'])->format('d/m/Y')  : '';
                $arr['Valores'][$fecha] = is_array($data['value']) ? array_sum($data['value']) : $data['value'];
            }
            $postMetricsValue = new Dictionary($arr);
            array_push($result, $postMetricsValue);
        }
        return Collect($result);
    }
}
