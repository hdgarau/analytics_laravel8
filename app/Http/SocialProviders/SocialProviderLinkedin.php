<?php

namespace App\Http\SocialProviders;

use Happyr\LinkedIn\LinkedIn;

class SocialProviderLinkedin extends SocialProvider
{
    const scopes = ['r_emailaddress','r_liteprofile','w_member_social'
        ];

    protected $driver = 'linkedin';

    const AVAILABLE_FILTERS = [
        [ 'type' => 'keyword', 'title' => 'Post Contents' ],
        [ 'type' => 'url', 'title' => 'URL contains' ],
    ];
    static function getParamIndicators($params = []){
        return [ 'addURL' => '', 'params' => []];

    }

    protected function getEndPoint($params,$token = '', $method = 'GET',$token_secret = '')
    {

        $config = Config('services.linkedin');

        // Config
        $client_id = $config['client_id'];
        $client_secret = $config['client_secret'];

        $linkedIn=new LinkedIn($client_id, $client_secret);
        $linkedIn->setAccessToken($token);
        //GET https://api.linkedin.com/&campaigns[0]=urn:li:sponsoredCampaign:1234567

        dd($linkedIn->get('v2/adAnalyticsV2?q=analytics&pivot=CAMPAIGN&timeGranularity=DAILY'));
        dd($linkedIn->get('v2/people/~:(firstName)'));
    }//
    /*
    protected $urlbase = Google_Client::API_BASE_PATH . '/plus/v1/';
    protected $client = null;

    function __construct()
    {
           $this->client = new Google_Client([
                'app_id' => env('google_client_id'),
                'app_secret' => ENV('google_client_secret'),
                ]);

    }

    public function setToken($token)
    {
        $this->client->setAccessToken($token);
    }
    public function getUserProfile($token)
    {
        return ( Socialite::driver('google')->userFromToken($token));
        /*
        $client = new Google_Client();
        $client->setApplicationName('Web client 1');
        $client->setClientId(env('google_client_id'));
        $client->setClientSecret(env('google_client_secret'));
        $client->setRedirectUri(env('google_redirect'));
        $client->setScopes(array(Google_Service_Plus::PLUS_ME));
        $plus = new Google_Service_Plus($client);


        if (isset($token)) {
            $client->setAccessToken($token);
        }

        if ($client->getAccessToken() ) {
            try {
                $me = $plus->people->get('me');
                $body = '<PRE>' . print_r($me, TRUE) . '</PRE>';
            } catch (Google_Exception $e) {
                error_log($e);
                $body = htmlspecialchars($e->getMessage());
            }
            # the access token may have been updated lazily
            //$_SESSION['access_token'] = $client->getAccessToken();
        } else {
                $client->createAuthUrl();
        }
        die($body);
        /*
        if(!empty($token))
        {
            $this->setToken($token);
        }

        $this->client->addScope(Google_Service_Plus::PLUS_ME);
        die($this->urlbase);
        $httpClient = $this->client->authorize();
          $response = new \stdClass();
        $response = $httpClient->get($this->urlbase . 'people/me');
        //die($this->urlbase . 'people/me');
        dd($response);
        return $response;

    }

    private function generateSig($endpoint, $params) {

// make an HTTP request
    }
*/
}
