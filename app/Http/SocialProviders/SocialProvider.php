<?php

namespace App\Http\SocialProviders;

use App\Http\Classes\Account;
use App\UserPlatform;
use Laravel\Socialite\Facades\Socialite;
interface iSocialProviderController
{
    public function getUserProfile($user);
}
abstract class SocialProvider implements iSocialProviderController
{
    const paramLogin = [];
    protected $driver;
    protected $url_base = '';
    protected $token ;
    const default_format = 'page';
    const scopes = [];
    const FORMATS_METRICS = ['web'];
    public function __construct($token = null)
    {
        if($token !== null)
        {
            $this->setToken($token);
        }
    }

    public function setToken($token)
    {
        $this->token = $token;
    }
    public function getIdByToken($token,$secret = null)
    {
        return $this->getUserProfile( $token, $secret )->id;
    }
    public function getNameByToken($token = '',$secret = null)
    {
        return $this->getUserProfile( $token, $secret )->name;
    }
    public function getUrlProfile(): string
    {
        return 'me';
    }
    public function getUserProfile($user = null)
    {
        $token = isset($user->token) && !empty($user->token) ? $user->token : $this->token;
        if($user == null)
        {
            $user = UserPlatform::where('token', $this->token)->first();
        }
        //dd($token);
        try {
            $instancia  = Socialite::driver($this->driver);
            if(method_exists($instancia, 'userFromToken'))
            {
		        return $instancia->userFromToken ($token);
            }
            else
            {
                $response = $instancia->userFromTokenAndSecret ($token,$user->token_secret);
                $response->token_secret = $user->token_secret;
                return $response;
            }
        }
        catch (\Exception $exception){
                $this->refreshAndUpdateToken($user);
                return $this->getUserProfile($user);
        }
    }
    public function refreshAndUpdateToken($user)
    {
        return redirect('/login/' . $this->driver);
    }
    public function getUserAccounts($user)
    {
        return new Account($this->getDriver(), $user->id, $user->name,$user->token,$user->token_secret);
    }
    public function getCurrentUserAccounts()
    {
        return $this->getUserAccounts($this->getUserProfile());
    }
    public function findAccount(string $account_id, string $token = '') : Account
    {
        $accounts = $this->getCurrentUserAccounts();

        foreach ($accounts as $account)
        {
            foreach (is_array($account) ? $account : [$account] as $item)
            {
                if($item->getID() == $account_id)
                {
                    return $item;
                }
            }
        }
        return null;
    }
    public function getDriver(){
        return $this->driver;
    }
    protected function getMetricsValues(Account $account, $metrics, $data = [], $target = '')
    {
        if(empty($metrics))
        {
            return collect();
        }

        $url = $this::{'getURL' . $target . 'Metrics'}($account->getID(),$metrics,$data);
        $this->setToken($account->getToken());
        if(method_exists($this, 'getEndPointByObjectHelper'))
        {
            $endPointResponse =  $this->getEndPointByObjectHelper($url );
        }
        else
        {
            $endPointResponse =  $this->getEndPoint($url,$account->getToken(), $account->getSecretToken());
        }
        if(isset($data['debug']) && $data['debug'])
        {
            dd($endPointResponse);
        }
        return $this->{'array' . $target . 'MetricsToStorageCollection'}($endPointResponse,$account);
    }

    public function getPostMetricsValues(Account $account, $metrics, $data = [])
    {
        return $this->getMetricsValues($account, $metrics, $data ,  'Post');
    }
    public function getPageMetricsValues(Account $account, $metrics,$data = [])
    {
        return $this->getMetricsValues($account, $metrics,$data ,  'Page');
    }
    public function getWebMetricsValues(Account $account, $metrics, $data = [])
    {
        return $this->getMetricsValues($account, $metrics, $data ,  'Web');
    }
    public function getAdsMetricsValues(Account $account, $metrics, $data = [])
    {
        return $this->getMetricsValues($account, $metrics, $data ,  'Ads');
    }

    //Pasaje a storage unidimensional suele ser igual para todos
    public function arrayPageMetricsToStorageCollection($endPoint,Account $account)
    {
        return $this->arrayMetricsToStorageCollection($endPoint,$account);
    }

}
