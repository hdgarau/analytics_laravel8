<?php

namespace App\Http\SocialProviders;


use App\Http\Classes\Account;
use App\Http\Classes\Dictionary;
use App\VendorFilesChanged\TwitterOAuth_custom;
use Carbon\Carbon;

class SocialProviderTwitter extends SocialProvider
{

    const AVAILABLE_FILTERS = [
        [ 'type' => 'keyword', 'title' => 'Tweet Content' ],
        [ 'type' => 'url', 'title' => 'URL contains' ],
    ];
    protected $driver = 'twitter';
    //
    //const scopes = ['3-legged'];
    protected $token;
    protected $token_secret;

    static function getURLPageMetrics($subject, $metrics, $data= []){

        if(is_array($metrics))
        {
            $metrics = implode(',',$metrics);
        }
        $url ='stats/accounts/' . $subject . '?entity=ACCOUNT&entity_ids=' . $subject . '&granularity=TOTAL&metric_groups=' . $metrics . '&placement=ALL_ON_TWITTER&';


        $hasta = isset($data['hasta']) ? $data['hasta'] : date("Y-m-d");
        $desde = isset($data['desde']) ? $data['desde'] : date( 'Y-m-d', strtotime ( $hasta . ' -7 days'));

        return $url . SocialProviderTwitter::addFiltrosURL($desde,$hasta);
    }

    static function getURLPostMetrics($subject, $metrics, $tweet_id, $data =[]){

        if(is_array($metrics))
        {
            $metrics = implode(',',$metrics);
        }
        $url ='stats/accounts/' . $subject . '?entity=ORGANIC_TWEET&entity_ids=' . $tweet_id . '&granularity=TOTAL&metric_groups=' . $metrics . '&placement=ALL_ON_TWITTER&';


        $hasta = isset($data['hasta']) ? $data['hasta'] : date("Y-m-d");
        $desde = isset($data['desde']) ? $data['desde'] : date( 'Y-m-d', strtotime ( $hasta . ' -7 days'));
        return $url . SocialProviderTwitter::addFiltrosURL($desde,$hasta);
    }

    private static function addFiltrosURL(string $desde, string $hasta)
    {

        return 'start_time=' . $desde . '&end_time=' . $hasta;
    }
    public function getUserAccounts($user)
    {
        $response = $this->getEndPoint('accounts',$user->token,$user->token_secret);
        $accounts = $response->data;

        return array_map (
            function ($account) use($user)
            {
                return new Account($this->getDriver(), $account->id, $account->name, $user->token, $user->token_secret);
            },
            $accounts
        );
    }

    public function getNameScreenByID( $token='',$secret = null)
    {
        $this->token = $token;
        $id = $this->getUserProfile()->id;
         $return = $this->getEndPoint('users/show.json?user_id=' . $id,$token,$secret,'get', 'DATA');
        if(empty($return->screen_name)) {
            throw new \Exception('No existe screen Name');
        }
        return $return->screen_name;
    }

    protected function getEndPoint($url,$token = '',$token_secret = '', $method = 'GET', $host = 'ADS')
    {
        $token = empty($token) ? $this->token : $token;
        $token_secret = empty($token_secret) ? $this->token_secret : $token_secret;

        $this->token = $token;
        $this->token_secret = $token_secret;

        $conn = new TwitterOAuth_custom(env('TWITTER_CLIENT_ID','rLPnbxN0sF7rYWBiYrim5xnjQ'),env('TWITTER_CLIENT_SECRET','68c7rfZ8bP52FvaU0aQEvXOEFSTQGOuOxR5bTQwyyF0Ft0Nbzx'),
            $token, $token_secret);
        $conn->setHost($host);
        $return = $conn->$method(trim($url));
        if(isset($return->errors)) dd(($return));
        return $return;
    }
    protected function arrayMetricsToStorageCollection($arrayMetricsResponse,Account $account)
    {

        $result = array();

        $arr['Plataforma'] = $account->getDriver();
        $arr['Usuario'] = $account->getName();
        try{
        $obj = $arrayMetricsResponse->data[0]->id_data[0]->metrics;
        }
        catch (\Exception $e){dd($arrayMetricsResponse);
            throw new \Exception('Error en la estructura de datos.' . print_r($arrayMetricsResponse));
        }
        foreach ((array) $obj as $indicator => $valores)
        {
            $arr['Metric'] = $indicator;
            $arr['Descripcion'] = 'Valores en las fechas ingresadas (últimos 7 días si no se indicó)';
            $arr['Periodo'] = 'Rango de fechas';
            $arr['Valores'] = is_array($valores) ? array_sum( $valores) : '0';
            $postIndicatorsValue = new Dictionary($arr);
            array_push($result, $postIndicatorsValue);
        }
        return Collect($result);
    }

    public function getPostMetricsValues(Account $account, $metrics,  $data = [])
    {
        if(empty($metrics))
        {
            return Collect();
        }
        $name_screen = $this->getNameScreenByID($account->getToken(),$account->getSecretToken());
        $msg = isset($data['filtro']) && !empty($data['filtro']) ? '"' . $data['filtro'] .'"' : '';
        $desde = isset($data['desde']) && !empty($data['desde']) ? Carbon::parse( $data['desde']) ->format('Y-m-d') : '';
        $hasta = isset($data['hasta']) && !empty($data['hasta']) ? Carbon::parse( $data['hasta']) ->format('Y-m-d') : '';
        $tweets = $this->getEndPoint(
            'search/tweets.json?q=' . $msg . ' from:' . $name_screen . ($hasta != '' ? '&until=' . $hasta : ''),
            $account->getToken(),
            $account->getSecretToken(),
            'get',
            'DATA');

        array_walk ($tweets->statuses , function ($tweet) use($account, $metrics, $desde, $hasta){
            $url = $this::getURLPostMetrics($account->getId(),$metrics,$tweet->id,$desde,$hasta);
            $response = $this->getEndPoint($url);
           $tweet->metrics = $response;
        });
        return $this->arrayPostMetricsToStorageCollection($tweets->statuses,$account);
    }
    protected function arrayPostMetricsToStorageCollection($arrayPostsResponse,Account $account)
    {
        $result = array();
        $arr['Plataforma'] = $account->getDriver();
        $arr['Usuario'] = $account->getName();
        foreach ($arrayPostsResponse as $post) {
            $arr['Mensaje'] = isset($post->text) ? $post->text : '';
            $arr['Id'] = $post->id;
            $arr['Insights'] = $this->arrayMetricsToStorageCollection($post->metrics,$account);
            $indicatorValue = new Dictionary($arr);
            array_push($result, $indicatorValue);
        }
        return Collect($result);
    }
}
