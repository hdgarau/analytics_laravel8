<?php

namespace App\Http\SocialProviders;


use App\Http\Classes\Account;
use App\Http\Classes\Dictionary;
use App\Models\UserPlatform;
use Carbon\Carbon;
use Google_Client;
use Google_Service_Analytics;
use Google_Service_AnalyticsReporting;
use Google_Service_Drive;
use Illuminate\Support\Collection;

class SocialProviderGoogle extends SocialProvider
{
    const scopes = [Google_Service_AnalyticsReporting::ANALYTICS_READONLY,
        Google_Service_Drive::DRIVE_METADATA_READONLY,
        'https://www.googleapis.com/auth/userinfo.profile',
        'https://www.googleapis.com/auth/userinfo.email',
        'https://www.googleapis.com/auth/analytics',
        'https://www.googleapis.com/auth/analytics.readonly',
        'https://www.googleapis.com/auth/yt-analytics.readonly',

        ];
    const paramLogin = ["access_type" => "offline", "prompt" => "consent select_account"];
    protected $driver = 'google';
    const default_format = 'web';

    const AVAILABLE_FILTERS = [
        [ 'type' => 'url', 'title' => 'URL contains' ],
    ];
    /**
     * @var Google_Service_Analytics
     */
    private $client;

    //método no utilizdo
    public function getIdByToken($token,$secret = null)
    {
        return [];
    }

    static function getURLWebMetrics($id, $metrics = [], $data= []){

        if(is_array($metrics))
        {
            if(empty($metrics))
            {
                $metrics = implode(',', Metric::getMetricsByDriverAndIndicator('google'));
            }
            else
            {
                $metrics = implode(',',$metrics);

            }
        }

        $hasta = isset($data['hasta']) ? $data['hasta'] : 'yesterday';
        $desde = isset($data['desde']) ? $data['desde'] : '30daysAgo';
        $filtro = isset( $data['url'] ) ? $data['url'] : '.';

        return ['ga:' . $id,$desde,$hasta,$metrics,
            ['filters' => 'ga:pagePath=~' . urlencode($filtro), 'dimensions' => 'ga:pagePath,ga:pageTitle']];
    }
    static function getURLPageMetrics($subject, $metrics, $data= []){
        return SocialProviderGoogle::getURLWebMetrics($subject ,$metrics, $data);
    }
    public function __construct( $token )
    {

        $config = Config('services.google');

        // Config
        $client_id = $config['client_id'];
        $client_secret = $config['client_secret'];

        $client = new Google_Client();

        $client->setClientId($client_id);
        $client->setClientSecret($client_secret);

        $client->addScope(Google_Service_Analytics::ANALYTICS_READONLY);
        //$r = $client->getHttpClient('https://www.googleapis.com/analytics/v3/data/ga?ids=ga%3A202471651&start-date=2019-12-01&end-date=2019-12-23&metrics=ga%3Ausers');

        $this->client = $client;
        parent::__construct($token);
    }

    public function refreshAndUpdateToken( $user)
    {
        $userPlatform = UserPlatform::find($user->id);
        $newToken = $this->client->refreshToken($userPlatform->refresh_token);
        $this->client->setAccessType('offline');

        $userPlatform->token = $newToken['access_token'];
        $userPlatform->save();
        //dd($newToken);
        return $newToken['access_token'];
    }

    //sólo para analytics
    public function getUserAccounts($user)
    {
        $this->client->setAccessToken($user->token);
        $analytics = new Google_Service_Analytics($this->client);

        return array_map(
            function ($account) use ($user)
            {
                return new Account(
                    $this->getDriver(),
                    $account->getID(),
                    $account->getWebsiteUrl(),
                    $user->token,
                    ''
                );
            },
            $this->getProfiles($analytics)
        );

    }

    protected function getEndPointByObjectHelper($params)
    {
        $this->client->setAccessToken($this->token);
        $analytics = new Google_Service_Analytics($this->client);

        $results = $this->getResults($analytics, $params)->getTotalsForAllResults();
        try {

            $results = $this->getResults($analytics, $params)->getTotalsForAllResults();
        } catch (\Exception $e) {
            $userPlatform = UserPlatform::select('refresh_token')->where('token', $this->token)->first();
            $this->token = $this->refreshAndUpdateToken($userPlatform);
            $this->client->setAccessToken($this->token);

            $results = $this->getResults($analytics, $params)->getTotalsForAllResults();
        }

        return $results;
    }

    private function getProfiles($analytics) {
        // Get the user's first view (profile) ID.

        // Get the list of accounts for the authorized user.
        try {
            $accounts = $analytics->management_accounts->listManagementAccounts();
        }
        catch (\Exception $exception)
        {
            return [];
        }
        if (count($accounts->getItems()) > 0) {
            $items = $accounts->getItems();
            $firstAccountId = $items[0]->getId();

            // Get the list of properties for the authorized user.
            $properties = $analytics->management_webproperties
                ->listManagementWebproperties($firstAccountId);

            if (count($properties->getItems()) > 0) {
                $items = $properties->getItems();
                $firstPropertyId = $items[0]->getId();

                // Get the list of views (profiles) for the authorized user.
                $profiles = $analytics->management_profiles
                    ->listManagementProfiles($firstAccountId, $firstPropertyId);

                if (count($profiles->getItems()) > 0) {
                    return $profiles->getItems();

                } else {
                    throw new \Exception('No views (profiles) found for this user.');
                }
            } else {
                throw new \Exception('No properties found for this user.');
            }
        } else {
            throw new \Exception('No accounts found for this user.');
        }
    }

    private function getResults(  $analytics, Array $param)
    {
        return call_user_func_array([ $analytics->data_ga, 'get'],$param);
    }


    public function getMetricsResponse(
        Collection $userAccountsSelectedCollection, array $metrics, array $filter,
        Carbon $date_since,Carbon $date_until
    ) : array
    {
        $return = [];
        if(empty($metrics))
        {
            return $return;
        }
        foreach ( $userAccountsSelectedCollection  as $account)
        {
            $row['account_id'] = $account->getID();
            $row['account_name'] = $account->getName();
            $row['metrics'] = $this->getEndPointByObjectHelper(['ga:' . $account->getID(),
                $date_since->format('Y-m-d'),$date_until->format('Y-m-d'),$metrics,
                [
                    //'filters' => 'ga:pagePath=~actualidad' //. urlencode($filtro),
                    //'dimensions' => 'ga:pagePath,ga:pageTitle'
                ]]);
            array_push($return, $row);
        }
        return $return;
    }
    static public function getMaxMetricsFromMetricsResponse($data, &$result = [])
    {
        foreach ($data as $row)
        {
            foreach ($row['metrics'] as $metric => $val)
            {
                if(!isset($result[$metric]) || $result[$metric] < $val)
                {
                    $result[$metric] = $val;
                }
            }
        }
        return $result;
    }
    protected function arrayMetricsToStorageCollection($arrayMetricsResponse,Account $account)
    {

        $result = array();
        $arr['Plataforma'] = $account->getDriver();
        $arr['Usuario'] = $account->getName();

        foreach ($arrayMetricsResponse as $metric => $valores) {
            $arr['Metric'] = $metric;
            $arr['Descripcion'] = 'Valores en las fechas ingresadas (últimos 30 días si no se indicó)';
            $arr['Periodo'] = 'Rango de fechas';
            $arr['Valores'] = is_array($valores) ? $valores[0] : $valores;
            $postIndicatorsValue = new Dictionary($arr);
            array_push($result, $postIndicatorsValue);
        }
        return Collect($result);
    }
    protected function arrayWebMetricsToStorageCollection($arrayResponse,Account $account)
    {
        return $this->arrayMetricsToStorageCollection($arrayResponse,$account);

    }
}
