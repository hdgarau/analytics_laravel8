<?php


namespace App\Http\Classes;


use Illuminate\Support\Collection;

abstract class Formatter
{
    static function PostMetricsCollection_ArrayByPlatformUserMetricValue(Collection $collection):array
    {
        $return = [];
        $collection->each(
            function ($el) use (&$return){
                $k1 = $el->getItem('Plataforma');
                $k2 = $el->getItem('Usuario');
                if($el->isItem('Error'))
                {
                    return $el;
                }
                foreach ($el->getItem('Insights') as $insightValue)
                {
                    $valor = is_array($insightValue->getItem('Valores') ) ? array_sum($insightValue->getItem('Valores')) :$insightValue->getItem('Valores');

                    $k3 = $insightValue->getItem('Metric');
                    if(isset($return[$k1] [$k2] [$k3]))
                    {
                        $return[$k1] [$k2] [$k3] += $valor;
                    }
                    else {
                        $return[$k1] [$k2] [$k3] = $valor;
                    }

                }
            });
        return $return;
    }
    static function PageMetricsCollection_ArrayByPlatformUserMetricValue(Collection $collection):array
    {
        $return = [];
        $collection->each(
            function ($el) use (&$return) {
                $k1 = $el->getItem('Plataforma');
                $k2 = $el->getItem('Usuario');
                $k3 = $el->getItem('Metric');
                $valor = is_array( $el->getItem('Valores') ) ? array_sum($el->getItem('Valores')) :$el->getItem('Valores');
                if(isset($return[$k1] [$k2] [$k3]))
                {
                    $return[$k1] [$k2] [$k3] += $valor;
                }
                else {
                    $return[$k1] [$k2] [$k3] = $valor;
                }
            });
        return $return;
    }
    static function webMetricsCollection_ArrayByPlatformUserMetricValue(Collection $collection):array
    {
        return Formatter::PageMetricsCollection_ArrayByPlatformUserMetricValue($collection);
    }
    static function FormatNameMetricsCollection_ArrayByPlatformUserMetricValue(Collection $collection, string $format_name):array
    {
        return Formatter::{$format_name . 'MetricsCollection_ArrayByPlatformUserMetricValue'}($collection);
    }
}
