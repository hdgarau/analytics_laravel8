<?php


namespace App\Http\Classes;


use Illuminate\Support\Collection;

class Utils
{
    static function setEnvironmentValue(array $values)
    {

        $envFile = app()->environmentFilePath();
        $str = file_get_contents($envFile);

        if (count($values) > 0) {
            foreach ($values as $envKey => $envValue) {

                $str .= "\n"; // In case the searched variable is in the last line without \n
                $keyPosition = strpos($str, "{$envKey}=");
                $endOfLinePosition = strpos($str, "\n", $keyPosition);
                $oldLine = substr($str, $keyPosition, $endOfLinePosition - $keyPosition);

                // If key does not exist, add it
                if (!$keyPosition || !$endOfLinePosition || !$oldLine) {
                    $str .= "{$envKey}={$envValue}\n";
                } else {
                    $str = str_replace($oldLine, "{$envKey}={$envValue}", $str);
                }

            }
        }

        $str = substr($str, 0, -1);
        if (!file_put_contents($envFile, $str)) return false;
        return true;
    }

    static function hasAnyValueFieldsJSONInArray(String $JSON,Array $names) : bool
    {
        return count(array_intersect($names, json_decode($JSON))) > 0;
    }
    static function InConditionCollectionRegex( Collection $collection, string $regex) : bool
    {
        return ($collection->count() == 0) || $collection->filter(
            function ($el) use ($regex) {  return preg_match( '/' . $el['key_word'] . '/i', $regex);
            })->count() > 0;
    }
}
