<?php


namespace App\Http\Classes\LogicGate;


interface iIsEvaluable
{
    public function test ( $value ) : bool;
}
