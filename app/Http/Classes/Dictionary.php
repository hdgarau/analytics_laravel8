<?php


namespace App\Http\Classes;


class Dictionary
{
    protected $items = [];

    public function __construct(Array $items = [])
    {
        $this->setItems($items);
        return $this;
    }

    public function setItem($k, $v)
    {
        $this->items [$k] = $v;
    }

    /**
     * @return array
     */
    public function isItem($key)
    {
        return isset($this->items[$key]);
    }
    public function getItems(): array
    {
        return $this->items;
    }

    /**
     * @param array $items
     */
    public function setItems(array $items): void
    {
        $this->items = $items;
    }
    public function getItem($key)
    {
        return isset($this->items[$key]) ? $this->items[$key] :dd($this);
    }
}
