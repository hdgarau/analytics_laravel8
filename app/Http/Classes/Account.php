<?php

namespace App\Http\Classes;

class Account
{
    protected $driver;
    protected $token;
    protected $secretToken;
    protected $id;
    protected $name;
    protected $params =[];

    /**
     * @return mixed
     */
    public function __construct($driver,$id,$name,$token,$secretToken='')
    {
        $this->driver = $driver;
        $this->id = $id;
        $this->name = $name;
        $this->token = $token;
        $this->secretToken = $secretToken;

        return $this;
    }

    public function getDriver()
    {
        return $this->driver;
    }

    /**
     * @param mixed $driver
     */
    public function setDriver($driver): void
    {
        $this->driver = $driver;
    }

    /**
     * @return mixed
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @param mixed $token
     */
    public function setToken($token): void
    {
        $this->token = $token;
    }

    /**
     * @return mixed
     */
    public function getSecretToken()
    {
        return $this->secretToken;
    }

    /**
     * @param mixed $secretToken
     */
    public function setSecretToken($secretToken): void
    {
        $this->secretToken = $secretToken;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return array
     */
    public function getParams(): array
    {
        return $this->params;
    }
    public function getParam($k)
    {

        return isset($this->params[$k]) ? $this->params[$k] : '';
    }

    /**
     * @param array $params
     */
    public function setParams(array $params): void
    {
        $this->params = $params;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }
    /**
     * @param array $params
     */
    public function addParam($k, $v): void
    {
       $this->params[$k] = $v;
    }
}
