<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Mail\ActivationShipped;
use App\Mail\ResetPasswordShipped;
use App\Models\PasswordReset;
use App\Providers\RouteServiceProvider;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\MessageBag;


class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function redirectPath()
    {
        return '/validation_email';
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users', 'confirmed'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Models\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'last_name' => $data['last_name'],
            'user_type' => 'client',
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
    }


    function validateUser(string $crypted_id)
    {
        $user = User::Find(decrypt(base64_decode($crypted_id)));
        $user->validated = true;
        $user->save();
        return redirect('/login');

    }
    public function register(Request $request)
    {

        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));


        $this->guard()->login($user);
        Mail::to($request->user())->send(new ActivationShipped($user));

        return $this->registered($request, $user)
            ?: redirect($this->redirectPath());
    }
    public function RecoverPassSendMail(Request $request)
    {
        $user = User::where(['email' =>  $request['email']])->first();

        if($user === null)
        {
            return redirect('/reset_password')->with('errors',new MessageBag(['User not found']));
        }
        Mail::to($request['email'])->send(new ResetPasswordShipped($user));
        return redirect('/reset_password_code/' . $user->id);
    }
    public function changePasswordForm(string $crypted_id)
    {
        $user = User::Find(decrypt(base64_decode($crypted_id)));

        return view('auth.reset_password', ['user' => $user]);

    }
    public function VerifyCodeSentToResetPassword(User $user, Request  $request)
    {

        if( PasswordReset::where('code' , $request['code'])
                ->where( 'user_id' , $user->id)
                ->where( 'expire', '>=', Carbon::now())
                ->get()->Count() > 0 )
        {

            return view('auth.reset_password')->with([ 'user' => $user ]);
        }
        return  view('auth.reset_password_code', ['errors' => new MessageBag(['Code incorrect or expired']), 'user'=> $user]);

    }
    public function changePassword(User $user,Request $request)
    {
        $validator = Validator::make($request->all(), [
            'password' => ['required', 'string', 'min:8','same:password_confirm'],
        ]);
        if($validator->validated())
        {
            $user->password = bcrypt( $request['password']);
            $user->save();
            return redirect('/login');
        }
        return view('auth.reset_password')->with([ 'user' => $user, 'errors' => $validator->errors() ]);

    }
}
