<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Factories\SocialProviderFactory;
use App\Models\Platform;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\MessageBag;
use Laravel\Socialite\Facades\Socialite;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;


    protected $redirectTo = RouteServiceProvider::HOME;


    public function __construct()
    {
        $this->middleware('guest')->except('logout','redirectToDriverProvider','handleProviderCallback');
    }
    public function redirectToDriverProvider($driver)
    {

        $platform = Platform::where(['driver' => $driver])->first();
        $obj = SocialProviderFactory::driver($platform->driver);
        $provider = Socialite::driver($driver);
        if(method_exists($provider,'with'))
        {
            $provider = $provider->with($obj::paramLogin);
        }

        if(!empty($obj::scopes) && method_exists($provider,'scopes')) {
            return $provider->scopes($obj::scopes)->redirect();
        }
        else{

            return $provider->redirect();
        }
    }

    /**
     * Obtain the user information from Facebook.
     *
     * @return void
     */
    public function handleProviderCallback($driver, Request $request)
    {
        $platform = Platform::where(['driver' => $driver])->first();
        try
        {
            $auth_user = Socialite::driver($driver)->user();
            if(\auth()->guest())
            {
                $userPlatform = \App\Models\UserPlatform::where([
                    'platform_id' => $platform->id, 'platform_foreign_id' => $auth_user->id])->first();
                if($userPlatform == null)
                {
                    return redirect('/login')->with('errors' , new MessageBag(['User not found']));
                }
                //dd($userPlatform,User::Find($userPlatform->user_id) );
                return view('auth.reset_password')->with([ 'user' => User::Find($userPlatform->user_id) ]);
            }
            //dd($auth_user);
            $userPlatform = \App\Models\UserPlatform::UpdateOrCreate(
                [
                    'user_id' => auth::user()->id,
                    'platform_foreign_id' => $auth_user->id,
                    'platform_id' => $platform->id
                ],
                [
                    'token' => $auth_user->token,
                    'token_secret' => isset($auth_user->tokenSecret) ? $auth_user->tokenSecret : '',
                    'refresh_token' => isset($auth_user->refreshToken) ? $auth_user->refreshToken : '',
                ]
            );
        }catch(\Exception $exception)
        {
            dd(Socialite::driver($driver)->user());
        }
        //dd($auth_user);
        //return auth::user()->id;
        //$request->session()->forget('userPlatforms_selected');
        //$request->session()->push('userPlatforms_selected', $userPlatform);

        return redirect()->to('/select_platform/' . $platform->id); // Redirect to a secure page
    }
}
