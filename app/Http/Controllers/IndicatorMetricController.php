<?php

namespace App\Http\Controllers;

use App\Models\Indicator;
use App\Models\IndicatorMetric;
use App\Models\Metric;
use App\Models\Platform;
use Illuminate\Http\Request;

class IndicatorMetricController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index( Indicator $indicator)
    {
        $from_indicator = Metric::select('platform_id')->join('indicator_metrics','metrics.id','metric_id')
            ->where ('indicator_id', $indicator->id)->get()->toArray();
        $from_session = array_map( function ($el) { return $el->platform_id; }, empty(session('userPlatforms_selected')) ? [] : session('userPlatforms_selected'));
        $platforms_preselected = Platform::whereIn('platforms.id', array_merge($from_session, $from_indicator))->get();
        return view('abms.indicator_metrics.edit', [ 'indicator' => $indicator, 'platforms_preselected' => $platforms_preselected, 'platforms' => Platform::all()]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $indicator_id, $metric_id)
    {
        //
        IndicatorMetric::firstOrCreate([ 'indicator_id' => $indicator_id, 'metric_id' => $metric_id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\IndicatorMetric  $labelIndicator
     * @return \Illuminate\Http\Response
     */
    public function show(IndicatorMetric $labelIndicator)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\IndicatorMetric  $labelIndicator
     * @return \Illuminate\Http\Response
     */
    public function edit(IndicatorMetric $labelIndicator)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\IndicatorMetric  $labelIndicator
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, IndicatorMetric $labelIndicator)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\IndicatorMetric  $labelIndicator
     * @return \Illuminate\Http\Response
     */
    public function destroy($indicator_id, $metric_id)
    {
        //
        IndicatorMetric::where(['indicator_id' => $indicator_id, 'metric_id' => $metric_id])->delete();
    }
}
