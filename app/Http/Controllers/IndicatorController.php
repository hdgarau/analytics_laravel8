<?php

namespace App\Http\Controllers;

use App\Models\Metric;
use App\Models\Indicator;
use App\Models\IndicatorMetric;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class IndicatorController extends Controller
{
    public function toggleMetric (Indicator $indicator, Metric $metric)
    {

        $obj = IndicatorMetric::where(['indicator_id' => $indicator->id, 'metric_id' => $metric->id])->First();
        if(empty($obj))
        {
            IndicatorMetric::Create([
                'indicator_id' => $indicator->id,
                'metric_id' => $metric->id
            ]);
        }
        else
        {
            $obj->delete();
        }
    }
    public function getMetrics (Indicator $indicator, Request $request)
    {
        $query = Metric::select( 'platforms.driver','metrics.id', 'metrics.name', 'description', DB::raw('CASE WHEN indicator_metrics.metric_id IS NOT NULL THEN TRUE ELSE FALSE END AS selected'))
            ->join('platforms', 'platforms.id', 'platform_id')
            ->leftJoin('indicator_metrics', function($join) use ($indicator)
            {
                $join->on('metrics.id', '=', 'metric_id');
                $join->on('indicator_id','=',DB::raw($indicator->id));
            });
        if(!isset($request['mode']) )
        {
            $query->where('indicator_id', $indicator->id);
        }
        if( $indicator->indicator_type == 'measurer' )
        {
            $query->where(function($query) use ($request) {
                $query->where('metrics.periods', 'LIKE', '%day%')
                    ->orWhere('metrics.platform_id', '!=', '1');
            });
        }
        if(isset($request['key_word']) && !empty($request['key_word']) )
        {
            $query->where(function($query) use ($request) {
                $query->where('metrics.name', 'LIKE', '%'.$request['key_word'].'%')
                    ->orWhere('metrics.description', 'LIKE', '%'.$request['key_word'].'%');
            });
        }
        if(isset($request['platforms']))
        {
            $query->whereIn('platform_id',explode(',',$request['platforms']));
        }
        return $query->orderBy('indicator_metrics.metric_id')->get()->unique(function ($item) {
            return $item->id;
        })->toJSON();
    }
    public function destroy(Indicator $indicator)
    {
        //
        $indicator->delete();
        return redirect()->route($this->redirectTo);
    }
}
