<?php

namespace App\Http\Controllers;

use App\Http\Classes\Utils;
use Illuminate\Http\Request;

class ConfigController extends Controller
{
    //
    protected $redirectTo = 'home';
    public function update(Request $request)
    {
        $validatedData = $request->validate([
            'facebook_id' => 'required',
            'facebook_secret' => 'required',
            'twitter_id' => 'required',
            'twitter_secret' => 'required',
            'google_id' => 'required',
            'google_secret' => 'required',
            'mailchimp_id' => 'required',
            'mailchimp_secret' => 'required',
        ]);

        Utils::setEnvironmentValue([
            'FACEBOOK_CLIENT_ID' => $request['facebook_id'],
            'FACEBOOK_CLIENT_SECRET' => $request['facebook_secret'],
            'TWITTER_CLIENT_ID' => $request['twitter_id'],
            'TWITTER_CLIENT_SECRET' => $request['twitter_secret'],
            'GOOGLE_CLIENT_ID' => $request['google_id'],
            'GOOGLE_CLIENT_SECRET' => $request['google_secret'],
            'MAILCHIMP_CLIENT_ID' => $request['mailchimp_id'],
            'MAILCHIMP_CLIENT_SECRET' => $request['mailchimp_secret'],
        ]);
        return redirect()->route($this->redirectTo);
    }
}
