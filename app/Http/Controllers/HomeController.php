<?php

namespace App\Http\Controllers;

use App\Http\Factories\SocialProviderFactory;
use App\Models\Platform;
use App\Models\UserPlatform;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $data = [
            'platforms' => Platform::where('driver','<>','instagram')->get()
        ];
        //dd($data);
        return view('home',$data);
    }

    public function selectPlatform(Platform $platform, Request $request)
    {
        //if(Session)

        if($request->session()->has('userPlatforms_selected') && in_array( $platform->id, array_map( function($el) {return $el->platform_id;}, $request->session()->get('userPlatforms_selected') )))
        {
            session(['userPlatforms_selected' => array_filter($request->session()->get('userPlatforms_selected'), function($el) use($platform){ return $platform->id != $el->platform_id;} ) ] );
            return redirect('/home');
        }
        else
        {
            $userPlatform = UserPlatform::where(['user_id' => auth::id(), 'platform_id' => $platform->id])->first();
            if(empty($userPlatform))
            {
                return redirect('/login/' . $platform->driver);
            }
            $graph = SocialProviderFactory::driver($platform->driver);
            $graph->getUserProfile( $userPlatform ); //solo para renovar el toke si hace falta. No uso estos datos
            $request->session()->push('userPlatforms_selected', $userPlatform);
            return redirect('/home');
        }
    }
    public function userPlatformUpdate(UserPlatform $userPlatform, Request $request)
    {
        $content = json_decode( $request->getContent());
        foreach ($content as $c => $v)
        {
            if($c !== '_token')
            {
                $userPlatform->$c = $v;
            }
        }
        $userPlatform->save();
        session(['userPlatforms_selected' => array_filter($request->session()->get('userPlatforms_selected'), function($el) use($userPlatform){ return $userPlatform->id != $el->id;} ) ] );
        $request->session()->push('userPlatforms_selected', $userPlatform);
        return true;
    }
    public function userPlatformDelete(UserPlatform $userPlatform, Request $request)
    {
        session(['userPlatforms_selected' => array_filter($request->session()->get('userPlatforms_selected'), function($el) use($userPlatform){ return $userPlatform->id != $el->id;} ) ] );
        $userPlatform->delete();
        return redirect('/home');
    }
}
