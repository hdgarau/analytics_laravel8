<?php

namespace App\Http\Controllers;

use App\Models\Metric;
use App\Models\UserPlatform;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class MetricController extends Controller
{
    private $path_name = 'abms.metrics';
    private $redirectTo = 'metrics.index';

    public function index()
    {
        return view('abms.metrics.list')->with('metrics', Metric::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

        return view($this->path_name . '.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'nombre' => [
                'required',
                Rule::unique('metrics' ,'name')
                    ->where('platform_id',$request['plataforma']),
                'max:255',
            ],
            'plataforma' => 'required',
            'formatos' => 'required',
            'objetivos' => 'required',
        ]);


        $metric = Metric::create([
            'name'                 =>  $validatedData['nombre'],
            'platform_id'          =>  $validatedData['plataforma'],
            'format_names'    =>  json_encode($validatedData['formatos']),
            'objective_names' =>  json_encode($validatedData['objetivos']),
            'description'          =>  $request['description'],
        ]);

        return redirect('metrics');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Metric  $indicator
     * @return \Illuminate\Http\Response
     */
    public function show(Metric $indicator)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Metric  $indicator
     * @return \Illuminate\Http\Response
     */
    public function edit(Metric $metric)
    {
        //
        return view($this->path_name . '.edit', ['obj' => $metric  ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Metric  $indicator
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Metric $metric)
    {
        //
        $request->validate([
            'nombre' => [
                'required',
                Rule::unique('metrics' ,'name')
                    ->where('platform_id',$metric->platform_id)
                    ->ignore($metric->id),
                'max:255',
            ],
            'plataforma' => 'required',
            'formatos' => 'required',
            'objetivos' => 'required',        ]);

        $metric->name = $request['nombre'];
        $metric->platform_id = $request['plataforma'];
        $metric->description = $request['description'];
        $metric->format_names = json_encode($request['formatos']);
        $metric->objective_names = json_encode($request['objetivos']);

        $metric->save();
        return redirect('metrics');    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Metric  $indicator
     * @return \Illuminate\Http\Response
     */
    public function destroy(Metric $metric)
    {
        //

        $metric->delete();
        return $this->index();
    }

    public function getValues()
    {
        //return UserPlatform::getArrayProfilesData();
        return view('metrics_values');
    }
    public function getValuesByIdUserPlatform(Request $request, UserPlatform $userPlatform)
    {
        $account = $userPlatform->findAccount($request['account_id']);
        $data = $userPlatform->getFormatDefaultMetricsValuesByTokenAndDriver($account);

        if($data->count() > 0 && $data->first()->isItem('Insights'))
        {
            $aux = [];
            foreach($data as $account)
            {
                $id = $account->getItem('Id');
                foreach ($account->getItem('Insights') as $item)
                {
                    $item->setItem('Usuario', $item->getItem('Usuario') . ' - ' . $id);
                    array_push($aux, $item);
                }
            }
            $data = Collect($aux);
        }
        return view( 'metrics_values_table', ['data' =>$data]);
    }


}
