<?php

namespace App\Http\Controllers;

use App\Models\Indicator;
use App\Models\Label;
use App\Models\LabelIndicator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class IndicatorLabelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index( Indicator $indicator)
    {

        $label_names =  array_column( LabelIndicator::select('labels.name')
            ->join('labels', 'label_id' , 'labels.id')
            ->where('indicator_id', $indicator->id)
            ->get()->toArray(), 'name');

        return view('abms.indicator_labels.edit', [ 'indicator' => $indicator, 'labels' => $label_names ]);

    }
    public function saveAndFinish( Indicator $indicator, Request $request)
    {
        DB::transaction( function () use($indicator,$request) {
            LabelIndicator::where('indicator_id', $indicator->id)->delete();
            if (isset($request['labels']) && is_array($request['labels'])) {
                foreach ($request['labels'] as $label_name) {
                    $label = Label::where('name', 'like', $label_name)
                        ->firstOr(function () use ($label_name) {

                            return Label::create(['name' => $label_name, 'user_id' => auth()->id()]);
                        });
                    LabelIndicator::Create(['label_id' => $label->id, 'indicator_id' => $indicator->id]);
                }
            }
        });

            return redirect('/select_indicators/' . $indicator->id);
        }
}

