<?php

namespace App\Http\Controllers;

use App\DataTables\UserDataTable;
use App\Indicator;
use App\User;
use http\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Illuminate\Validation\Rule;

class UserController extends Controller
{
    private $path_name = 'abms.users';
    private $redirectTo = 'users.index';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(UserDataTable $dataTable)
    {
        return view('');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view($this->path_name . '.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $validatedData = $request->validate([
            'nombre' => 'required|unique:users,name|max:255',
            'clave' => 'required',
            'email' => 'required',
            'tipo_usuario' => 'required',
        ]);
//die( $validatedData['tipo_usuario']);
        User::create([
            'name'          =>  $validatedData['nombre'],
            'email'   =>  $validatedData['email'],
            'password'   =>  hash::make($validatedData['clave']),
            'user_type'    =>  $validatedData['tipo_usuario'],
        ]);
        return redirect()->route($this->redirectTo);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit( $id)
    {
        $user = User::find($id);
        return view($this->path_name . '.edit', ['obj' => $user   ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,  $user_id)
    {
        $user = User::find($user_id);
        $validatedData = $request->validate([
            'nombre' => [
                'required',
                Rule::unique('users' ,'name')->ignore($user->id),
                'max:255',
            ],
            'email' => [
                'required',
                Rule::unique('users' ,'email')->ignore($user->id),
                'max:255',
            ],
            'tipo_usuario' => 'required',
        ]);

        $user->name = $request['nombre'];
        $user->email = $request['email'];
        $user->user_type = $request['tipo_usuario'];
        if(isset($request['clave']) && !empty($request['clave']))
        {
            $user->password = hash::make($request['clave']);
        }
        $user->save();
        return redirect()->route($this->redirectTo);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        //

        $user->delete();
        return redirect()->route($this->redirectTo);
    }
    public function changePassword(Request $request)
    {
        $request->validate([
            'old' => 'required',
            'new' => 'required',
            ]
        );
        if(! hash::check(  $request['old'], auth::user()->password ) )
        {
            $msg = "Clave actual incorrecta";
        }
        else {
            $msg = "La clave se modifico correctamente";
            auth::user()->password = hash::make( $request['new']);
            auth::user()->save();
        }
        return view('change_password',['msg' => $msg]);
    }
}
