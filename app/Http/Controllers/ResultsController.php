<?php

namespace App\Http\Controllers;

use App\Http\Factories\SocialProviderFactory;
use App\Models\Indicator;
use App\Models\Metric;
use App\Models\Platform;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class ResultsController extends Controller
{
    public function index(Request $request)
    {
        $indicatorSelected = session('indicator_selected');
        $date_past_since = session('date_past_since');
        $date_past_until = session('date_past_until');
        $date_recent_since = session('date_recent_since');
        $date_recent_until = session('date_recent_until');
        $accountsCollection = (new Collection(session('accounts_selected')));
        $platforms =
            array_map( function ($driver) { return Platform::where('driver',$driver)->first(); },
                array_unique(
                    array_map(function ($el){ return $el->driver;},session('accounts_selected'))
                ));

        $dataPast = $this->getData(
            session('userPlatforms_selected'),
            $accountsCollection,
            $indicatorSelected,
            session('conditions_content'),
            $date_past_since,
            $date_past_until
        );
        $dataRecent = $this->getData(
            session('userPlatforms_selected'),
            $accountsCollection,
            $indicatorSelected,
            session('conditions_content'),
            $date_recent_since,
            $date_recent_until);
        $maxMetric = [];
        foreach ($dataPast as $driver => $response)
        {
            (SocialProviderFactory::driver($driver))::getMaxMetricsFromMetricsResponse(current($response), $maxMetric[$driver]);
        }
        foreach ($dataRecent as $driver => $response)
        {
            (SocialProviderFactory::driver($driver))::getMaxMetricsFromMetricsResponse(current($response),$maxMetric[$driver]);
        }
        if(isset($request['debug']) )
        {

            dd(
                'Data Past: ',$dataPast,
                'DataRecent:', $dataRecent,
                'Max Merge', $maxMetric
            );
        }
        return view('result', ['dataPast' => $dataPast, 'dataRecent' => $dataRecent,
            'date_past_until' => $date_past_until->format("d/m/Y"),
            'date_past_since' => $date_past_since->format("d/m/Y"),
            'date_recent_until' => $date_recent_until->format("d/m/Y"),
            'date_recent_since' => $date_recent_since->format("d/m/Y"),
            'max_metric' => $maxMetric
            ]);
    }
    public function getData(array $userPlatforms, Collection $accountsCollection,Indicator $indicatorSelected,
                                array $filters,Carbon $date_since,Carbon $date_until)
    {

        $return =  [];
        foreach ($userPlatforms as $userPlatform)
        {
            $userAccountsIDSelectedArray = array_column( $accountsCollection
                ->where('parent-id', $userPlatform->id)
                ->all(), 'id');
            $platform = $userPlatform->getPlatform();
            $socialProvider = SocialProviderFactory::driver($platform->driver, $userPlatform->token, $userPlatform->server );
            $userAccountsCollection = new Collection( $socialProvider->getUserAccounts($userPlatform));
            $userAccountsSelectedCollection = $userAccountsCollection->filter( function ($el) use($userAccountsIDSelectedArray){
                return in_array($el->getID(), $userAccountsIDSelectedArray);
            });

            foreach($userAccountsSelectedCollection->groupBy(function ($ac) { return $ac->getDriver();})->all()
                    as $driver => $userAccountsSelectedCollectionByPlatformInside )
            {
                $socialProvider = SocialProviderFactory::driver($driver, $userPlatform->token, $userPlatform->server );

                foreach($socialProvider::FORMATS_METRICS as $format)
                {
                    $metrics [ $format ] = Metric::getMetricsByDriverAndIndicator($driver, $indicatorSelected->id, $format);
                }
                if(empty($metrics) ) continue;
                $return[$driver][$userPlatform->id] = $socialProvider->getMetricsResponse(
                //dd(
                    Collect($userAccountsSelectedCollectionByPlatformInside),
                    count( $metrics) == 1 ? current($metrics) : $metrics,
                    isset ( $filters [ $platform->name ] ) ? $filters [ $platform->name ] : [ ],
                    $date_since,
                    $date_until
                );
                $metrics = null;
            }
        }
        return $return;
    }
}
