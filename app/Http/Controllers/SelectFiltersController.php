<?php

namespace App\Http\Controllers;

use App\Http\Classes\LogicGate\LogicGate;
use App\Http\Factories\SocialProviderFactory;
use App\Models\Platform;
use Carbon\Carbon;
use Illuminate\Http\Request;

class SelectFiltersController extends Controller
{
    public function timeRangeIndex()
    {
        return view('select_time_range');
    }
    public function timeRangeSaveAndNext(Request $request)
    {
        if(empty($request['date_past_since']) || empty($request['date_past_until']))
        {
            return $this->timeRangeIndex()->with('error', 'Complete Date Reference');
        }
        $since = Carbon::createFromFormat('Y-m-d', $request['date_past_since']);
        $until = Carbon::createFromFormat('Y-m-d', $request['date_past_until']);
        $range = $since->diffInDays($until);
        session([
            'date_past_since' => $since,
            'date_past_until' => $until
        ]);
        switch($request['compare_with'])
        {
            case 'past' :
                if(empty($request['date_recent_since']) || empty($request['date_recent_until']))
                {
                    return $this->timeRangeIndex()->with('error', 'Complete Date Near Past');
                }
                session([
                    'date_recent_since' => Carbon::createFromFormat('Y-m-d', $request['date_recent_since']),
                    'date_recent_until' => Carbon::createFromFormat('Y-m-d', $request['date_recent_until'])
                ]);
                break;
            case 'present':
                session([
                    'date_recent_since' => Carbon::today()->addDays( $range * -1),
                    'date_recent_until' =>  Carbon::today()
                ]);
                break;
            default:
                 throw new \Exception('ERROR DEL SISTEMA');
        }
        return redirect('select_key_words');
    }

    public function keyWordsIndex()
    {
        session(['conditions_content' => [] ]);
        $platforms =
            array_map( function ($driver) { return Platform::where('driver',$driver)->first(); },
                array_unique(
                    array_map(function ($el){ return $el->driver;},session('accounts_selected'))
                ));
        foreach ($platforms as $platform)
        {
            $filtersAvailable[$platform->name] = (SocialProviderFactory::driver($platform->driver))::AVAILABLE_FILTERS;
        }
        return view('select_key_words' )->with('filtersAvailable',$filtersAvailable);
    }

    public function keyWordsSaveAndNext( Request $request)
    {
        foreach ($request['keyword'] as $platform => $arrDataByType )
        {
            foreach ($arrDataByType as $type => $arrDataFilter)
            {
                if(!isset($filterSession[$platform][$type]))
                {
                    $filterSession[$platform][$type] = [];
                }
                foreach ( $arrDataFilter as $i => $keyword )
                {
                    if(empty($keyword))
                    {
                        continue;
                    }
                    array_push( $filterSession[$platform][$type],
                        [ 'value' => $keyword, 'next_gate' => $request['logic_gate'][$platform][$type][$i], 'operator' => LogicGate::OP_REGEX]);
                }
            }
        }
        session(['conditions_content' => $filterSession ]);
        return redirect('show_result');
    }
}
