<?php

namespace App\Http\Controllers;

use App\Http\Factories\SocialProviderFactory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SelectProfilesController extends Controller
{
    //
    public function index(Request $request)
    {
        return view('/select_profiles')->with('profiles', $this->getProfilesAndAccounts());
    }
    public function saveAccountsSelected(Request $request)
    {
        session(['accounts_selected' => array_map('json_decode',$request['accounts'])]);
        return redirect('/select_indicators');
    }
    private function getProfilesAndAccounts( )
    {
        if(empty(session('userPlatforms_selected') ))
        {
            return redirect('/home');
        }
        $arr = array();
        foreach (session('userPlatforms_selected') as  $item)
        {
            $platform = $item->getPlatform();
            $graph = SocialProviderFactory::driver($platform->driver, $item->token, $item->server );
            $user = $graph->getUserProfile( $item );
            $user->id = $item->id;
            $user->user_id = auth::user()->id;

            $user->accounts = $graph->getUserAccounts( $user );
            $user->platform = $platform;
            //dd($user->accounts);

            array_push($arr,$user);
        }
        //dd($arr2);
        return $arr;
    }
}
