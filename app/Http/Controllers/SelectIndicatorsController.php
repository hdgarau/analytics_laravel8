<?php

namespace App\Http\Controllers;

use App\Models\Indicator;
use App\Models\LabelIndicator;
use App\Models\Metric;
use Illuminate\Http\Request;

class SelectIndicatorsController extends Controller
{
    //
    public function index(Request $request, Indicator $indicator)
    {
        $indicators = Indicator::select('indicators.*')->
        join('users', 'user_id', 'users.id')
        ->where('user_id', auth()->id())->whereOr('user_type', 'admin')->get();
        //dd($indicators);
        return view('/select_indicator', [ 'indicators' => $indicators , 'indicator_preselect' => $indicator]);
    }
    public function saveAndNext(Indicator $indicator)
    {
        session(['indicator_selected' => $indicator]);
        return redirect('/select_time_range');
    }
    public function description( Indicator $indicator )
    {
        $metrics = Metric::select(['metrics.name', 'platforms.driver'])
            ->join('indicator_metrics', 'metrics.id', 'metric_id')
            ->join('platforms', 'platforms.id', 'platform_id')
            ->where('indicator_id', $indicator->id)
            ->get()->groupBy( 'driver');

        $label_names =  implode(', ', array_column( LabelIndicator::select('labels.name')
            ->join('labels', 'label_id' , 'labels.id')
            ->where('indicator_id', $indicator->id)
            ->get()->toArray(), 'name'));

        return view('includes.select_indicator_description',
            ['indicator' => $indicator, 'label_names' => $label_names, 'metrics' => $metrics]);
    }
    public function createOrUpdate( Request $request, Indicator $indicator)
    {
        if( empty($request['name']) )
        {
            return view('abms.indicators.name', ['indicator' => $indicator, 'request' => $request ]);
        }
        if( empty($request['description']) )
        {
            return view('abms.indicators.description', ['indicator' => $indicator, 'request' => $request ]);
        }
        $indicator->name = $request['name'];
        $indicator->indicator_type = $request['indicator_type'];
        $indicator->description = $request['description'];
        $indicator->user_id = \auth()->id();
        $indicator->save();
        return redirect('/indicator_metrics/' . $indicator->id);
    }
   }
