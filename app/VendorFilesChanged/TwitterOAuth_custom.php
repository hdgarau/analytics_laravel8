<?php


namespace App\VendorFilesChanged;


use Abraham\TwitterOAuth\Consumer;
use Abraham\TwitterOAuth\HmacSha1;
use Abraham\TwitterOAuth\Request;
use Abraham\TwitterOAuth\Response;
use Abraham\TwitterOAuth\Token;
use Abraham\TwitterOAuth\TwitterOAuthException;
use Abraham\TwitterOAuth\Util\JsonDecoder;

class TwitterOAuth_custom extends TwitterOAuth
{
     const URL_ADS = 'https://ads-api.twitter.com';
     const URL_DATA = 'https://api.twitter.com';


    public $version = '1';
    public $API_HOST = 'https://api.twitter.com';
    public $UPLOAD_HOST = 'https://upload.twitter.com';
    private $end_url = '';
    private $default_host = 'DATA';

    /** @var Response details about the result of the last request */
    protected $response;
    /** @var string|null Application bearer token */
    private $bearer;
    /** @var Consumer Twitter application details */
    private $consumer;
    /** @var Token|null User access token details */
    private $token;
    /** @var HmacSha1 OAuth 1 signature type used by Twitter */
    private $signatureMethod;
    /** @var int Number of attempts we made for the request */
    private $attempts = 0;

    /**
     * Constructor
     *
     * @param string      $consumerKey      The Application Consumer Key
     * @param string      $consumerSecret   The Application Consumer Secret
     * @param string|null $oauthToken       The Client Token (optional)
     * @param string|null $oauthTokenSecret The Client Token Secret (optional)
     */
    public function __construct($consumerKey, $consumerSecret, $oauthToken = null, $oauthTokenSecret = null)
    {
        parent::__construct($consumerKey, $consumerSecret, $oauthToken , $oauthTokenSecret);
        $this->setHost($this->default_host);
    }
    public function setHost($host)
    {
        switch ($host)
        {
            case 'ADS' :
                $this->API_HOST = self::URL_ADS;
                $this->end_url = '';
                $this->version = '8';
                break;
            case 'DATA' :
                $this->API_HOST = self::URL_DATA;
                $this->end_url = '';
                $this->version = '2';
                break;
            default:
                throw new \Exception('Host no definida');
        }
    }


    public function url($path, array $parameters)
    {
        $this->resetLastResponse();
        $this->response->setApiPath($path);
        $query = http_build_query($parameters);
        return sprintf('%s/%s?%s' . $this->end_url, $this->API_HOST, $path, $query);
    }

    public function oauth($path, array $parameters = [])
    {
        $response = [];
        $this->resetLastResponse();
        $this->response->setApiPath($path);
        $url = sprintf('%s/%s', $this->API_HOST, $path);
        $result = $this->oAuthRequest($url, 'POST', $parameters);

        if ($this->getLastHttpCode() != 200) {
            throw new TwitterOAuthException($result);
        }

        parse_str($result, $response);
        $this->response->setBody($response);

        return $response;
    }

    /**
     * Make /oauth2/* requests to the API.
     *
     * @param string $path
     * @param array  $parameters
     *
     * @return array|object
     */
    public function oauth2($path, array $parameters = [])
    {
        $method = 'POST';
        $this->resetLastResponse();
        $this->response->setApiPath($path);
        $url = sprintf('%s/%s', $this->API_HOST, $path);
        $request = Request::fromConsumerAndToken($this->consumer, $this->token, $method, $url, $parameters);
        $authorization = 'Authorization: Basic ' . $this->encodeAppAuthorization($this->consumer);
        $result = $this->request($request->getNormalizedHttpUrl(), $method, $authorization, $parameters);
        $response = JsonDecoder::decode($result, $this->decodeJsonAsArray);
        $this->response->setBody($response);
        return $response;
    }

    /**
     * Make GET requests to the API.
     *
     * @param string $path
     * @param array  $parameters
     *
     * @return array|object
     */
    public function get($path, array $parameters = [])
    {

        return $this->http('GET', $this->API_HOST, $path, $parameters, false);
    }


    /**
     * Make POST requests to the API.
     *
     * @param string $path
     * @param array  $parameters
     * @param bool   $json
     *
     * @return array|object
     */
    public function post($path, array $parameters = [], $json = false)
    {
        return $this->http('POST', $this->API_HOST, $path, $parameters, $json);
    }

    /**
     * Make DELETE requests to the API.
     *
     * @param string $path
     * @param array  $parameters
     *
     * @return array|object
     */
    public function delete($path, array $parameters = [])
    {
        return $this->http('DELETE', $this->API_HOST, $path, $parameters, false);
    }

    /**
     * Make PUT requests to the API.
     *
     * @param string $path
     * @param array  $parameters
     *
     * @return array|object
     */
    public function put($path, array $parameters = [])
    {
        return $this->http('PUT', $this->API_HOST, $path, $parameters, false);
    }


    /**
     * Private method to upload media (not chunked) to upload.twitter.com.
     *
     * @param string $path
     * @param array  $parameters
     *
     * @return array|object
     */
    /**
     * @param string $method
     * @param string $host
     * @param string $path
     * @param array  $parameters
     * @param bool   $json
     *
     * @return array|object
     */
    protected function http($method, $host, $path, array $parameters, $json)
    {

        $this->resetLastResponse();
        $this->resetAttemptsNumber();
        $url = sprintf('%s/%s/%s'  . $this->end_url,$this->API_HOST, $this->version, $path);

        $this->response->setApiPath($path);
        if (!$json) {
            $parameters = $this->cleanUpParameters($parameters);
        }

        return $this->makeRequests($url, $method, $parameters, $json);
    }

}



