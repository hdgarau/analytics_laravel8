<?php

namespace App\Mail;

use App\Models\PasswordReset;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ResetPasswordShipped extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    protected $user;
    protected $code;
    public function __construct(User $user)
    {
        $this->user = $user;
        $this->code = rand(10000,99999);
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $passwordReset = new PasswordReset();
        $passwordReset->user_id = $this->user->id;
        $passwordReset->code = $this->code;
        $passwordReset->expire = Carbon::now()->addHour();
        $passwordReset->save();
        return $this->view('mails.reset_password_code')->with([ 'code' => $this->code ]);
    }
}
