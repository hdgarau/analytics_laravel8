<?php

namespace App\View\Components;

use Illuminate\View\Component;

class SelectStd extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public $name;
    public $options;
    public $selected;
    public function __construct($name, $source,$selected=null)
    {
        $this->name =$name;
        $this->selected =$selected;
        $this->options = $this->get($source);
    }

    public function isSelected($value)
    {
        return $this->selected == $value;
    }
    protected function get($source)
    {
        if(is_object($source))
        {
            return $source;
        }
        switch ($source)
        {
            case 'platforms':
                return \App\Models\Platform::select(['id as value', 'name as label'])->get();
            case 'metric_formats':
                return \App\Models\MetricFormats::select(['name as value', 'caption as label'])->get();
            case 'metric_objectives':
                return \App\Models\MetricObjectives::select(['name as value', 'caption as label'])->get();
        }
    }
    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('components.select-std');
    }
}
