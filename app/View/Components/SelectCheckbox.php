<?php

namespace App\View\Components;

use Illuminate\View\Component;

class SelectCheckbox extends SelectStd
{
    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('components.select-checkbox');
    }
    public function __construct($name, $source, $selected = null)
    {
        parent::__construct($name, $source, $selected);
        $this->selected = json_decode($this->selected) ? json_decode($this->selected) : [];
    }

    public function isSelected($value)
    {
        return in_array(  $value, $this->selected);
    }
}
