<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LabelIndicator extends Model
{
    protected $fillable = ['indicator_id', 'label_id'];

}
