<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Indicator extends Model
{
    //
    protected $fillable = ['name','user_id','description', 'indicator_type'];

}
