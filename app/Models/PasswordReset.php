<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PasswordReset  extends Model
{
    protected $fillable = [
        'user_id','code','expire'
    ];
    const UPDATED_AT = null;
    const CREATED_AT = null;

}
