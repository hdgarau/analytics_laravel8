<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MetricFormats extends Model
{
    //
    protected $fillable = ['name', 'caption'];
}
