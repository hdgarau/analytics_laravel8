<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class IndicatorMetric extends Model
{
    protected $fillable = ['indicator_id', 'metric_id'];


}
