<?php

namespace App\Models;

use App\Http\Classes\Account;
use App\Http\Classes\Formatter;
use App\Http\Factories\CollectionAccountFactory;
use App\Http\Factories\SocialProviderFactory;
use Illuminate\Database\Eloquent\Model;

class UserPlatform extends Model
{
    protected $fillable = ['user_id', 'platform_id', 'token', 'token_secret','refresh_token', 'platform_foreign_id','server'];


    public function getDriver()
    {
        return Platform::find($this->platform_id)->driver;
    }
    public function getPlatform()
    {
        return Platform::find($this->platform_id);
    }

    public function findAccount($account_id)
    {
        $graph = SocialProviderFactory::driver($this->getDriver());
        $graph->setToken($this->token);
        return $graph->findAccount($account_id);
    }


    protected function getMetricsValuesByTokenAndDriver(Account $account, $metrics = null,  $format_names = null)
    {
        $graph = SocialProviderFactory::driver($account->getDriver());
        $graph->setToken($this->token);

        $arrMetrics =
            $metrics === null ?
                Metric::getMetricsByDriverAndIndicator($account->getDriver(), null, $format_names) :
                $metrics;

        return $graph->{'get' . ucfirst( $format_names ) . 'MetricsValues'}($account, $arrMetrics);
    }
    protected function getPageMetricsValuesByTokenAndDriver($account, $metrics = null)
    {
        $this->getMetricsValuesByTokenAndDriver($account,$metrics,'page');
    }
    protected function getPostMetricsValuesByTokenAndDriver($account, $metrics = null)
    {
        $this->getMetricsValuesByTokenAndDriver($account,$metrics,'post');
    }
    protected function getWebMetricsValuesByTokenAndDriver($account, $metrics = null)
    {
        $this->getMetricsValuesByTokenAndDriver($account,$metrics,'web');
    }

    protected function getMetricsValues($indicator_id = null, $format_names = null)
    {
        $data = collect();

        foreach ($this->getAccounts() as $account) {
            $data = $data->merge( $this->getMetricsValuesByTokenAndDriver(
                $account,
                Metric::getMetricsByDriverAndIndicator($account->getDriver(), $indicator_id, $format_names),
                $format_names)
            );

        }
        return Formatter::FormatNameMetricsCollection_ArrayByPlatformUserMetricValue( $data, $format_names);
    }
    protected function getPageMetricsValues($indicator_id = null)
    {
        $this->getMetricsValues($indicator_id,'page');
    }
    protected function getPostMetricsValues($indicator_id = null)
    {
        $this->getMetricsValues($indicator_id,'post');
    }
    protected function getWebMetricsValues($indicator_id = null)
    {
        $this->getMetricsValues($indicator_id,'web');
    }

    public function getAccounts()
    {
        $graph = SocialProviderFactory::driver($this->getDriver());
        $graph->setToken($this->token);
        return $graph->getCurrentUserAccounts();
    }
    public function getFormatDefaultMetricsValuesByTokenAndDriver(Account $account, $metrics = null)
    {
        return $this->getMetricsValuesByTokenAndDriver(
            $account,
            $metrics,
            SocialProviderFactory::driver($account->getDriver())::default_format
        );
    }
}
