<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Metric extends Model
{
    //
    protected $fillable = [ 'name', 'platform_id','format_names','objective_names','description'];
    protected $casts = ['format_names' => 'array', 'objective_names' => 'array',];


    public static function __callStatic($method, $parameters)
    {
        $method_allowed = [
            'getMetricsByDriverAndIndicator' => '^get(\w+?)MetricsByDriverAndIndicator',
            'getMetricsByIndicator' => '^get(\w+?)MetricsByIndicator'
        ];
        foreach ($method_allowed as $method_base => $regex)
        {
            if(preg_match('/' . $regex .'/i', $method, $matches))
            {
                return (new static)->$method_base(...array_merge($parameters, [strtolower( $matches[1])]));
            }
        }
        return (new static)->$method(...$parameters);
    }

    public function getFormatCaptions()
    {
        return array_map(
            function($el){
                $obj  = MetricFormats::where('name', $el)->first();
                return is_null($obj) ? $el : $obj->caption;
                },
            json_decode($this->format_names));
    }
    public function getObjectiveCaptions()
    {
        return array_map(
            function($el){
                $obj  = MetricObjectives::where('name', $el)->first();
                return is_null($obj) ? $el : $obj->caption;
            },
            json_decode($this->objective_names));
    }

    static function getMetricsByDriverAndIndicator($driver = null, $indicator_id = null, $format_names = null)
    {
        $metrics = (new static)->select(['metrics.name','format_names'])
            ->join('platforms', 'platforms.id', '=', 'metrics.platform_id');
        if ($driver !== null) {
            $metrics->where('platforms.driver', $driver);
        }

        if ($indicator_id !== null) {
            $metrics
                ->join('indicator_metrics', 'metrics.id', '=', 'indicator_metrics.metric_id')
                ->where('indicator_id' ,'=',$indicator_id);

        }

        $metrics = $metrics->get();

        if ($format_names !== null) {
            $metrics = $metrics->filter(function ($el) use($format_names)
            { return \App\Http\Classes\Utils::hasAnyValueFieldsJSONInArray($el->format_names,[$format_names]);});
        }
        return
            array_map(function ($v) {
                return $v['name'];
            }, $metrics->toArray());
    }

    static function getMetricsByIndicator($indicator_id = null, $format_name=null)
    {
        (new static)->getMetricsByDriverAndIndicator(null,$indicator_id,$format_name);
    }

    public function platform()
    {
        return $this->belongsTo('App\Models\Platform');
    }
}
