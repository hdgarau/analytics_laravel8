<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MetricObjectives extends Model
{
    //

    protected $fillable = ['name', 'caption'];
}
