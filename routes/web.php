<?php


use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->to('/home');
});
Route::get('/about', function () {
    $filename = 'app/policy.pdf';
    $path = storage_path($filename);

    return Response::make(file_get_contents($path), 200, [
        'Content-Type' => 'application/pdf',
        'Content-Disposition' => 'inline; filename="'.$filename.'"'
    ]);
    ///readfile( storage_path('app/policy.pdf'));
});

Route::get('/test', function (){
    return 'test';
});
//------------------------------------------------Login--------------------------------------//
Auth::routes();

Route::get('/validation_email',  function (){
    return view('auth.msg_mail_validation');
});
Route::get('/reset_password_no_code/{user}', function (\App\Models\User $user){
    return view('auth.reset_password_no_code')->with('user', $user);
});
Route::get('/login_from_social/{user}', function (\App\Models\User $user){
    return view('auth.login_from_social')->with('user', $user);
});
Route::get('/reset_password', function (){
    return view('auth.reset_password_email');
});
Route::post('/reset_password',  '\App\Http\Controllers\Auth\RegisterController@RecoverPassSendMail');

Route::post('/reset_password_code/{user}','\App\Http\Controllers\Auth\RegisterController@VerifyCodeSentToResetPassword');
Route::put('/reset_password_code/{user}','\App\Http\Controllers\Auth\RegisterController@ChangePassword');
Route::get('/reset_password_code/{user}',  function (\App\Models\User $user){
    if(empty($user))
    {
        abort(419, 'Datos perdidos');
    }
    return view('auth.reset_password_code', ['user' => $user]);
});

Route::get('/user/activate/{id}', '\App\Http\Controllers\Auth\RegisterController@validateUser');
Route::get('/user/reset_password/{id}', '\App\Http\Controllers\Auth\RegisterController@resetPasswordForm');

//------------------------------------------------Social--------------------------------------//
Route::get('/login/{driver}', '\\App\\Http\\Controllers\\Auth\LoginController@redirectToDriverProvider');
Route::get('/login/callback/{driver}', '\\App\\Http\\Controllers\\Auth\LoginController@handleProviderCallback');

//----------------------------------------------Middleware auth-------------------------------//
Route::group(['middleware' => [
    'auth',
    \App\Http\Middleware\HasEmailVerify::class
    //function (){ return  auth::user()->validated ? true : redirect('login');}
]], function() {
    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
    Route::get('/select_platform/{platform}', '\\App\\Http\\Controllers\\HomeController@selectPlatform');
    Route::post('/user_platform/update_fields/{userPlatform}', '\\App\\Http\\Controllers\\HomeController@userPlatformUpdate');
    Route::delete('/user_platform/{userPlatform}', '\\App\\Http\\Controllers\\HomeController@userPlatformDelete');
    Route::get('/logout', '\\App\\Http\\Controllers\\Auth\\LoginController@logout');
    Route::group(['middleware' => [
        \App\Http\Middleware\RedirectIfIsNotPlatformSelected::class]],
        function() {
            Route::get('/select_profiles', '\\App\\Http\\Controllers\\SelectProfilesController@index');
            Route::post('/select_profiles', '\\App\\Http\\Controllers\\SelectProfilesController@saveAccountsSelected');
            Route::group(['middleware' => [
                \App\Http\Middleware\RedirectIfIsNotAccountSelected::class]],
                function() {
                    Route::get('/select_indicators', '\\App\\Http\\Controllers\\SelectIndicatorsController@index');
                    Route::post('/select_indicators/{indicator}', '\\App\\Http\\Controllers\\SelectIndicatorsController@saveAndNext');
                    Route::get('/select_indicators/{indicator}', '\\App\\Http\\Controllers\\SelectIndicatorsController@index');
                    Route::get('/select_indicators/description/{indicator}', '\\App\\Http\\Controllers\\SelectIndicatorsController@description');
                    Route::get('/indicator', '\\App\\Http\\Controllers\\SelectIndicatorsController@createOrUpdate');
                    Route::get('/indicator/{indicator}', '\\App\\Http\\Controllers\\SelectIndicatorsController@createOrUpdate');
                    Route::post('/indicator', '\\App\\Http\\Controllers\\SelectIndicatorsController@createOrUpdate');
                    Route::post('/indicator/{indicator}', '\\App\\Http\\Controllers\\SelectIndicatorsController@createOrUpdate');
                    Route::post('/indicator/{indicator}/toggle_metric/{metric}', '\\App\\Http\\Controllers\\IndicatorController@toggleMetric');

                    Route::get('/indicator_metrics/{indicator}', '\\App\\Http\\Controllers\\IndicatorMetricController@index');
                    Route::get('/indicator_labels/{indicator}', '\\App\\Http\\Controllers\\IndicatorLabelController@index');
                    Route::post('/indicator_labels/{indicator}', '\\App\\Http\\Controllers\\IndicatorLabelController@saveAndFinish');
                    Route::get('/indicators/{indicator}/metrics', '\\App\\Http\\Controllers\\IndicatorController@getMetrics');

                    Route::group(['middleware' => [
                        \App\Http\Middleware\RedirectIfIsNotIndicatorSelected::class]],
                        function() {
                            Route::get('/select_time_range', '\\App\\Http\\Controllers\\SelectFiltersController@timeRangeIndex');
                            Route::post('/select_time_range', '\\App\\Http\\Controllers\\SelectFiltersController@timeRangeSaveAndNext');
                            Route::get('/select_time_range/form_past_today', function () {
                                return view('includes.form_past_today');
                            });
                            Route::get('/select_time_range/form_past_past', function () {
                                return view('includes.form_past_past');
                            });
                            Route::group(['middleware' => [
                                \App\Http\Middleware\RedirectIfIsNotDatesSelected::class]],
                                function () {
                                    Route::get('/select_key_words', '\\App\\Http\\Controllers\\SelectFiltersController@keyWordsIndex');
                                    Route::post('/select_key_words', '\\App\\Http\\Controllers\\SelectFiltersController@keyWordsSaveAndNext');


                                    Route::get('/show_result', '\\App\\Http\\Controllers\\ResultsController@index'

                                        /*function () {
                                        return view('msg.desarrollo');
                                        dd(session('indicator_selected'),
                                            session('userPlatforms_selected'),
                                            session('accounts_selected'),
                                            session('date_past_since'),
                                            session('date_past_until'),
                                            session('date_recent_since'),
                                            session('date_recent_until'),
                                            session('conditions_content'));
                                    }*/);
                                });
                        });
                });
        });



    Route::get('/configuration', function(){
        return view('configuration');
    });
    Route::post('/configuration', '\\App\\Http\\Controllers\\ConfigController@update');

    //abms
    Route::resource('users', '\\App\\Http\\Controllers\\UserController')->middleware(\App\Http\Middleware\RedirectIfIsNotAdmin::class);
    Route::resource('metrics', '\\App\\Http\\Controllers\\MetricController')->middleware(\App\Http\Middleware\RedirectIfIsNotAdmin::class);
    Route::resource('metric_formats', '\\App\\Http\\Controllers\\MetricFormatsController')->middleware(\App\Http\Middleware\RedirectIfIsNotAdmin::class);
    Route::resource('metric_objectives', '\\App\\Http\\Controllers\\MetricObjectivesController')->middleware(\App\Http\Middleware\RedirectIfIsNotAdmin::class);
});

